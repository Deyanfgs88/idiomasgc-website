<?php

/*

Template Name: Traducciones

*/

get_header(); 
the_post(); ?>

<div class="template-traducciones">
    <div class="container-fluid">
        <div class="container-traducciones">
            <div class="cta-back-title-general-traducciones">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-traducciones">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_traducciones'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-traducciones">

                <div class="imagen-text-pasos-traduccion-portada">
                    <img src="<?php the_field('imagen_portada_traducciones'); ?>" alt="imagen portada traducciones">
                    <div class="text-info-traducciones">
                        <?php the_field('texto_informacion_portada_traducciones'); ?>
                    </div>

                    <?php 
                    $imagen_pasos_proceso_traduccion = get_field('imagen_pasos_proceso_traduccion');
                    if ($imagen_pasos_proceso_traduccion){ 
                    ?>

                    <div class="imagen-pasos-procesos-traduccion">
                        <div class="titulo-pasos-traduccion">
                            <?php the_field('titulo_pasos_proceso_traduccion'); ?>
                        </div>
                        <img src="<?php the_field('imagen_pasos_proceso_traduccion'); ?>" alt="imagen pasos proceso traducción">
                    </div>

                    <?php } ?>

                </div> <?php // .imagen-text-pasos-traduccion-portada ?>

                <div class="item-info-traducciones">
                    <?php
                        $titulo_lista_motivos_traducciones = get_field('titulo_motivos_para_elegirnos_traducciones');
                        $lista_motivos_traducciones = get_field('motivos_para_elegirnos_traducciones');
                        if ($titulo_lista_motivos_traducciones && $lista_motivos_traducciones){
                            echo '<div class="titulo-item-info">' . $titulo_lista_motivos_traducciones . '</div>';
                            echo '<ul>';
                            foreach ($lista_motivos_traducciones as $motivo_eleccion) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $motivo_eleccion['motivo_eleccion_traducciones'] . '</li>';
                            }
                            echo '</ul>';
                        }
                    ?>
                </div>

                <div class="title-nuestros-servicios">
                    <div class="title-servicios">
                        <?php the_field('titulo_nuestros_servicios'); ?>
                    </div>
                </div>
                
                <?php
                $servicio_traduccion = get_field('servicio_traduccion');
                if ($servicio_traduccion){ 
                ?>
                <div class="item-info-traducciones servicios">
                    <p><i class="fas fa-language"></i></p>
                    <div class="titulo-item-info">
                        <?php the_field('titulo_servicio_traduccion'); ?>
                    </div>
                    <?php the_field('servicio_traduccion'); ?>
                </div>
                <?php } ?>

                <?php
                $servicio_traduccion_jurada = get_field('servicio_traduccion_jurada');
                if ($servicio_traduccion_jurada){ 
                ?>
                <div class="item-info-traducciones servicios">
                    <p><i class="fas fa-gavel"></i></p>
                    <div class="titulo-item-info">
                        <?php the_field('titulo_servicio_traduccion_jurada'); ?>
                    </div>
                    <?php the_field('servicio_traduccion_jurada'); ?>
                </div>
                <?php } ?>

                <?php
                $servicio_localizacion = get_field('servicio_localizacion');
                if ($servicio_localizacion){ 
                ?>
                <div class="item-info-traducciones servicios">
                    <p><i class="fas fa-map-marker-alt"></i></p>
                    <div class="titulo-item-info">
                        <?php the_field('titulo_servicio_localizacion'); ?>
                    </div>
                    <?php the_field('servicio_localizacion'); ?>
                </div>
                <?php } ?>

                <?php
                $servicio_licitaciones_internacionales= get_field('servicio_licitaciones_internacionales');
                if ($servicio_licitaciones_internacionales){ 
                ?>
                <div class="item-info-traducciones servicios">
                    <p><i class="fas fa-globe-africa"></i></p>
                    <div class="titulo-item-info">
                        <?php the_field('titulo_servicio_licitaciones_internacionales'); ?>
                    </div>
                    <?php the_field('servicio_licitaciones_internacionales'); ?>
                </div>
                <?php } ?>

                <?php
                $servicio_maquetacion = get_field('servicio_maquetacion');
                if ($servicio_maquetacion){ 
                ?>
                <div class="item-info-traducciones servicios">
                    <p><i class="fas fa-file-invoice"></i></p>
                    <div class="titulo-item-info">
                        <?php the_field('titulo_servicio_maquetacion'); ?>
                    </div>
                    <?php the_field('servicio_maquetacion'); ?>
                </div>
                <?php } ?>

                <?php
                $servicio_interpretacion = get_field('servicio_interpretacion');
                if ($servicio_interpretacion){ 
                ?>
                <div class="item-info-traducciones servicios">
                    <p><i class="fas fa-headset"></i></p>
                    <div class="titulo-item-info">
                        <?php the_field('titulo_servicio_interpretacion'); ?>
                    </div>
                    <?php the_field('servicio_interpretacion'); ?>
                </div>
                <?php } ?>

                <?php
                $servicio_revision_correccion = get_field('servicio_revision_correccion');
                if ($servicio_revision_correccion){ 
                ?>
                <div class="item-info-traducciones servicios">
                    <p><i class="fas fa-check"></i></p>
                    <div class="titulo-item-info">
                        <?php the_field('titulo_servicio_revision_correccion'); ?>
                    </div>
                    <?php the_field('servicio_revision_correccion'); ?>
                </div>
                <?php } ?>

                <?php
                $servicio_transcripcion = get_field('servicio_transcripcion');
                if ($servicio_transcripcion){ 
                ?>
                <div class="item-info-traducciones servicios">
                    <p><i class="fas fa-file-alt"></i></p>
                    <div class="titulo-item-info">
                        <?php the_field('titulo_servicio_transcripcion'); ?>
                    </div>
                    <?php the_field('servicio_transcripcion'); ?>
                </div>
                <?php } ?>

                <?php
                $servicio_copywriting = get_field('servicio_copywriting');
                if ($servicio_copywriting){ 
                ?>
                <div class="item-info-traducciones servicios">
                    <p><i class="fas fa-sync-alt"></i></p>
                    <div class="titulo-item-info">
                        <?php the_field('titulo_servicio_copywriting'); ?>
                    </div>
                    <?php the_field('servicio_copywriting'); ?>
                </div>
                <?php } ?>

                <?php
                $servicio_glosario_memorias_traduccion = get_field('servicio_glosario_memorias_traduccion');
                if ($servicio_glosario_memorias_traduccion){ 
                ?>
                <div class="item-info-traducciones servicios">
                    <p><i class="fas fa-list-ul"></i></p>
                    <div class="titulo-item-info">
                        <?php the_field('titulo_servicio_glosario_memorias_traduccion'); ?>
                    </div>
                    <?php the_field('servicio_glosario_memorias_traduccion'); ?>
                </div>
                <?php } ?>

                <div class="cta-solicita-presupuesto">
                    <a href="<?php the_field('boton_solicita_presupuesto_traducciones'); ?>">Solicita tu presupuesto</a>
                </div>

            </div> <?php // . container-general-info-traducciones ?>
            
         </div> <?php // .container-traducciones ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-traducciones ?>




<?php get_footer(); ?>