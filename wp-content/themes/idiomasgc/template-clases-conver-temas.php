<?php

/*

Template Name: Temas Clases de conversación

*/

get_header(); 
the_post(); ?>

<div class="template-clases-conver-temas">
    <div class="container-fluid">
        <div class="container-clases-conver-temas">
            <div class="cta-back-title-general-clases-conver-temas">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pagina_clases_conver_temas'); ?>"><i class="fas fa-chevron-left"></i>Clases de conversación</a>
                </div>
                <div class="title-clases-conver-temas">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_pagina_principal_clases_conver_temas'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-clases-conver-temas">

                <div class="semana-temas-info">
                    <?php
                        $temas_semana_actual_conver = get_field('semana_actual_clases_conver_temas');
                        if ($temas_semana_actual_conver){
                    ?>
                    <div class="semana-info">
                        <h3><i class="far fa-calendar-alt"></i><?php the_field('titulo_semana_actual_clases_conver_temas'); ?></h3>
                        <?php
                            echo '<ul>';
                            foreach ($temas_semana_actual_conver as $temas_semana_actual) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $temas_semana_actual['texto_nivel_conver_temas'] . ': <a href="'. $temas_semana_actual['archivo_tema_conversacion'] .'" target="_blank">Descargar</a></li>';
                            }
                            echo '</ul>';
                        ?>
                    </div>
                    <?php } ?>

                    <?php
                        $temas_semana_proxima_conver = get_field('semana_proxima_clases_conver_temas');
                        if ($temas_semana_proxima_conver){
                    ?>
                    <div class="semana-info">
                        <h3><i class="far fa-calendar-alt"></i><?php the_field('titulo_semana_proxima_clases_conver_temas'); ?></h3>
                        <?php
                            echo '<ul>';
                            foreach ($temas_semana_proxima_conver as $temas_semana_proxima) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $temas_semana_proxima['texto_nivel_conver_temas'] . ': <a href="'. $temas_semana_proxima['archivo_tema_conversacion'] .'" target="_blank">Descargar</a></li>';
                            }
                            echo '</ul>';
                        ?>
                    </div>
                    <?php } ?>

                </div>

            </div> <?php // . container-general-info-clases-conver-temas ?>
            
         </div> <?php // .container-clases-conver-temas ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-clases-conver-temas ?>




<?php get_footer(); ?>