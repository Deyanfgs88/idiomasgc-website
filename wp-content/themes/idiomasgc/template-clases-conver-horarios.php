<?php

/*

Template Name: Horarios Clases de conversación

*/

get_header(); 
the_post(); ?>

<div class="template-clases-conver-horarios">
    <div class="container-fluid">
        <div class="container-clases-conver-horarios">
            <div class="cta-back-title-general-clases-conver-horarios">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pagina_clases_conver_horarios'); ?>"><i class="fas fa-chevron-left"></i>Clases de conversación</a>
                </div>
                <div class="title-clases-conver-horarios">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_pagina_principal_clases_conver_horarios'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-clases-conver-horarios">

                <div class="container-item-horarios">
                    
                    <?php
                        $tomas_morales_horarios =  get_field('tomas_morales_clases_conver_horarios');
                        if ($tomas_morales_horarios){
                    ?>
                    <div class="info-horarios-conver">
                        <h3><i class="fas fa-map-marker-alt"></i>Tomás Morales</h3>
                        <div class="table-horarios">
                            <table class="table table-bordered">
                                <?php
                                echo '<thead>';
                                echo '<tr>';
                                    foreach ( $tomas_morales_horarios['header'] as $th ) {
                
                                        echo '<th>';
                                            echo $th['c'];
                                        echo '</th>';
                                    }
                    
                                echo '</tr>';
                                echo '</thead>';
                                ?>
                                <tbody>
                                <?php
                                    foreach ( $tomas_morales_horarios['body'] as $tr ) {
                                        echo '<tr>';
                                            foreach ( $tr as $td ) {
                                                echo '<td>';
                                                    echo $td['c'];
                                                echo '</td>';
                                            }
                                        echo '</tr>';
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div> <?php // .info-horarios-conver?>
                    <?php } ?>
                    
                    <?php
                        $mesa_lopez_horarios =  get_field('mesa_lopez_clases_conver_horarios');
                        if ($mesa_lopez_horarios){
                    ?>
                    <div class="info-horarios-conver">
                        <h3><i class="fas fa-map-marker-alt"></i>Mesa y López</h3>
                        <div class="table-horarios">
                            <table class="table table-bordered">
                                <?php
                                echo '<thead>';
                                echo '<tr>';
                                    foreach ( $mesa_lopez_horarios['header'] as $th ) {
                
                                        echo '<th>';
                                            echo $th['c'];
                                        echo '</th>';
                                    }
                    
                                echo '</tr>';
                                echo '</thead>';
                                ?>
                                <tbody>
                                <?php
                                    foreach ( $mesa_lopez_horarios['body'] as $tr ) {
                                        echo '<tr>';
                                            foreach ( $tr as $td ) {
                                                echo '<td>';
                                                    echo $td['c'];
                                                echo '</td>';
                                            }
                                        echo '</tr>';
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div> <?php // .info-horarios-conver ?>
                    <?php } ?>

                    <?php
                        $siete_palmas_horarios =  get_field('siete_palmas_clases_conver_horarios');
                        if ($siete_palmas_horarios){
                    ?>
                    <div class="info-horarios-conver">
                        <h3><i class="fas fa-map-marker-alt"></i>7 Palmas</h3>
                        <div class="table-horarios">
                            <table class="table table-bordered">
                                <?php
                                echo '<thead>';
                                echo '<tr>';
                                    foreach ( $siete_palmas_horarios['header'] as $th ) {
                
                                        echo '<th>';
                                            echo $th['c'];
                                        echo '</th>';
                                    }
                    
                                echo '</tr>';
                                echo '</thead>';
                                ?>
                                <tbody>
                                <?php
                                    foreach ( $siete_palmas_horarios['body'] as $tr ) {
                                        echo '<tr>';
                                            foreach ( $tr as $td ) {
                                                echo '<td>';
                                                    echo $td['c'];
                                                echo '</td>';
                                            }
                                        echo '</tr>';
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div> <?php // .info-horarios-conver ?>
                    <?php } ?>

                </div> <?php // .container-item-horarios ?>

            </div> <?php // . container-general-info-clases-conver-horarios ?>
            
         </div> <?php // .container-clases-conver-horarios ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-clases-conver-horarios ?>




<?php get_footer(); ?>