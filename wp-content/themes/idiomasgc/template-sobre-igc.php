<?php

/*

Template Name: Sobre IGC

*/

get_header(); 
the_post(); ?>

<div class="template-sobre-igc">
    <div class="container-fluid">
        <div class="container-sobre-igc">
            <div class="cta-back-title-general-sobre-igc">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-sobre-igc">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_sobre_igc'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-sobre-igc">
                
                <?php
                    $texto_sobre_igc = get_field('texto_sobre_igc');
                    if ($texto_sobre_igc){
                ?>
                <div class="cont-text-sobre-igc">
                    <?php the_field('texto_sobre_igc'); ?>
                </div>
                <?php } ?>

                <div class="row">

                    <div class="col-12 col-lg-6">
                        <?php
                            $video_sobre_igc = get_field('video_sobre_igc');
                            if ($video_sobre_igc){
                        ?>
                        <div class="container-video">
                            <iframe src="<?php the_field('video_sobre_igc'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <?php } ?>
                    </div> <?php // .col ?>

                    <div class="col-12 col-lg-6">
                        <?php
                            $titulo_sobre_igc = get_field('titulo_sobre_igc');
                            $lista_sobre_igc = get_field('lista_sobre_igc');
                            if ($titulo_sobre_igc && $lista_sobre_igc){
                        ?>
                        <div class="container-lista-ofrecemos">
                            <div class="titulo-sobre-igc">
                                <?php the_field('titulo_sobre_igc'); ?>
                            </div>
                            <?php
                                echo '<ul>';
                                foreach ($lista_sobre_igc as $sobre_igc) {
                                    echo '<li><i class="fas fa-chevron-right"></i>' . $sobre_igc['info_sobre_igc'] . '</li>';
                                }
                                echo '</ul>';
                            ?>
                        </div>
                        <?php } ?>
                    </div> <?php // .col ?>

                </div> <?php // .row ?>

            </div> <?php // . container-general-info-sobre-igc ?>
            
         </div> <?php // .container-sobre-igc ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-sobre-igc ?>


<?php get_footer(); ?>