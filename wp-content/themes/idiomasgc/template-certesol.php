<?php

/*

Template Name: CertTESOL Trinity College London

*/

get_header(); 
the_post(); ?>

<div class="template-formac-profesorado-info">
    <div class="container-fluid">
        <div class="container-formac-prof-info">
            <div class="cta-back-title-general-formac-info">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_formac_profesorado'); ?>"><i class="fas fa-chevron-left"></i>Formación del profesorado</a>
                </div>
                <div class="title-formac-prof">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_certesol_info'); ?></h2>
                </div>
            </div>

            <div class="container-general-formac-prof-info">

                <div class="imagen-text-portada-formac-prof">
                    <img src="<?php the_field('imagen_portada_certesol'); ?>" alt="imagen portada certTESOL">
                    <div class="text-info-formac-prof">
                        <?php the_field('texto_informacion_certesol'); ?>
                    </div>
                </div> <?php // .imagen-text-portada-formac-prof ?>

                <div class="item-info-formac-prof">
                    <h4>Destinatarios:</h4>
                    <?php
                        $lista_destinatarios_certesol = get_field('lista_destinatarios_certesol');
                        if ($lista_destinatarios_certesol){
                            echo '<ul>';
                            foreach ($lista_destinatarios_certesol as $destinario_certesol) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $destinario_certesol['destinatario_certesol'] . '</li>';
                            }
                            echo '</ul>';
                        }
                    ?>
                </div>

                <div class="item-info-formac-prof">
                    <h4>Competencias generales:</h4>
                    <?php
                        $lista_competencias_generales_certesol = get_field('lista_competencias_generales_certesol');
                        if ($lista_competencias_generales_certesol){
                            echo '<ul>';
                            foreach ($lista_competencias_generales_certesol as $competencia_general_certesol) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $competencia_general_certesol['competencia_certesol'] . '</li>';
                            }
                            echo '</ul>';
                        }
                    ?>
                </div>

                <div class="item-info-formac-prof">
                    <h4>Detalles:</h4>
                    <?php
                        $lista_detalles_certesol = get_field('lista_detalles_certesol');
                        if ($lista_detalles_certesol){
                            echo '<ul>';
                            foreach ($lista_detalles_certesol as $detalle_certesol) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $detalle_certesol['detalle_certesol'] . '</li>';
                            }
                            echo '</ul>';
                        }
                    ?>
                </div>

                <div class="item-info-formac-prof">
                    <h4><i class="fas fa-euro-sign"></i>Precio:</h4>
                    <?php
                        $lista_precios_certesol = get_field('lista_precio_certesol');
                        if ($lista_precios_certesol){
                            echo '<ul>';
                            foreach ($lista_precios_certesol as $precio_certesol) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $precio_certesol['precio_certesol'] . '</li>';
                            }
                            echo '</ul>';
                        }
                    ?>
                </div>
                
                <div class="item-info-formac-prof ctas">
                    <a href="<?php the_field('boton_url_trinity_college_london_certesol'); ?>" target="_blank">Visita la web del Trinity College London</a>
                    <a href="<?php the_field('boton_sumario_curso_certesol'); ?>" target="_blank">Descarga el sumario del curso</a>
                    <a href="<?php the_field('boton_reserva_ahora_certesol'); ?>">Reserva ahora</a>
                </div>

            </div> <?php // . container-general-formac-prof-info ?>
            
         </div> <?php // .container-formac-prof-info ?>
    </div> <?php // .container-fluid ?>
</div><? // .template-formac-profesorado-info ?>




<?php get_footer(); ?>