<?php
/**
 * idiomasgc functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package idiomasgc
 */

if ( ! function_exists( 'idiomasgc_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function idiomasgc_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on idiomasgc, use a find and replace
		 * to change 'idiomasgc' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'idiomasgc', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-principal' => esc_html__('Menu Principal'),
			'menu-movil' => esc_html__('Menu Movil')
			//'menu-principal' => esc_html__( 'Primary', 'idiomasgc' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'idiomasgc_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'idiomasgc_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function idiomasgc_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'idiomasgc_content_width', 640 );
}
add_action( 'after_setup_theme', 'idiomasgc_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function idiomasgc_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'idiomasgc' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'idiomasgc' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'idiomasgc_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function idiomasgc_scripts() {
	wp_enqueue_style( 'idiomasgc-style', get_template_directory_uri() . '/stylesheets/style.css' );

	wp_enqueue_script( 'idiomasgc-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'idiomasgc-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'idiomasgc-actions', get_template_directory_uri() . '/assets/js/actions.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Enqueue Bootstrap
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/tools/bootstrap/dist/css/bootstrap.min.css' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/tools/bootstrap/dist/js/bootstrap.bundle.min.js');

	// Enqueue Font Awesome
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/tools/fontawesome/css/all.css' );


}
add_action( 'wp_enqueue_scripts', 'idiomasgc_scripts' );

/*** Custom Login ***/
function my_custom_login() {
    wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/stylesheets/login/custom-login.css' );
}
add_action( 'login_enqueue_scripts', 'my_custom_login' );

/*** Custom Login URL ***/

function custom_loginlogo_url() {
	return home_url();
}
add_filter( 'login_headerurl', 'custom_loginlogo_url' );


function wpdocs_my_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
    <input type="text" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" class="button-submit" value="'. esc_attr__( 'Search' ) .'" />
    </form>';
 
    return $form;
}
add_filter( 'get_search_form', 'wpdocs_my_search_form' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**	
 *  Custom Metaboxes for tags Title and H1
 */

class etiquetastitleyh1Metabox {
	private $screen = array(
		'post',
		'page',
	);
	private $meta_fields = array(
		array(
			'label' => 'Etiqueta Title',
			'id' => 'etiquetatitle_83938',
			'type' => 'textarea',
		),
		array(
			'label' => 'Etiqueta H1',
			'id' => 'etiquetah1_62791',
			'type' => 'textarea',
		),
	);
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_fields' ) );
	}
	public function add_meta_boxes() {
		foreach ( $this->screen as $single_screen ) {
			add_meta_box(
				'etiquetastitleyh1',
				__( 'Etiquetas Title y H1', 'textdomain' ),
				array( $this, 'meta_box_callback' ),
				$single_screen,
				'advanced',
				'high'
			);
		}
	}
	public function meta_box_callback( $post ) {
		wp_nonce_field( 'etiquetastitleyh1_data', 'etiquetastitleyh1_nonce' );
		$this->field_generator( $post );
	}
	public function field_generator( $post ) {
		$output = '';
		foreach ( $this->meta_fields as $meta_field ) {
			$label = '<label for="' . $meta_field['id'] . '">' . $meta_field['label'] . '</label>';
			$meta_value = get_post_meta( $post->ID, $meta_field['id'], true );
			if ( empty( $meta_value ) ) {
				$meta_value = $meta_field['default']; }
			switch ( $meta_field['type'] ) {
				case 'textarea':
					$input = sprintf(
						'<textarea style="width: 100%%" id="%s" name="%s" rows="5">%s</textarea>',
						$meta_field['id'],
						$meta_field['id'],
						$meta_value
					);
					break;
				default:
					$input = sprintf(
						'<input %s id="%s" name="%s" type="%s" value="%s">',
						$meta_field['type'] !== 'color' ? 'style="width: 100%"' : '',
						$meta_field['id'],
						$meta_field['id'],
						$meta_field['type'],
						$meta_value
					);
			}
			$output .= $this->format_rows( $label, $input );
		}
		echo '<table class="form-table"><tbody>' . $output . '</tbody></table>';
	}
	public function format_rows( $label, $input ) {
		return '<tr><th>'.$label.'</th><td>'.$input.'</td></tr>';
	}
	public function save_fields( $post_id ) {
		if ( ! isset( $_POST['etiquetastitleyh1_nonce'] ) )
			return $post_id;
		$nonce = $_POST['etiquetastitleyh1_nonce'];
		if ( !wp_verify_nonce( $nonce, 'etiquetastitleyh1_data' ) )
			return $post_id;
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;
		foreach ( $this->meta_fields as $meta_field ) {
			if ( isset( $_POST[ $meta_field['id'] ] ) ) {
				switch ( $meta_field['type'] ) {
					case 'email':
						$_POST[ $meta_field['id'] ] = sanitize_email( $_POST[ $meta_field['id'] ] );
						break;
					case 'text':
						$_POST[ $meta_field['id'] ] = sanitize_text_field( $_POST[ $meta_field['id'] ] );
						break;
				}
				update_post_meta( $post_id, $meta_field['id'], $_POST[ $meta_field['id'] ] );
			} else if ( $meta_field['type'] === 'checkbox' ) {
				update_post_meta( $post_id, $meta_field['id'], '0' );
			}
		}
	}
}
if (class_exists('etiquetastitleyh1Metabox')) {
	new etiquetastitleyh1Metabox;
};

/**
 *  Remove title tag by default
 */
remove_action( 'wp_head', '_wp_render_title_tag', 1 );

/**
 *  Remove editor in the pages
 */
add_action('init', 'my_remove_editor_from_post_type');
function my_remove_editor_from_post_type() {
    remove_post_type_support( 'page', 'editor' );
}

/**
 *  Add code GA for specific pages
 */
function wecodewp_add_ga_code() {
    if( is_page(2755) ){ // Promo Enero
    ?>

	<!-- Global site tag (gtag.js) - Google Ads: 793709920 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-793709920"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'AW-793709920');
	</script>

	<!-- Event snippet for Promos Curso Inglés conversion page
	In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
	<script>
	function gtag_report_conversion_promo_enero(url) {
	var callback = function () {
		if (typeof(url) != 'undefined') {
		window.location = url;
		}
	};
	gtag('event', 'conversion', {
		'send_to': 'AW-793709920/hZS1CNC4vJEBEOCavPoC',
		'event_callback': callback
	});
	return false;
	}
	</script>

	<?php
    }
}
add_action( 'wp_head', 'wecodewp_add_ga_code' );


function mycustom_wp_footer() { 
	if( is_page(2755) ){ // Promo Enero
	?>

	<script type="text/javascript">
	document.addEventListener( 'wpcf7mailsent', function( event ) {
		gtag_report_conversion_promo_enero();
	}, false );
	</script>

<?php
	}	
}

add_action( 'wp_footer', 'mycustom_wp_footer' );

/**
 *  Get ACF bg and text color button submit form for promotion landing in actions js file
 */
function pass_acf_value_to_actions_js_file () {
  $my_field = array ();
  $my_field[] = array (bgcolorformsubmit => get_field ('bgcolor_boton_submit_form_contact'));
  $my_field[] = array (textcolorformsubmit => get_field ('textcolor_boton_submit_form_contact'));
  wp_localize_script ('idiomasgc-actions', 'MyFields', $my_field);
}
add_action ('wp_enqueue_scripts','pass_acf_value_to_actions_js_file');
