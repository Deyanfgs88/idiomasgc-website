<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package idiomasgc
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php idiomasgc_post_thumbnail(); ?>

	<div class="post-title">
		<?php
			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
		?>
	</div>

	<div class="entry-content">
		<?php

		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Páginas:', 'idiomasgc' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<div class="post-meta">
		<?php

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<p>By <?php echo get_the_author(); ?></p>
				<p><?php echo get_the_date('j \d\e F Y'); ?></p>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</div><!-- .entry-header -->

</article><!-- #post-<?php the_ID(); ?> -->
