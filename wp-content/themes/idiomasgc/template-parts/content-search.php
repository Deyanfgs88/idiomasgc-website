<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package idiomasgc
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="title-result-search-post">
		<?php the_title( sprintf( '<h2 class="entry-title"><i class="fas fa-chevron-right"></i><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
