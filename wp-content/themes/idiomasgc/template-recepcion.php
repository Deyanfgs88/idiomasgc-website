<?php

/*

Template Name: Recepción

*/

get_header(); 
the_post(); ?>

<?php
    global $post;
    if ( !post_password_required( $post ) ) {
?>
<div class="template-recepcion">
    <div class="container-fluid">
        <div class="container-recepcion">
            <div class="cta-back-title-general-recepcion">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-recepcion">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                </div>
            </div>

            <div class="container-general-info-recepcion">

                <div class="container-formulario">
                    <div class="titulo">
                        <h2><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_ficha_test'); ?></h2>
                    </div>
                    <div class="formulario">
                        <?php echo do_shortcode( '[contact-form-7 id="3003" title="Ficha Test"]' ); ?>
                    </div>
                </div> <?php // .container-formulario ?>

                <div class="container-formulario">
                    <div class="titulo">
                        <h2><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_clase_prueba'); ?></h2>
                    </div>
                    <div class="formulario">
                        <?php echo do_shortcode( '[contact-form-7 id="3023" title="Clase prueba"]' ); ?>
                    </div>
                </div> <?php // .container-formulario ?>

                <div class="container-formulario">
                    <div class="titulo">
                        <h2><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_bienvenido_nuevo_alumno'); ?></h2>
                    </div>
                    <div class="formulario">
                        <?php echo do_shortcode( '[contact-form-7 id="3027" title="Bienvenida (Nuevo alumno)"]' ); ?>
                    </div>
                </div> <?php // .container-formulario ?>

                <div class="container-formulario">
                    <div class="titulo">
                        <h2><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_bienvenido_nuevo_alumno_nino'); ?></h2>
                    </div>
                    <div class="formulario">
                        <?php echo do_shortcode( '[contact-form-7 id="3050" title="Bienvenida (Nuevo alumno - niño)"]' ); ?>
                    </div>
                </div> <?php // .container-formulario ?>

            </div> <?php // . container-general-info-recepcion ?>
            
         </div> <?php // .container-recepcion ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-recepcion ?>

<?php
    }else{
        echo get_the_password_form();
    }
?>




<?php get_footer(); ?>