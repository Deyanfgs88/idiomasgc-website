<?php

/*

Template Name: Landing Promoción Campaña

*/

get_header(); 
the_post(); ?>

<div class="template-landing-promocion-campana">
    <div class="container-fluid">
        <div class="container-landing-promocion-campana">

            <?php
                $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                if ($tag_h1_meta){
            ?>
            <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
            <?php } else{ ?>
            <h1 class="d-none"><?php echo get_the_title(); ?></h1>
            <?php } ?>
            
            <div class="container-general-info-landing-promocion-campana">
                <div class="container">

                    <div class="imagen-portada-mobile-tablet d-xl-none">
                        <?php
                            $imagen_portada_mobile_tablet = get_field('imagen_portada_promocion_campana_mobile_tablet');
                        ?>
                        <img src="<?php echo $imagen_portada_mobile_tablet['url']; ?>" alt="<?php echo $imagen_portada_mobile_tablet['alt']; ?>" />
                    </div> <?php // .imagen-portada-mobile-tablet ?>

                    <div class="container-form-contact-bg desktop imagen-bg d-none d-xl-flex" style="background-image: url('<?php the_field('imagen_portada_promocion_campana_desktop'); ?>'); background-position: <?php the_field('posicion_portada_promocion_campana'); ?>;">
                        <div class="form-contact" style="background-color: <?php the_field('bgcolor_form_contact'); ?>">
                            <a href="tel:+34928909054" class="telefono-header-form"><i class="fas fa-phone"></i>928 90 90 54</a>
                            <?php echo do_shortcode( '[bkFormFoot observaciones="" imagen="" origen="14" origenForm="contacto" formulario="100"][/bkFormFoot]' ); ?>
                        </div> <?php // .form-contact ?>
                    </div> <?php // .container-form-contact ?>

                    <div class="title-principal-promocion">
                        <div class="titulo">
                            <?php the_field('titulo_principal_promocion_campana'); ?>
                        </div>
                    </div> <?php // .title-principal-promocion ?>

                    <div class="cont-info-promocion">
                        <div class="info-promocion">
                            <div class="titulo">
                                <?php the_field('titulo_curso_idioma'); ?>
                            </div>
                            <?php the_field('texto_curso_idioma'); ?>
                        </div>
                    </div> <?php // .cont-info-promocion ?>

                    <div class="cont-info-promocion">
                        <div class="info-promocion">
                            <div class="titulo">
                                <?php the_field('titulo_promo_seccion_1'); ?>
                            </div>
                            <?php the_field('texto_promo_seccion_1'); ?>
                        </div>
                    </div> <?php // .cont-info-promocion ?>

                    <div class="cont-info-promocion">
                        <div class="info-promocion">
                            <div class="titulo">
                                <?php the_field('titulo_aprende_idioma'); ?>
                            </div>
                            <?php the_field('texto_aprende_idioma'); ?>
                        </div>
                    </div> <?php // .cont-info-promocion ?>

                    <div class="item-info nuestros-cursos">
                        <div class="titulo">
                            <?php the_field('titulo_tus_cursos_idiomasgc'); ?>
                        </div>
                        <?php
                            $lista_info_idiomasgc = get_field('lista_idiomasgc');
                            echo '<ul>';
                            foreach ($lista_info_idiomasgc as $lista_idiomasgc) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $lista_idiomasgc['info_idiomasgc'] . '</li>';
                            }
                            echo '</ul>';
                        ?>
                    </div> <?php // .nuestros-cursos ?>

                    <?php
                        $video_promo = get_field('video_promo_campana');
                        if ($video_promo){
                    ?>
                    <div class="item-info video-promo">
                        <div class="video">
                            <iframe src="<?php the_field('video_promo_campana'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div> <?php // .video-promo ?>
                    <?php } ?>

                    <?php
                        $titulo_lista_cursos = get_field('titulo_lista_cursos');
                        $lista_cursos = get_field('lista_cursos');
                        if (($titulo_lista_cursos) && ($lista_cursos)) {
                    ?>
                    <div class="item-info lista-cursos-promo">
                        <div class="titulo-cursos">
                            <?php the_field('titulo_lista_cursos'); ?>
                        </div>
                        <div class="info-cursos">
                            <ul>
                                <?php foreach ($lista_cursos as $course) {
                                    echo '<li>';
                                    echo '<img src="' . $course['imagen_curso'] . '" alt="imagen curso">';
                                    echo '<div class="titulo-texto">';
                                    echo '<a href="' . $course['pagina_curso']. '"><div class="title">' . $course['titulo_curso'] . '</div></a>';
                                    echo '<div class="text">' . $course['texto_curso'] . '</div>';
                                    echo '</div>';
                                    echo '<a href="' . $course['pagina_curso']. '" class="cta-curso">' . $course['texto_boton_curso'] . '</a>';
                                    echo '</li>';
                                } 
                                ?> 
                            </ul>
                        </div>
                    </div> <?php // .lista-cursos ?>
                    <?php } ?>

                    <?php
                        $titulo_partners = get_field('titulo_partners');
                        $imagenes_partners = get_field('galeria_partners');
                        if (($titulo_partners) && ($imagenes_partners)) {
                    ?>
                    <div class="item-info imagenes-partners">
                        <div class="titulo">
                            <?php the_field('titulo_partners'); ?>
                        </div>
                        <div class="partners">
                            <?php
                                $images = get_field('galeria_partners');
                                $size = 'full'; 
                                if( $images ): ?>
                                <ul>
                                <?php foreach( $images as $image ): ?>
                                    <li> <?php echo wp_get_attachment_image( $image['ID'], $size ); ?> </li>
                                <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div> <?php // .imagenes-partners ?>
                    <?php } ?>

                    <div class="item-info que-dicen-nosotros">
                        <div class="titulo">
                            <?php the_field('titulo_testimonios_idiomasgc'); ?>
                        </div>
                        <div class="texto d-xl-none">
                            <?php the_field('texto_testimonios_idiomasgc');?>
                        </div>

                        <?php 
                            $urls_videos_testimonios = get_field('url_videos_testimonios');
                            if ($urls_videos_testimonios){
                        ?>
                        <div class="videos">
                            <?php 
                                foreach ($urls_videos_testimonios as $url_testimonio) {
                                    echo '<iframe src="' . $url_testimonio['url_video_testi'] . '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'; 
                                } 
                            ?>
                        </div>
                        <?php } ?>

                    </div> <?php // .que-dicen-nosotros ?>

                    <div class="item-info cont-mapas">
                        <div class="titulo">
                            <?php the_field('titulo_localizacion_idiomasgc'); ?>
                        </div>
                        <div class="texto d-xl-none">
                            <?php the_field('texto_localizacion_idiomasgc'); ?>
                        </div>
                        <div class="mapas">
                            <div class="row">
                                <iframe src="<?php the_field('url_mapa_sede_sietepalmas'); ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                            <div class="row">
                                <iframe src="<?php the_field('url_mapa_sede_mesaylopez'); ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                            <div class="row">
                                <iframe src="<?php the_field('url_mapa_sede_tomasmorales'); ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div> <?php // .cont-mapas ?>

                    <div class="form-contact tablet-mobile d-xl-none" style="background-color: <?php the_field('bgcolor_form_contact'); ?>">
                        <a href="tel:+34928909054" class="telefono-header-form"><i class="fas fa-phone"></i>928 90 90 54</a>
                        <?php echo do_shortcode( '[bkFormFoot observaciones="" imagen="" origen="14" origenForm="contacto" formulario="100"][/bkFormFoot]' ); ?>
                    </div>

                </div> <?php // .container ?>
            </div> <?php // . container-general-info-landing-promocion-campana ?>
            
         </div> <?php // .container-landing-promocion-campana ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-landing-promocion-campana ?>




<?php get_footer(); ?>