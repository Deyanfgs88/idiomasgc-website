<?php

/*

Template Name: Ubicaciones en GC

*/

get_header(); 
the_post(); ?>

<div class="template-ubica-gc">
    <div class="container-fluid">
        <div class="container-ubica-gc">
            <div class="cta-back-title-general-ubica-gc">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-ubica-gc">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_ubica_gc'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-ubica-gc">
                <?php
                    $titulo_ubica_gc = get_field('titulo_ubica_gc');
                    $texto_ubica_gc = get_field('texto_ubica_gc');
                    if ($titulo_ubica_gc && $texto_ubica_gc){
                ?>
                <div class="container-donde-estamos">
                    <div class="titulo-ubica-gc">
                        <?php the_field('titulo_ubica_gc'); ?>
                    </div>
                    <?php the_field('texto_ubica_gc'); ?>
                </div> <?php // .container-donde-estamos ?>
                <?php } ?>

                <div class="container-sedes">
                    <div class="row">

                        <div class="col-12 col-lg-4">
                            <?php
                                $titulo_sede_tomas_morales = get_field('titulo_sede_tomas_morales_ubica_gc');
                                $imagen_sede_tomas_morales = get_field('imagen_sede_tomas_morales_ubica_gc');
                                if ($titulo_sede_tomas_morales && $imagen_sede_tomas_morales){
                            ?>
                            <div class="cont-sede">
                                <div class="sede">
                                    <div class="titulo-sede">
                                        <?php the_field('titulo_sede_tomas_morales_ubica_gc'); ?>
                                    </div>
                                    <a href="<?php the_field('pagina_sede_tomas_morales_ubica_gc'); ?>">
                                        <div class="imagen-bg-sede" style="background-image: url('<?php the_field('imagen_sede_tomas_morales_ubica_gc'); ?>')">
                                            <div class="overlay"><i class="fas fa-chevron-right"></i></div>
                                        </div>
                                    </a>
                                    <div class="ctas-info-como-llegar">
                                        <a href="<?php the_field('pagina_sede_tomas_morales_ubica_gc'); ?>">Más información</a>
                                        <a href="<?php the_field('boton_como_llegar_sede_tomas_morales_ubica_gc'); ?>" target="_blank">Cómo llegar</a>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div> <?php // .col ?>

                        <div class="col-12 col-lg-4">
                            <?php
                                $titulo_sede_mesa_lopez = get_field('titulo_sede_mesa_lopez_ubica_gc');
                                $imagen_sede_mesa_lopez = get_field('imagen_sede_mesa_lopez_ubica_gc');
                                if ($titulo_sede_mesa_lopez && $imagen_sede_mesa_lopez){
                            ?>
                            <div class="cont-sede">
                                <div class="sede">
                                    <div class="titulo-sede">
                                        <?php the_field('titulo_sede_mesa_lopez_ubica_gc'); ?>
                                    </div>
                                    <a href="<?php the_field('pagina_sede_mesa_lopez_ubica_gc'); ?>">
                                        <div class="imagen-bg-sede" style="background-image: url('<?php the_field('imagen_sede_mesa_lopez_ubica_gc'); ?>')">
                                            <div class="overlay"><i class="fas fa-chevron-right"></i></div>
                                        </div>
                                    </a>
                                    <div class="ctas-info-como-llegar">
                                        <a href="<?php the_field('pagina_sede_mesa_lopez_ubica_gc'); ?>">Más información</a>
                                        <a href="<?php the_field('boton_como_llegar_sede_mesa_lopez_ubica_gc'); ?>" target="_blank">Cómo llegar</a>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div> <?php // .col ?>

                        <div class="col-12 col-lg-4">
                            <?php
                                $titulo_sede_siete_palmas = get_field('titulo_sede_siete_palmas_ubica_gc');
                                $imagen_sede_siete_palmas = get_field('imagen_sede_siete_palmas_ubica_gc');
                                if ($titulo_sede_siete_palmas && $imagen_sede_siete_palmas){
                            ?>
                            <div class="cont-sede">
                                <div class="sede">
                                    <div class="titulo-sede">
                                        <?php the_field('titulo_sede_siete_palmas_ubica_gc'); ?>
                                    </div>
                                    <a href="<?php the_field('pagina_sede_siete_palmas_ubica_gc'); ?>">
                                        <div class="imagen-bg-sede" style="background-image: url('<?php the_field('imagen_sede_siete_palmas_ubica_gc'); ?>')">
                                            <div class="overlay"><i class="fas fa-chevron-right"></i></div>
                                        </div>
                                    </a>
                                    <div class="ctas-info-como-llegar">
                                        <a href="<?php the_field('pagina_sede_siete_palmas_ubica_gc'); ?>">Más información</a>
                                        <a href="<?php the_field('boton_como_llegar_sede_siete_palmas_ubica_gc'); ?>" target="_blank">Cómo llegar</a>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div> <?php // .col ?>

                    </div> <?php // .row ?>
                </div> <?php // .container-sedes ?>

            </div> <?php // . container-general-info-ubica-gc ?>
            
         </div> <?php // .container-ubica-gc ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-ubica-gc ?>


<?php get_footer(); ?>