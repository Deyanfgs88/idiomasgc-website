(function() {
    /* * * * * * * * * * * * * * * * Variables */


    /* * * * * * * * * * * * * * * * Functions */


    /* * * * * * * * * * * * * * * * Events  */

    jQuery(function($) {
        $(document).ready(function() {

        });
    });

}).call(this);

jQuery(document).ready(function() {
    // Add Border Bottom color when scroll
    jQuery(window).scroll(function() {
        var scroll = jQuery(window).scrollTop();
        if (scroll >= 10) {
            jQuery(".container-header").addClass("border-down");
        } else {
            jQuery(".container-header").removeClass("border-down");
        }
    });

    // h2 and h3 for mobile on homepage
    /*if (jQuery(window).width() < 767){
        jQuery("#h2-homepage-tablet-desktop").remove();
        jQuery("#h3-homepage-tablet-desktop").remove();
    }else{
        jQuery("#h2-homepage-mobile").remove();
        jQuery("#h3-homepage-mobile").remove();
    }*/

    // contact form on promociones campaña landing page
    if (jQuery(window).width() > 1199){
        jQuery(".template-landing-promocion-campana .form-contact.tablet-mobile").remove();
    } else {
        jQuery(".template-landing-promocion-campana .container-form-contact-bg.desktop").remove();
    }

    // add css bg and text color field for button form contact on campaña landing page
    var formsubmitbotonbg = MyFields[0].bgcolorformsubmit;
    var formsubmitbotontextcolor = MyFields[1].textcolorformsubmit;
    jQuery(".page-template-template-landing-promocion-campana .form-contact .button-submit a").css({"background-color": formsubmitbotonbg, "border-color": formsubmitbotonbg, "color": formsubmitbotontextcolor});

 
    // Select course item on courses templates
    jQuery(".seleccionar-curso").on('change', function(){
        jQuery('body,html').animate({ scrollTop: jQuery('#' + jQuery(this).val()).position().top });
    });
    
});