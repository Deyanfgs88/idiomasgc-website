<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idiomasgc
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="google-site-verification" content="fxP6UIvU21n-YpvoB3pJxKlBTIp5lB5FR9mrWs7PTXs" />
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" defer='defer'></script>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

	<?php
		/**
		 *  Custom Title
		 */
		$title = get_post_meta($post->ID, 'etiquetatitle_83938', true);
		if ($title){
			echo '<title>' . $title . '</title>';
		} else{
			echo '<title>';
			wp_title('');
			echo '</title>';
		}
	?>

	<?php wp_head(); ?>


	<script>
	
		/**
		 *  VALIDATE ORION FORM
		 */

		function validar(formaFrom) {
			var elFo = formaFrom;
			var pass = true;
			var mensaje = 'Se han detectado los siguientes errores:nn';
			if(document.forms["forma"+elFo]["nombre"].value == '') {
				pass = false;
				document.forms["forma"+elFo]["nombre"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["nombre"].style.background="none";
			}
			if(document.forms["forma"+elFo]["apellidos"].value == '') {
				pass = false;
				document.forms["forma"+elFo]["apellidos"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["apellidos"].style.background="none";
			}
			if(document.forms["forma"+elFo]["comentarios"].value == '') {
				pass = false;
				document.forms["forma"+elFo]["comentarios"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["comentarios"].style.background="none";
			}
			/*var reg = /^([A-Za-z0-9_-.])+@([A-Za-z0-9_-.])+.([A-Za-z]{2,4})$/;*/
			correo = document.forms["forma"+elFo]["emilio"].value;
			if(correo == '') {
				pass = false;
				document.forms["forma"+elFo]["emilio"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["emilio"].style.background="none";
			}
			if(document.forms["forma"+elFo]["telefono"].value == '' ) {
				pass = false;
				document.forms["forma"+elFo]["telefono"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["telefono"].style.background="none";
			}
			
			if(document.forms["forma"+elFo]["donde"].value == '0') {
				pass = false;
				document.forms["forma"+elFo]["donde"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["donde"].style.background="none";
			}
			if(document.forms["forma"+elFo]["idioma"].value == '0') {
				pass = false;
				document.forms["forma"+elFo]["idioma"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["idioma"].style.background="none";
			}
			if(!document.forms["forma"+elFo]["politica"].checked) {
				pass = false;
				document.getElementById("alert"+elFo).style.display="block";
				
			} else {
				document.getElementById("alert"+elFo).style.display="none";
			}

			if (pass) {
				document.forms["forma"+elFo].action = "https://www.lcidiomasgc.com/gracias/";
				document.forms["forma"+elFo].submit();
			}
		return
		}
	</script>

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "LocalBusiness",
			"name": "Idiomas GC Tomás Morales",
			"image": "https://idiomasgc.com/wp-content/uploads/2018/09/logo-idiomasgc-lc.png",
			"@id": "",
			"url": "https://idiomasgc.com",
			"telephone": "+34928909054",
			"address": {
				"@type": "PostalAddress",
				"streetAddress": "Paseo Tomás Morales, 50",
				"addressLocality": "Las Palmas de Gran Canaria",
				"postalCode": "35003",
				"addressCountry": "ES"
			}
		}
	</script>

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "LocalBusiness",
			"name": "Idiomas GC Mesa y López",
			"image": "https://idiomasgc.com/wp-content/uploads/2018/09/logo-idiomasgc-lc.png",
			"@id": "",
			"url": "https://idiomasgc.com",
			"telephone": "+34928909054",
			"address": {
				"@type": "PostalAddress",
				"streetAddress": "Calle República Dominicana, 17",
				"addressLocality": "Las Palmas de Gran Canaria",
				"postalCode": "35010",
				"addressCountry": "ES"
			}
		}
	</script>

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "LocalBusiness",
			"name": "Idiomas GC 7 Palmas",
			"image": "https://idiomasgc.com/wp-content/uploads/2018/09/logo-idiomasgc-lc.png",
			"@id": "",
			"url": "https://idiomasgc.com",
			"telephone": "+34928909054",
			"address": {
				"@type": "PostalAddress",
				"streetAddress": "Avenida Pintor Felo Monzón, Calle Lomo San Lázaro, 27",
				"addressLocality": "Las Palmas de Gran Canaria",
				"postalCode": "35019",
				"addressCountry": "ES"
			}
		}
	</script>

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '2074792849444558');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=2074792849444558&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header>
		<div class="container-header">

			<div class="container-logo">
				<?php
					$custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
					?>
					<a href="<?php echo get_home_url(); ?>">
						<img src="<?php echo $logo[0]; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
					</a>
			</div> <?php //.container-logo ?>

			<div class="container-sobre-igc-ubic-menu">

				<div class="sobre-ubic">
					<?php
						$link_sobre_igc = get_theme_mod('sobre_igc');
						$texto_sobre_igc = get_theme_mod('texto_sobre_igc');
						$link_ubicaciones_gc = get_theme_mod('ubicaciones_gc');
						$texto_ubicaciones_gc = get_theme_mod('texto_ubicaciones_gc');
					?>
					<a href="<?php echo get_permalink($link_sobre_igc); ?>"><?php echo $texto_sobre_igc; ?></a>
					<a href="<?php echo get_permalink($link_ubicaciones_gc); ?>"><?php echo $texto_ubicaciones_gc; ?></a>
				</div>

				<div class="phone-menu">

					<div class="phone">
						<?php  
							$telefono = get_theme_mod('phone');
							$tel_sin_espacios = str_replace(' ', '', $telefono); 
						?>
						<a href="tel:+34<?php echo $tel_sin_espacios; ?>"><i class="fas fa-phone"></i><?php echo $telefono; ?></a>
					</div>

					<div class="menu-desktop">
						<?php wp_nav_menu( array( 'theme_location' => 'menu-principal') ); ?>
					</div> <?php // .menu-desktop ?>

					<div class="menu-mobile">

						<!-- Icon Burguer -->
						<div class="icon-burguer-menu">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mydropdown" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
  						</button> 
						</div>
						<!-- .Icon Burguer -->
						
					</div> <?php //.menu-mobile ?>

				</div> <?php // .phone-menu ?>

				<div class="collapse navbar-collapse custom-menu-mobile" id="mydropdown">
					<?php wp_nav_menu( array( 'theme_location' => 'menu-movil') ); ?>
				</div>

			</div> <?php //. container-sobre-igc-ubic-menu ?>

		</div> <?php //.container-header ?>

	</header> <?php // .header ?>

	<div id="content" class="site-content">
