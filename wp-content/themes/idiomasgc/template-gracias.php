<?php

/*

Template Name: Gracias

*/

get_header(); 
the_post(); ?>

<div class="template-gracias">
    <div class="container-fluid">
        <div class="container-gracias">
            <div class="cta-back-title-general-gracias">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
            </div>

            <div class="container-general-info-gracias">

                <div class="imagen-mobile d-block d-lg-none">
                    <img src="<?php the_field('imagen_background_gracias'); ?>" alt="imagen gracias">
                </div> <?php // .imagen-mobile ?>
            
                <div class="container-bg-gracias" style="background-image: url('<?php the_field('imagen_background_gracias'); ?>');">
                    <div class="cont-texto-gracias">
                        <div class="texto">
                            <?php
                                $texto_principal = get_field('texto_principal_gracias');
                                if ($texto_principal){
                            ?>
                            <div class="item">
                                <?php the_field('texto_principal_gracias'); ?>
                            </div>
                            <?php } ?>

                            <?php
                                $texto_secundario = get_field('texto_secundario_gracias');
                                if ($texto_secundario){
                            ?>
                            <div class="item">
                                <?php the_field('texto_secundario_gracias'); ?>
                            </div>
                            <?php } ?>
                        </div> <?php // .texto ?>
                    </div> <?php // cont-texto-gracias ?>
                </div> <? // .container-bg-gracias ?>

            </div> <?php // . container-general-info-gracias ?>
            
         </div> <?php // .container-gracias ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-gracias ?>




<?php get_footer(); ?>