<?php

/*

Template Name: Idiomas y formación en el extranjero

*/

get_header(); 
the_post(); ?>

<div class="template-idiomas-extranjero">
    <div class="container-fluid">
        <div class="container-idiomas-extranjero">
            <div class="cta-back-title-general-idiomas-extranjero">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-idiomas-extranjero">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_idiomas_extra'); ?></h2>
                </div>          
            </div>

            <div class="container-general-info-idiomas-extranjero">
                <?php
                    $imagen_portada_idiomas_extra = get_field('imagen_portada_idiomas_extra');
                    if ($imagen_portada_idiomas_extra){
                ?>
                <div class="imagen-portada">
                    <img src="<?php the_field('imagen_portada_idiomas_extra'); ?>" alt="imagen portada idiomas extranjero">
                </div>
                <?php } ?>

                <?php
                    $contenido_idiomas_extra = get_field('contenido_idiomas_extra');
                    if ($contenido_idiomas_extra){
                ?>
                <div class="contenido-idiomas-extra">
                    <?php the_field('contenido_idiomas_extra'); ?>
                </div>
                <?php } ?>

                <?php
                    $cta_solicita_informacion = get_field('boton_solicita_info_idiomas_extra');
                    if ($cta_solicita_informacion){
                ?>
                <div class="cta-informacion">
                    <a href="<?php the_field('boton_solicita_info_idiomas_extra'); ?>">Solicita más información</a>
                </div>
                <?php } ?>

            </div>   

        </div> <?php // .container-idiomas-extranjero ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-idiomas-extranjero ?>



<?php get_footer(); ?>