<?php

/*

Template Name: Destacamos esta semana

*/

get_header(); 
the_post(); ?>

<div class="template-destacamos-semana">
    <div class="container-fluid">
        <div class="container-destacamos-semana">
            <div class="cta-back-title-general-destacamos-semana">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-destacamos-semana">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_destaca_semana'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-destacamos-semana">

                <?php
                    $title_destaca_semana = get_field('titulo_destaca_semana');
                    if ($title_destaca_semana){
                ?>
                <div class="subtitle-destacamos-semana">
                    <h2><i class="fas fa-chevron-right"></i><?php the_field('titulo_destaca_semana'); ?></h2>
                </div>
                <?php } ?>

                <div class="imagen-text-portada-destacamos-semana">
                    <img src="<?php the_field('imagen_portada_destaca_semana'); ?>" alt="imagen portada destacamos semana">
                    <div class="text-info-destacamos-semana">
                        <?php the_field('texto_informacion_destaca_semana'); ?>
                    </div>

                </div> <?php // .imagen-text-cta-portada-destacamos-semana ?>

            </div> <?php // . container-general-info-destacamos-semana ?>
            
         </div> <?php // .container-destacamos-semana ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-destacamos-semana ?>


<?php get_footer(); ?>