<?php

/*

Template Name: Exámenes Oficiales

*/

get_header(); 
the_post(); ?>

<div class="template-cursos">
    <div class="container-fluid">
        <div class="container-curso">
            <div class="cta-back-title-general-curso">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-curso">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_page_exam_oficiales'); ?></h2>
                </div>
            </div>

            <div class="container-general-class">

                <div class="container-clases">
                    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_exam_oxford'); ?>" alt="imagen examen oxford">
                            <a href="<?php the_field('pagina_mas_informacion_exam_oxford'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_exam_oxford'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_exam_oxford'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_exam_oxford'); ?>">Más Información</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_exam_cambridge'); ?>" alt="imagen examen cambridge">
                            <a href="<?php the_field('pagina_mas_informacion_exam_cambridge'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_exam_cambridge'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_exam_cambridge'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_exam_cambridge'); ?>">Más Información</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_exam_ielts'); ?>" alt="imagen examen IELTS">
                            <a href="<?php the_field('pagina_mas_informacion_exam_ielts'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_exam_ielts'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_exam_ielts'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_exam_ielts'); ?>">Más Información</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
                    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_exam_toefl'); ?>" alt="imagen examen TOEFL">
                            <a href="<?php the_field('pagina_mas_informacion_exam_toefl'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_exam_toefl'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_exam_toefl'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_exam_toefl'); ?>">Más Información</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_exam_delf_dalf'); ?>" alt="imagen examen DELF-DALF">
                            <a href="<?php the_field('pagina_mas_informacion_exam_delf_dalf'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_exam_delf_dalf'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_exam_delf_dalf'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_exam_delf_dalf'); ?>">Más Información</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_exam_goethe'); ?>" alt="imagen examen goethe">
                            <a href="<?php the_field('pagina_mas_informacion_exam_goethe'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_exam_goethe'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_exam_goethe'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_exam_goethe'); ?>">Más Información</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                </div> <?php // .container-clases ?>
            </div> <?php // .container-general-class ?>


        </div> <?php // .container-curso ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-cursos ?>



<?php get_footer(); ?>