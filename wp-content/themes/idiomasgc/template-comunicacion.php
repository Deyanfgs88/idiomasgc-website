<?php

/*

Template Name: Comunicación

*/

get_header(); 
the_post(); ?>

<div class="template-comunicacion">
    <div class="container-fluid">
        <div class="container-comunicacion">
            <div class="cta-back-title-general-comunicacion">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-comunicacion">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_comunicacion'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-comunicacion">

                <?php
                    $info_comunicacion = get_field('texto_comunicacion');
                    if ($info_comunicacion){
                ?>
                <div class="info-comunicacion">
                    <?php the_field('texto_comunicacion') ?>
                </div>
                <?php } ?>

                <div class="container-formulario">
                    <div class="formulario">
                        <?php the_field('formulario_comunicacion'); ?>
                    </div> <?php // .formulario ?>
                </div>

            </div> <?php // . container-general-info-comunicacion ?>
            
         </div> <?php // .container-comunicacion ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-comunicacion ?>




<?php get_footer(); ?>