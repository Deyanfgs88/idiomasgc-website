<?php

/*

Template Name: Contacto Orion

*/

get_header(); 
the_post(); ?>

<div class="template-contacto-orion">
    <div class="container-fluid">
        <div class="container-contacto-orion">
            <div class="cta-back-title-general-contacto-orion">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-contacto-orion">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_contacto_orion'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-contacto-orion">

                <?php
                    $texto_contacto = get_field('texto_principal_contacto_orion');
                    if ($texto_contacto){
                ?>
                <div class="texto-contacto">
                    <h3><?php the_field('texto_principal_contacto_orion'); ?></h3>
                </div>
                <?php } ?>

                <div class="imagen-mobile d-block d-xl-none">
                    <img src="<?php the_field('imagen_background_contacto_orion'); ?>" alt="imagen contacto">
                </div> <?php // .imagen-mobile ?>

                <div class="container-bg-formulario" style="background-image: url('<?php the_field('imagen_background_contacto_orion'); ?>');">
                    <div class="formulario">
                        <?php echo do_shortcode( '[bkFormFoot observaciones="" imagen="" origen="14" origenForm="contacto" formulario="100"][/bkFormFoot]' ); ?>
                    </div>
                </div>

            </div> <?php // . container-general-info-contacto-orion ?>
            
         </div> <?php // .container-contacto-orion ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-contacto-orion ?>


<?php get_footer(); ?>