<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package idiomasgc
 */

get_header();
?>


<div class="template-error-404">
	<div class="container-fluid">
		<div class="container-error-404">

			<div class="cta-back-error-404">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
            </div>

			<div class="container-general-info-error-404">

				<div class="page-title">
					<h1><span>Oops! </span><?php esc_html_e( 'La página no ha podido ser encontrada.', 'idiomasgc' ); ?></h1>
				</div>
				<div class="text-404">
					<p>404</p>
				</div>
				<div class="cont-search">
					<p><?php esc_html_e( 'Parece que nada ha sido encontrado en esta ubicación. ¿Podrías probrar uno de los links de abajo o en la búsqueda?', 'idiomasgc' ); ?></p>
					<div class="search">
						<?php get_search_form(); ?>
					</div>
				</div>
				
				<div class="cont-recent-post">
					<h2>Entradas recientes</h2>
					<ul>
					<?php
						$recent_posts = wp_get_recent_posts();
						foreach( $recent_posts as $recent ){
							echo '<li><i class="fas fa-chevron-right"></i><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
						}
						wp_reset_query();
					?>
					</ul>
				</div>

				<div class="cont-archivos">
					<?php
					/* translators: %1$s: smiley */
					$idiomasgc_archive_content = '<p>' . sprintf( esc_html__( 'Intenta buscar en los archivos mensuales. %1$s', 'idiomasgc' ), convert_smilies( ':)' ) ) . '</p>';
					the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$idiomasgc_archive_content" );

					the_widget( 'WP_Widget_Tag_Cloud' );
					?>
				</div>
			
			</div> <?php // .container-general-info-error-404 ?>

		</div> <?php // .container-error-404 ?>
	</div> <?php // .container-fluid ?>
</div> <?php // .template-error-404 ?>


<?php get_footer(); ?>
