<?php

/*

Template Name: Incompany Lista Cursos

*/

get_header(); 
the_post(); ?>

<div class="template-incompany-lista-cursos">
    <div class="container-fluid">
        <div class="container-incompany-lista-cursos">
            <div class="cta-back-title-general-incompany-lista-cursos">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pagina_volver_lista_cursos_incompany'); ?>"><i class="fas fa-chevron-left"></i>Volver</a>
                </div>
                <div class="title-incompany-lista-cursos">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_incompany_lista_cursos'); ?></h2>
                </div>
            </div>

            <div class="container-general-incompany-lista-cursos">

                <h3>Elige un curso</h3>

                <div class="lista-cursos">
                    <div class="cont-lista">
                        <?php
                            $lista_cursos_incompany = get_field('lista_cursos_incompany');
                            if ($lista_cursos_incompany){
                                echo '<ul>';
                                foreach ($lista_cursos_incompany as $curso_incompany) {
                                    echo '<li><i class="fas fa-chevron-right"></i><a href="' . $curso_incompany['pagina_cursos_incompany'] .'">' . $curso_incompany['texto_cursos_incompany'] . '</a></li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                    </div> <?php // .cont-lista ?>
                </div> <?php // .lista-cursos ?>

            </div> <?php // . container-general-incompany-lista-cursos ?>
            
         </div> <?php // .container-incompany-lista-cursos ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-incompany-lista-cursos ?>


<?php get_footer(); ?>