<?php

/*

Template Name: Actividades Extraescolares y campamentos de verano

*/

get_header(); 
the_post(); ?>

<div class="template-actividades-campa-verano template-cursos-info">
    <div class="container-fluid">
        <div class="container-curso-info">
            <div class="cta-back-title-general-curso">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pag_todos_programas_campa_veran_activ') ?>"><i class="fas fa-chevron-left"></i>Todos los programas</a>
                </div>
                <div class="title-curso-info">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_camp_veran_activ'); ?></h2>
                </div>
            </div>

            <div class="container-general-class-info">

                <div class="container-clase-info activ-campa-verano">
                    
                    <div class="cont-clase-info">
                        <div class="clase-info">
                            <div class="row">

                                <div class="col-12 col-lg-4">
                                    <div class="info left">
                                        <img src="<?php the_field('imagen_principal_camp_veran_activ'); ?>" alt="imagen campamentos verano actividades">
                                        <div class="titulo-curso">
                                            <?php the_field('titulo_camp_veran_activ'); ?>
                                        </div>
                                        <div class="small-info">
                                            <?php the_field('texto_principal_camp_veran_activ'); ?>
                                        </div>
                                        <div class="ctas-campa-verano-actividades">
                                            <a href="<?php the_field('pagina_reserva_ahora_camp_veran_activ'); ?>">Reserva ahora</a>
                                            <a href="<?php the_field('pagina_ver_info_camp_veran_activ'); ?>">Ver info</a>
                                        </div>
                                    </div> <? // .info ?>
                                </div> <?php // .col ?>

                                <div class="col-12 col-lg-8">
                                    <div class="info right">

                                        <div class="gallery-images-actividades">
                                            <?php the_field('galeria_actividades'); ?>
                                        </div>

                                        <!--
                                        <div class="lista-actividades-camp-verano">
                                            <?php /*
                                                $lista_actividades_camp_verano = get_field('lista_camp_veran_activ');
                                                if ($lista_actividades_camp_verano){
                                                    foreach ($lista_actividades_camp_verano as $actividad) {
                                                        echo '<div class="actividad">';
                                                        echo '<div class="imagen"><div class="bg-imagen" style="background-image: url(' . $actividad['imagen_activ_camp_verano'] . ');"></div></div>';
                                                        echo '<div class="info">';
                                                        echo '<h3>' . $actividad['titulo_activ_camp_verano'] . '</h3>';
                                                        echo $actividad['texto_activ_camp_verano'];
                                                        echo '</div>'; // .info
                                                        echo '</div>'; // .actividad
                                                    }
                                                } */
                                            ?>
                                        </div> -->

                                    </div> <?php // .info ?>
                                </div> <?php // .col ?>
                            </div> <?php // .row ?>
                            
                        </div> <?php // .clase-info ?>
                    </div> <?php // .cont-clase-info ?>
    
                </div> <?php // .container-clase-info ?>
            </div> <?php // .container-general-class-info ?>


        </div> <?php // .container-curso-info ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-actividades-campa-verano ?>



<?php get_footer(); ?>