<?php

/*

Template Name: Cursos Online y Semipresenciales

*/

get_header(); 
the_post(); ?>

<div class="template-cursos-online-semi template-cursos">
    <div class="container-fluid">
        <div class="container-curso-online-semi">
            <div class="cta-back-title-general-curso-online-semi">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-curso-online-semi">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_curso_online'); ?></h2>
                </div>          
            </div> <?php // .cta-back-title-general-curso-online-semi ?>

            <div class="container-general-class-online-semi">

                <div class="container-clases-online-semi">

                    <div class="imagen-text-portada-cursos-online-semi">
                        <img src="<?php the_field('imagen_portada_curso_online'); ?>" alt="imagen portada cursos online y semipresenciales">
                        <div class="text-info-cursos-online-semi">
                            <?php the_field('texto_informacion_portada_curso_online'); ?>
                        </div>
                    </div> <?php // .imagen-text-portada-cursos-online-semi ?>
                    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_ingles_online'); ?>" alt="imagen curso inglés online">
                            <a href="<?php the_field('pagina_mas_informacion_curso_ingles_online'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_ingles_online'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_curso_ingles_online'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_curso_ingles_online'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_ingles_online'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_semi_basico'); ?>" alt="imagen curso semipresencial básico">
                            <a href="<?php the_field('pagina_mas_informacion_curso_semi_basico'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_semi_basico'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_curso_semi_basico'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_curso_semi_basico'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_semi_basico'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_semi_premium'); ?>" alt="imagen curso semipresencial premium">
                            <a href="<?php the_field('pagina_mas_informacion_curso_semi_premium'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_semi_premium'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_curso_semi_premium'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_curso_semi_premium'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_semi_premium'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
                    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_semi_platinum'); ?>" alt="imagen curso semipresencial platinum">
                            <a href="<?php the_field('pagina_mas_informacion_curso_semi_platinum'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_semi_platinum'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_curso_semi_platinum'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_curso_semi_platinum'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_semi_platinum'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
    
                </div> <?php // .container-clases-online-semi ?>
            </div> <?php // .container-general-class-online-semi ?>


        </div> <?php // .container-curso-online-semi ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-cursos-online-semi ?>


<?php get_footer(); ?>