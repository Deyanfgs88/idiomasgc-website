<?php

/*

Template Name: Curso Universitario y Adultos

*/

get_header(); 
the_post(); ?>

<div class="template-cursos">
    <div class="container-fluid">
        <div class="container-curso">
            <div class="cta-back-title-general-curso">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-curso">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_curso_uni_adultos'); ?></h2>
                </div>
            </div>

            <?php
                $texto_select_course = get_field('texto_seleccionar_curso');
                $list_select_courses = get_field('lista_seleccionar_cursos');
            ?>
            <div class="container-seleccionar-item-curso">
                <?php
                    if ($texto_select_course) {
                ?>
                <p><?php the_field('texto_seleccionar_curso'); ?></p>
                <?php } ?>
                <?php
                    if ($list_select_courses) {
                ?>
                <select class="seleccionar-curso">
                    <?php
                        $i = 0;
                        foreach ($list_select_courses as $course) {
                            $i++;
                            echo '<option value="curso-' . $i .'">' . $course['texto_curso'] . '</option>';
                        }
                    ?>
                </select>
                <?php } ?>
            </div> <?php // .container-seleccionar-item-curso ?>

            <div class="container-general-class">

                <div class="container-clases">

                    <div id="curso-1" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_ano_academico_uni_adultos'); ?>" alt="imagen curso año académico">
                            <a href="<?php the_field('pagina_mas_info_ano_academico_uni_adultos'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_ano_academico_uni_adultos'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_ano_academico = get_field('duracion_ano_academico');
                                if ($duracion_ano_academico){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_ano_academico'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_ano_academico_uni_adultos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_info_ano_academico_uni_adultos'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_ano_academico_uni_adultos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
                    
                    <div id="curso-2" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_general_uni_adultos'); ?>" alt="imagen curso general universitarios y adultos">
                            <a href="<?php the_field('pagina_mas_info_curso_general_uni_adultos'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_general_uni_adultos'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_curso_general = get_field('duracion_curso_general');
                                if ($duracion_curso_general){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_curso_general'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_curso_general_uni_adultos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_info_curso_general_uni_adultos'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_general_uni_adultos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div id="curso-3" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_general_plus_uni_adultos'); ?>" alt="imagen curso general plus universitarios y adultos">
                            <a href="<?php the_field('pagina_mas_info_curso_general_plus_uni_adultos'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_general_plus_uni_adultos'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_curso_general_plus = get_field('duracion_curso_general_plus');
                                if ($duracion_curso_general_plus){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_curso_general_plus'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_curso_general_plus_uni_adultos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_info_curso_general_plus_uni_adultos'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_general_plus_uni_adultos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div id="curso-4" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_intensivo_uni_adultos'); ?>" alt="imagen curso intensivo universitarios y adultos">
                            <a href="<?php the_field('pagina_mas_info_curso_intensivo_uni_adultos'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_intensivo_uni_adultos'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_curso_intensivo = get_field('duracion_curso_intensivo');
                                if ($duracion_curso_intensivo){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_curso_intensivo'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_curso_intensivo_uni_adultos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_info_curso_intensivo_uni_adultos'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_intensivo_uni_adultos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
                    
                    <div id="curso-5" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_prepara_exam_oficiales_uni_adultos'); ?>" alt="imagen preparación exámenes oficiales universitarios y adultos">
                            <a href="<?php the_field('pagina_curso_info_prepara_exam_oficiales_uni_adultos'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_prepara_exam_oficiales_uni_adultos'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_prepara_exam_oficiales = get_field('duracion_prepara_exam_oficiales');
                                if ($duracion_prepara_exam_oficiales){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_prepara_exam_oficiales'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_prepara_exam_oficiales_uni_adultos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_curso_info_prepara_exam_oficiales_uni_adultos'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_curso_reserva_ahora_prepara_exam_oficiales_uni_adultos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-6" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_mini_grupo_uni_adultos'); ?>" alt="imagen curso mini grupos universitarios y adultos">
                            <a href="<?php the_field('pagina_mas_info_curso_mini_grupo_uni_adultos'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_mini_grupo_uni_adultos'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_mini_grupo = get_field('duracion_mini_grupo');
                                if ($duracion_mini_grupo){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_mini_grupo'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_curso_mini_grupo_uni_adultos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_info_curso_mini_grupo_uni_adultos'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_mini_grupo_uni_adultos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-7" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_individual_uni_adultos'); ?>" alt="imagen curso individual universitarios y adultos">
                            <a href="<?php the_field('pagina_mas_info_curso_individual_uni_adultos'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_individual_uni_adultos'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_curso_individual = get_field('duracion_curso_individual');
                                if ($duracion_curso_individual){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_curso_individual'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_curso_individual_uni_adultos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_info_curso_individual_uni_adultos'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_individual_uni_adultos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-8" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_conversacion_uni_adultos'); ?>" alt="imagen curso conversación universitarios y adultos">
                            <a href="<?php the_field('pagina_mas_info_curso_conversacion_uni_adultos'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_conversacion_uni_adultos'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_curso_conversacion = get_field('duracion_curso_conversacion');
                                if ($duracion_curso_conversacion){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_curso_conversacion'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_curso_conversacion_uni_adultos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_info_curso_conversacion_uni_adultos'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_conversacion_uni_adultos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-9" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_conversacion_plus_uni_adultos'); ?>" alt="imagen curso conversación plus universitarios y adultos">
                            <a href="<?php the_field('pagina_mas_info_curso_conversacion_plus_uni_adultos'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_conversacion_plus_uni_adultos'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_curso_conversacion_plus = get_field('duracion_curso_conversacion_plus');
                                if ($duracion_curso_conversacion_plus){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_curso_conversacion_plus'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_curso_conversacion_plus_uni_adultos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_info_curso_conversacion_plus_uni_adultos'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_conversacion_plus_uni_adultos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-10" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_verano_general_uni_adultos'); ?>" alt="imagen curso verano general universitarios y adultos">
                            <a href="<?php the_field('pagina_mas_info_curso_verano_general_uni_adultos'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_verano_general_uni_adultos'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_curso_verano = get_field('duracion_curso_verano');
                                if ($duracion_curso_verano){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_curso_verano'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_curso_verano_general_uni_adultos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_info_curso_verano_general_uni_adultos'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_verano_general_uni_adultos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-11" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_verano_intensivo_uni_adultos'); ?>" alt="imagen curso verano intensivo universitarios y adultos">
                            <a href="<?php the_field('pagina_mas_info_curso_verano_intensivo_uni_adultos'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_verano_intensivo_uni_adultos'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_curso_verano_intensivo = get_field('duracion_curso_verano_intensivo');
                                if ($duracion_curso_verano_intensivo){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_curso_verano_intensivo'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_curso_verano_intensivo_uni_adultos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_info_curso_verano_intensivo_uni_adultos'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_curso_verano_intensivo_uni_adultos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
    
                </div> <?php // .container-clases ?>
            </div> <?php // .container-general-class ?>

        </div> <?php // .container-curso ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-cursos ?>



<?php get_footer(); ?>