<?php

/*

Template Name: Cursos Seniors

*/

get_header(); 
the_post(); ?>

<div class="template-cursos">
    <div class="container-fluid">
        <div class="container-curso">
            <div class="cta-back-title-general-curso">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-curso">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_curso'); ?></h2>
                </div>          
            </div>

            <div class="container-general-class">

                <div class="container-clases">
                    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_programa_ingles_mayores_50'); ?>" alt="imagen programa inglés +50">
                            <a href="<?php the_field('pagina_curso_informacion_programa_ingles_mayores_50'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_programa_ingles_mayores_50'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_programa_ingles_mayores_50'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_curso_informacion_programa_ingles_mayores_50'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_curso_reserva_ahora_programa_ingles_mayores_50'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
                                   
                </div> <?php // .container-clases ?>
            </div> <?php // .container-general-class ?>


        </div> <?php // .container-curso ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-cursos ?>



<?php get_footer(); ?>