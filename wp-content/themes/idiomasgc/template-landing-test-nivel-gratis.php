<?php

/*

Template Name: Landing Test Nivel Gratis

*/

get_header(); 
the_post(); ?>

<div class="template-landing-test-nivel-gratis">
    <div class="container-fluid">
        <div class="container-landing-test-nivel-gratis">
            
            <div class="container-general-info-landing-test-nivel-gratis">
                <div class="container">

                    <div class="imagen-portada-mobile-tablet d-xl-none">
                        <img src="<?php the_field('imagen_portada_test_nivel'); ?>" alt="imagen portada test de nivel gratis">
                    </div> <?php // .imagen-portada-mobile-tablet ?>

                    <div class="container-info-test-mobile-tablet d-xl-none">
                        <div class="image-icon-test-texto">
                            <img src="<?php the_field('imagen_icono_test_nivel'); ?>" alt="imagen icono test">
                            <div class="cont-conoce-nivel-text">
                                <div class="texto-test">
                                    <?php the_field('texto_conoce_test_nivel')?>
                                </div>
                            </div>
                        </div>
                        <a href="#test-nivel" class="cta-empieza-test"><?php the_field('texto_boton_empieza_test_nivel'); ?></a>
                    </div> <?php // .container-info-test-mobile-tablet ?>

                    <div class="title-principal-test mobile-tablet d-xl-none">
                        <div class="titulo">
                            <?php the_field('titulo_principal_test_nivel'); ?>
                        </div>
                    </div> <?php // .title-principal-test ?>

                    <div class="container-portada-test desktop imagen-bg d-none d-xl-flex" style="background-image: url('<?php the_field('imagen_portada_test_nivel'); ?>'); background-position: <?php the_field('posicion_portada_test_nivel'); ?>;">
                        <div class="container-info-test">
                            <img src="<?php the_field('imagen_icono_test_nivel'); ?>" alt="imagen icono test">
                            <div class="cont-conoce-nivel-text">
                                <div class="texto-test">
                                    <?php the_field('texto_conoce_test_nivel')?>
                                </div>
                            </div>
                            <a href="#test-nivel" class="cta-empieza-test"><?php the_field('texto_boton_empieza_test_nivel'); ?></a>
                        </div> <?php // .container-info-test-mobile-tablet ?>
                    </div> <?php // .container-form-contact ?>

                    <div class="title-principal-test desktop d-none d-xl-block">
                        <div class="titulo">
                            <?php the_field('titulo_principal_test_nivel'); ?>
                        </div>
                    </div> <?php // .title-principal-test ?>

                    <div class="cont-info-promocion">
                        <div class="info-promocion">
                            <div class="titulo">
                                <?php the_field('titulo_curso_idioma'); ?>
                            </div>
                            <?php the_field('texto_curso_idioma'); ?>
                        </div>
                    </div> <?php // .cont-info-promocion ?>

                    <div class="cont-info-promocion">
                        <div class="info-promocion">
                            <div class="titulo">
                                <?php the_field('titulo_aprende_idioma'); ?>
                            </div>
                            <?php the_field('texto_aprende_idioma'); ?>
                        </div>
                    </div> <?php // .cont-info-promocion ?>

                    <div id="test-nivel" class="cont-info-promocion">
                        <div class="info-promocion">
                            <div class="titulo">
                                <?php the_field('titulo_conoce_test_gratis'); ?>
                            </div>
                            <?php the_field('texto_conoce_test_gratis'); ?>
                            <div class="container-iframe-test-nivel">
                                <iframe src="<?php the_field('url_iframe_test_nivel'); ?>" width="100%" height="880" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div> <?php // .cont-info-promocion ?>

                    <div class="item-info nuestros-cursos">
                        <div class="titulo">
                            <?php the_field('titulo_tus_cursos_idiomasgc'); ?>
                        </div>
                        <?php
                            $lista_info_idiomasgc = get_field('lista_idiomasgc');
                            echo '<ul>';
                            foreach ($lista_info_idiomasgc as $lista_idiomasgc) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $lista_idiomasgc['info_idiomasgc'] . '</li>';
                            }
                            echo '</ul>';
                        ?>
                    </div> <?php // .nuestros-cursos ?>

                    <div class="item-info imagenes-partners">
                        <div class="titulo">
                            <?php the_field('titulo_partners_test_nivel'); ?>
                        </div>
                        <div class="partners">
                            <?php
                                $images = get_field('galeria_partners_test_nivel');
                                $size = 'full'; 
                                if( $images ): ?>
                                <ul>
                                <?php foreach( $images as $image ): ?>
                                    <li> <?php echo wp_get_attachment_image( $image['ID'], $size ); ?> </li>
                                <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div> <?php // .imagenes-partners ?>

                    <div class="item-info que-dicen-nosotros">
                        <div class="titulo">
                            <?php the_field('titulo_testimonios_idiomasgc'); ?>
                        </div>
                        <div class="texto d-xl-none">
                            <?php the_field('texto_testimonios_idiomasgc');?>
                        </div>
                        <div class="videos">
                            <iframe src="<?php the_field('url_testimonio_video_1'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            <iframe src="<?php the_field('url_testimonio_video_2'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div> <?php // .que-dicen-nosotros ?>

                    <div class="item-info cont-mapas">
                        <div class="titulo">
                            <?php the_field('titulo_localizacion_idiomasgc'); ?>
                        </div>
                        <div class="texto d-xl-none">
                            <?php the_field('texto_localizacion_idiomasgc'); ?>
                        </div>
                        <div class="mapas">
                            <div class="row">
                                <iframe src="<?php the_field('url_mapa_sede_sietepalmas'); ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                            <div class="row">
                                <iframe src="<?php the_field('url_mapa_sede_mesaylopez'); ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                            <div class="row">
                                <iframe src="<?php the_field('url_mapa_sede_tomasmorales'); ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div> <?php // .cont-mapas ?>

                </div> <?php // .container ?>
            </div> <?php // . container-general-info-landing-test-nivel-gratis ?>
            
         </div> <?php // .container-landing-test-nivel-gratis ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-landing-test-nivel-gratis ?>




<?php get_footer(); ?>