<?php

/*

Template Name: Tripartita

*/

get_header(); 
the_post(); ?>

<div class="template-formac-profesorado-info">
    <div class="container-fluid">
        <div class="container-formac-prof-info">
            <div class="cta-back-title-general-formac-info">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_formac_profesorado'); ?>"><i class="fas fa-chevron-left"></i>Formación del profesorado</a>
                </div>
                <div class="title-formac-prof">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_tripartita_info'); ?></h2>
                </div>
            </div>

            <div class="container-general-formac-prof-info">

                <div class="imagen-text-portada-formac-prof">
                    <img src="<?php the_field('imagen_tripartita_info'); ?>" alt="imagen portada tripartita">
                    <div class="text-info-formac-prof">
                        <?php the_field('texto_informacion_tripartita_info'); ?>
                    </div>
                </div> <?php // .imagen-text-portada-formac-prof ?>
                
                <div class="item-info-formac-prof ctas">
                    <a href="<?php the_field('boton_convenio_agrupacion_empresas_tripartita'); ?>" target="_blank">Descarga el convenio de agrupación de empresas</a>
                    <a href="<?php the_field('boton_ficha_inscripcion_sietepalmas_empresa_tripartita'); ?>" target="_blank">Descarga la ficha de inscripción de la empresa - 7palmas</a>
                    <a href="<?php the_field('boton_ficha_inscripcion_mesaylopez_empresa_tripartita'); ?>" target="_blank">Descarga la ficha de inscripción de la empresa - Mesa y López</a>
                    <a href="<?php the_field('boton_ficha_inscripcion_tomasmorales_empresa_tripartita'); ?>" target="_blank">Descarga la ficha de inscripción de la empresa - Tomás Morales</a>
                    <a href="<?php the_field('boton_reserva_ahora_tripartita'); ?>">Reserva ahora</a>
                </div>

            </div> <?php // . container-general-formac-prof-info ?>
            
         </div> <?php // .container-formac-prof-info ?>
    </div> <?php // .container-fluid ?>
</div><? // .template-formac-profesorado-info ?>


<?php get_footer(); ?>