<?php

/*

Template Name: Sede ubicación en Gran Canaria

*/

get_header(); 
the_post(); ?>

<div class="template-sede-gc">
    <div class="container-fluid">
        <div class="container-sede-gc">
            <div class="cta-back-title-general-sede-gc">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pagina_todas_escuelas_sede_gc'); ?>"><i class="fas fa-chevron-left"></i>Todas las escuelas</a>
                </div>
                <div class="title-sede-gc">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_sede_gc'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-sede-gc">
                <div class="row">

                    <div class="col-12 col-lg-6">
                        <div class="container-video-info">
                            <?php
                                $video_sede_gc = get_field('video_sede_gc');
                                if ($video_sede_gc){
                            ?>
                            <div class="video">
                                <iframe src="<?php the_field('video_sede_gc'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                            <?php } ?>

                            <?php
                                $texto_info_sede_gc = get_field('texto_info_sede_gc');
                                if ($texto_info_sede_gc){
                            ?>
                            <div class="texto-info-sede">
                                <?php the_field('texto_info_sede_gc'); ?>
                            </div>
                            <?php } ?>

                            <?php
                                $lista_info_sede_gc = get_field('lista_info_sede_gc');
                                if ($lista_info_sede_gc){
                            ?>
                            <div class="lista-info-sede">
                                <?php
                                    echo '<ul>';
                                    foreach ($lista_info_sede_gc as $item_info) {
                                        echo '<li><i class="fas fa-chevron-right"></i>' . $item_info['item_info_sede_gc'] . '</li>';
                                    }
                                    echo '</ul>';
                                ?>
                            </div>
                            <?php } ?>

                        </div> <?php // .container-video-info ?>
                    </div> <?php // .col ?>

                    <div class="col-12 col-lg-6">
                        <div class="container-mapa-info">

                            <?php
                                $mapa_sede_gc = get_field('mapa_sede_gc');
                                if ($mapa_sede_gc){
                            ?>
                            <div class="mapa">
                                <iframe src="<?php the_field('mapa_sede_gc'); ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                            <?php } ?>

                            <div class="cont-localizacion-horario">
                                <?php
                                    $direccion_sede = get_field('direccion_sede_gc');
                                    if ($direccion_sede){
                                ?>
                                <div class="item-info">
                                    <i class="fas fa-map-marker-alt"></i><?php the_field('direccion_sede_gc'); ?>
                                </div>
                                <?php } ?>
                                
                                <?php
                                    $telefono_sede = get_field('telefono_sede_gc');
                                    $tel_sin_espacios = str_replace(' ', '', $telefono_sede); 
                                    if ($telefono_sede){
                                ?>
                                <div class="item-info">
                                    <i class="fas fa-phone"></i><a href="tel:+34<?php echo $tel_sin_espacios; ?>"><?php the_field('telefono_sede_gc'); ?></a>
                                </div>
                                <?php } ?>

                                <?php
                                    $titulo_horario_academia = get_field('titulo_horario_academia_idiomas_sede_gc');
                                    $texto_horario_academia = get_field('texto_horario_academia_idiomas_sede_gc');
                                    if ($titulo_horario_academia && $texto_horario_academia){
                                ?>
                                <div class="item-info">
                                    <i class="fas fa-clock"></i>
                                    <div class="info">
                                        <div class="titulo-horario">
                                            <?php the_field('titulo_horario_academia_idiomas_sede_gc'); ?>
                                        </div>
                                        <?php the_field('texto_horario_academia_idiomas_sede_gc'); ?>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php
                                    $titulo_horario_extra_academia = get_field('titulo_oficina_idiomas_extranjero_sede_gc');
                                    $texto_horario_extra_academia = get_field('texto_horario_oficina_idiomas_extranjero_sede_gc');
                                    if ($titulo_horario_extra_academia && $texto_horario_extra_academia){
                                ?>
                                <div class="item-info">
                                    <i class="fas fa-clock"></i>
                                    <div class="info">
                                        <div class="titulo-horario">
                                            <?php the_field('titulo_oficina_idiomas_extranjero_sede_gc'); ?>
                                        </div>
                                        <?php the_field('texto_horario_oficina_idiomas_extranjero_sede_gc'); ?>
                                    </div>
                                </div>
                                <?php } ?>

                            </div>
                            
                            <?php
                                $galeria_sede_gc = get_field('galeria_sede_gc');
                                if ($galeria_sede_gc){
                            ?>
                            <div class="cta-galeria-sede">
                                <a href="#" data-toggle="modal" data-target="#ModalCenter"><i class="fas fa-chevron-right"></i>Ver galería</a>
                            </div>

                            <div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="gallery">
                                                <?php echo do_shortcode('[acf_gallery_slider acf_field="galeria_sede_gc"]'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <?php // . modal ?>

                            <?php } ?>

                        </div> <?php // .container-mapa-info ?>

                    </div> <?php // .col ?>

                </div> <?php // .row ?>
            </div> <?php // . container-general-info-sede-gc ?>
            
         </div> <?php // .container-sede-gc ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-sede-gc ?>


<?php get_footer(); ?>