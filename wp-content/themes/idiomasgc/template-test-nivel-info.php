<?php

/*

Template Name: Información Test de Nivel

*/

get_header(); 
the_post(); ?>

<div class="template-test-nivel-info">
    <div class="container-fluid">
        <div class="container-test-nivel-info">
            <div class="cta-back-title-general-test-nivel-info">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pagina_test_nivel'); ?>"><i class="fas fa-chevron-left"></i>Volver al test de nivel</a>
                </div>
                <div class="title-test-nivel-info">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_pagina_principal_test_nivel_info'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-test-nivel-info">
                <div class="cont-iframe-formulario-test">
                    <iframe src="<?php the_field('iframe_url_formulario_test_nivel'); ?>" width="100%" height="900" frameborder="0"></iframe>
                </div>
            </div> <?php // . container-general-info-test-nivel-info ?>
            
         </div> <?php // .container-test-nivel-info ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-test-nivel-info ?>


<?php get_footer(); ?>