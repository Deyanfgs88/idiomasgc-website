<?php

/*

Template Name: Información Curso Seniors Info

*/

get_header(); 
the_post(); ?>

<div class="template-cursos-info">
    <div class="container-fluid">
        <div class="container-curso-info">
            <div class="cta-back-title-general-curso">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pagina_todos_los_programas_seniors') ?>"><i class="fas fa-chevron-left"></i>Todos los programas</a>
                </div>
                <div class="title-curso-info">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_info_curso_seniors'); ?></h2>
                </div>
            </div>

            <div class="container-general-class-info">

                <div class="container-clase-info">
                    
                    <div class="cont-clase-info">
                        <div class="clase-info">
                            <div class="row">

                                <div class="col-12 col-lg-6">
                                    <div class="info left">
                                        <img src="<?php the_field('imagen_info_curso_seniors'); ?>" alt="imagen info clase">
                                        <div class="titulo-curso">
                                            <?php the_field('titulo_info_curso_seniors'); ?>
                                        </div>
                                        <div class="small-info">
                                            <?php the_field('texto_info_curso_seniors'); ?>
                                        </div>
                                        <div class="cta-reserva-ahora">
                                            <a href="<?php the_field('pagina_curso_info_reserva_ahora_seniors'); ?>">Reserva ahora</a>
                                        </div>
                                    </div> <? // .info ?>
                                </div> <?php // .col ?>

                                <div class="col-12 col-lg-6">
                                    <div class="info right">
                                        <div class="sistema-bonos">
                                            <div class="info-bonos">
                                                <div class="title-bonos">
                                                    <?php the_field('titulo_sistema_bonos_curso_seniors'); ?>
                                                </div>
                                                <div class="desc-bonos">
                                                    <?php the_field('descripcion_sistema_bonos_curso_seniors'); ?>
                                                </div>
                                            </div>
                                        </div> <?php // .sistema-bonos ?>

                                        <div class="alumnos-distri-inicio-certi">

                                            <div class="item alumnos">
                                                <i class="fas fa-users"></i>
                                                <div class="title-item">
                                                    <?php the_field('titulo_numero_de_alumnos_curso_seniors'); ?>
                                                </div>
                                                <div class="text-item">
                                                    <?php the_field('texto_numero_de_alumnos_curso_seniors'); ?>
                                                </div>
                                            </div>

                                            <div class="item distribucion">
                                                <i class="fas fa-stopwatch"></i>
                                                <div class="title-item">
                                                    <?php the_field('titulo_distribucion_curso_seniors'); ?>
                                                </div>
                                                <div class="text-item">
                                                    <?php the_field('texto_distribucion_curso_seniors'); ?>
                                                </div>
                                            </div>

                                            <div class="item inicio">
                                                <i class="far fa-calendar-alt"></i>
                                                <div class="title-item">
                                                    <?php the_field('titulo_inicio_curso_seniors'); ?>
                                                </div>
                                                <div class="text-item">
                                                    <?php the_field('texto_inicio_curso_seniors'); ?>
                                                </div>
                                            </div>

                                            <div class="item certificacion">
                                                <i class="fas fa-trophy"></i>
                                                <div class="title-item">
                                                    <?php the_field('titulo_certificacion_curso_seniors'); ?>
                                                </div>
                                                <div class="text-item">
                                                    <?php the_field('texto_certificacion_curso_seniors'); ?>
                                                </div>
                                            </div>

                                        </div> <?php // .alumnos-distri-inicio-certi ?>

                                        <div class="container-general-precios">
                                            
                                            <div class="container-precios">
                                                <div class="icon-euros">
                                                    <i class="fas fa-euro-sign"></i>
                                                </div>
                                                <div class="cont-info-prices">
                                                    <div class="row">

                                                        <div class="col-12 col-lg-6">

                                                            <div class="prices-hours uni-adultos">
                                                                <?php the_field('precios_hora_curso_seniors'); ?>
                                                            </div> <?php // . prices-hours ?>

                                                            <div class="list-coste-libros">
                                                                <div class="titulo-coste-libros">
                                                                    <?php the_field('titulo_precios_libros_curso_seniors'); ?>
                                                                </div>
                                                                <?php
                                                                    $list_prices_books = get_field('lista_precios_libros_curso_seniors');
                                                                    if ($list_prices_books){
                                                                        echo '<ul>';
                                                                        foreach ($list_prices_books as $prices_books) {
                                                                            echo '<li><i class="fas fa-chevron-right"></i><span class="nivel">' . $prices_books['nivel_curso'] . ': </span>' . $prices_books['coste_libro'] . '</li>';
                                                                        }
                                                                        echo '</ul>';
                                                                    }
                                                                ?>
                                                            </div> <?php // .list-coste-libros ?>

                                                            <div class="inclusion-libros">
                                                                <?php the_field('texto_inclusion_libros_seniors'); ?>
                                                            </div> <?php // .inclusion-libros ?>

                                                        </div> <?php // . col ?>

                                                        <div class="col-12 col-lg-6">
                                                            
                                                            <div class="precio-total-curso">
                                                                <?php
                                                                    $total_price = get_field('precio_total_curso_seniors');
                                                                    if ($total_price){
                                                                        echo '<p><span>Precio del curso: </span>' . $total_price . '</p>';              
                                                                    }
                                                                ?>
                                                            </div>

                                                            <div class="detalles-info">
                                                                <?php the_field('texto_detalles_info_seniors'); ?>
                                                            </div>

                                                            <div class="text-matricula">
                                                                <?php the_field('texto_anotacion_matricula_curso_seniors'); ?>
                                                            </div>
                                                                    
                                                        </div> <?php // . col ?>

                                                    </div> <?php // . row ?>

                                                </div> <?php // .cont-info-prices ?>

                                            </div> <?php // . container-precios ?>

                                        </div> <?php // .container-general-precios ?>

                                        <?php
                                            $texto_igic = get_field('texto_igic_curso_seniors');
                                            if ($texto_igic){
                                        ?>
                                        <div class="texto-igic-uni-adultos">
                                            <?php the_field('texto_igic_curso_seniors'); ?>
                                        </div>
                                        <?php } ?>
                                        
                                        <?php
                                            $more_info = get_field('mas_info_seniors');
                                            if ($more_info){
                                        ?>
                                        <div class="more-info">
                                            <?php the_field('mas_info_seniors'); ?>
                                        </div>
                                        <?php } ?>

                                    </div> <?php // .info ?>
                                </div> <?php // .col ?>
                            </div> <?php // .row ?>
                            
                        </div> <?php // .clase-info ?>
                    </div> <?php // .cont-clase-info ?>
    
                </div> <?php // .container-clase-info ?>
            </div> <?php // .container-general-class-info ?>


        </div> <?php // .container-curso-info ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-cursos-info ?>



<?php get_footer(); ?>