<?php

/*

Template Name: Extraescolares y campamentos de verano

*/

get_header(); 
the_post(); ?>

<div class="template-extraesco-campa-verano template-cursos">
    <div class="container-fluid">
        <div class="container-curso">
            <div class="cta-back-title-general-curso">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-curso title-extraesco-campa-verano">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_campa_verano'); ?></h2>
                </div>          
            </div>
            
            <div class="cont-portada-campamento-verano">

                <?php
                    $imagen_principal_campa_verano = get_field('imagen_principal_campa_verano');
                    if ($imagen_principal_campa_verano){
                ?>
                <div class="item-info-campa-verano imagen-principal">
                    <img src="<?php the_field('imagen_principal_campa_verano'); ?>" alt="imagen principal campamento verano">
                </div>
                <?php } ?>

                <?php
                    $titulo_aprender_ingles_gc = get_field('titulo_aprender_ingles_gc_campa_verano');
                    $subtitulo_aprender_ingles_gc = get_field('subtitulo_aprender_ingles_gc_campa_verano');
                    $texto_portada_idiomas_extra = get_field('texto_aprender_ingles_gc_campa_verano');
                    if ($titulo_aprender_ingles_gc && $subtitulo_aprender_ingles_gc && $texto_portada_idiomas_extra){
                ?>
                <div class="item-info-campa-verano aprender-ingles-gc">
                    <h3><?php the_field('titulo_aprender_ingles_gc_campa_verano'); ?></h3>
                    <h4><?php the_field('subtitulo_aprender_ingles_gc_campa_verano'); ?></h4>
                    <div class="content-text">
                        <?php the_field('texto_aprender_ingles_gc_campa_verano'); ?>
                    </div>
                </div>
                <?php } ?>

                <div class="item-info-campa-verano lista-ofrecen-campa-verano">
                    <?php
                    $titulo_ofrecen_campa_verano = get_field('titulo_que_ofrecen_campa_verano');
                    $lista_ofrecen_campa_verano = get_field('lista_que_ofrecen_campa_verano');
                    if ($titulo_ofrecen_campa_verano && $lista_ofrecen_campa_verano){
                        echo '<h4>' . $titulo_ofrecen_campa_verano . '</h4>';
                        echo '<ul>';
                        foreach ($lista_ofrecen_campa_verano as $ofrece_campa) {
                            echo '<li><i class="fas fa-chevron-right"></i>' . $ofrece_campa['ofrecen_campa_verano'] . '</li>';
                        }
                        echo '</ul>';
                    }
                    ?>
                </div>

            </div> <?php // .cont-portada-campamento-verano ?>

            <div class="container-general-class campa-verano">

                <div class="container-clases">
                    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_urban_daily_camp'); ?>" alt="imagen urban daily camp">
                            <a href="<?php the_field('pagina_informacion_urban_daily_camp'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_urban_daily_camp'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_urban_daily_camp'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_informacion_urban_daily_camp'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_urban_daily_camp'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div class="cont-clase d-none">
                        <div class="clase">
                            <img src="<?php the_field('imagen_discover_nature_camp'); ?>" alt="imagen discover nature camp">
                            <a href="<?php the_field('pagina_informacion_discover_nature_camp'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_discover_nature_camp'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_discover_nature_camp'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_informacion_discover_nature_camp'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_discover_nature_camp'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                </div> <?php // .container-clases ?>
            </div> <?php // .container-general-class ?>
            
            <?php
                $cta_revista_campa_verano = get_field('boton_descarga_revista_campa_verano');
                if ($cta_revista_campa_verano){
            ?>
            <div class="cta-revista-campa-verano">
                <a href="<?php the_field('boton_descarga_revista_campa_verano'); ?>" target="_blank">Descargar revista</a>
            </div>
            <?php } ?>


        </div> <?php // .container-curso ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-extraesco-campa-verano ?>



<?php get_footer(); ?>