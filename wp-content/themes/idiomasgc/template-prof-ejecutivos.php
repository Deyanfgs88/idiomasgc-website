<?php

/*

Template Name: Curso Profesionales y Ejecutivos

*/

get_header(); 
the_post(); ?>

<div class="template-cursos">
    <div class="container-fluid">
        <div class="container-curso">
            <div class="cta-back-title-general-curso">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-curso">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_curso_prof_ejecutivos'); ?></h2>
                </div>          
            </div>

            <?php
                $texto_select_course = get_field('texto_seleccionar_curso');
                $list_select_courses = get_field('lista_seleccionar_cursos');
            ?>
            <div class="container-seleccionar-item-curso">
                <?php
                    if ($texto_select_course) {
                ?>
                <p><?php the_field('texto_seleccionar_curso'); ?></p>
                <?php } ?>
                <?php
                    if ($list_select_courses) {
                ?>
                <select class="seleccionar-curso">
                    <?php
                        $i = 0;
                        foreach ($list_select_courses as $course) {
                            $i++;
                            echo '<option value="curso-' . $i .'">' . $course['texto_curso'] . '</option>';
                        }
                    ?>
                </select>
                <?php } ?>
            </div> <?php // .container-seleccionar-item-curso ?>

            <div class="container-general-class">

                <div class="container-clases">
                    
                    <div id="curso-1" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_general_plus_intensivo_prof_ejecutivos'); ?>" alt="imagen general plus / intensivo">
                            <a href="javascript:void(0)">
                                <div class="titulo">
                                    <?php the_field('titulo_general_plus_intensivo_prof_ejecutivos'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_general_plus_intensivo_prof_ejecutivos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_reserva_ahora_general_plus_intensivo_prof_ejecutivos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div id="curso-2" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_individual_mini_grupo_prof_ejecutivos'); ?>" alt="imagen individual mini grupo">
                            <a href="javascript:void(0)">
                                <div class="titulo">
                                    <?php the_field('titulo_individual_mini_grupo_prof_ejecutivo'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_individual_mini_grupo_prof_ejecutivo'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_reserva_ahora_individual_mini_grupo_prof_ejecutivo'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-3" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_idiomas_mundo_empresarial_prof_ejecutivos'); ?>" alt="imagen idiomas para el mundo empresarial">
                            <a href="javascript:void(0)">
                                <div class="titulo">
                                    <?php the_field('titulo_idiomas_mundo_empresarial_prof_ejecutivos'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_idiomas_mundo_empresarial_prof_ejecutivos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_reserva_ahora_idiomas_mundo_empresarial_prof_ejecutivos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div id="curso-4" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_atencion_cliente_prof_ejecutivos'); ?>" alt="imagen atención al cliente">
                            <a href="javascript:void(0)">
                                <div class="titulo">
                                    <?php the_field('titulo_atencion_cliente_prof_ejecutivos'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_atencion_cliente_prof_ejecutivos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_reserva_ahora_atencion_cliente_prof_ejecutivos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
                    
                    <div id="curso-5" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_idiomas_turismo_prof_ejecutivos'); ?>" alt="imagen idiomas para el turismo y la hostelería">
                            <a href="javascript:void(0)">
                                <div class="titulo">
                                    <?php the_field('titulo_idiomas_turismo_prof_ejecutivos'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_idiomas_turismo_prof_ejecutivos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_reserva_ahora_idiomas_turismo_prof_ejecutivos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-6" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_presen_online_prof_ejecutivos'); ?>" alt="imagen cursos a medida: presencial / online">
                            <a href="javascript:void(0)">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_presen_online_prof_ejecutivos'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_curso_presen_online_prof_ejecutivos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_reserva_ahora_curso_presen_online_prof_ejecutivos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
    
                </div> <?php // .container-clases ?>
            </div> <?php // .container-general-class ?>


        </div> <?php // .container-curso ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-cursos ?>



<?php get_footer(); ?>