<?php

/*

Template Name: Información Idiomas y formación en el extranjero

*/

get_header(); 
the_post(); ?>

<div class="template-idiomas-extranjero-info">
    <div class="container-fluid">
        <div class="container-idiomas-extranjero-info">
            <div class="cta-back-title-general-idiomas-extranjero-info">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pagina_idiomas_extranjero') ?>"><i class="fas fa-chevron-left"></i>Idiomas y formación en el extranjero</a>
                </div>
                <div class="title-idiomas-extranjero-info">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_destino_extranjero'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-idiomas-extranjero-info">

                <div class="imagen-text-portada-idiomas-extranjero-info">
                    <img src="<?php the_field('imagen_portada_destino_extranjero'); ?>" alt="imagen portada destino extranjero">
                    <div class="text-info-idiomas-extranjero-info">
                        <?php the_field('texto_informacion_destino_extranjero'); ?>
                    </div>
                    <div class="cta-informacion">
                        <a href="<?php the_field('pagina_mas_informacion'); ?>">Solicita más información</a>
                    </div>

                </div> <?php // .imagen-text-cta-portada-idiomas-extranjero-info ?>

            </div> <?php // . container-general-info-idiomas-extranjero-info ?>
            
         </div> <?php // .container-idiomas-extranjero-info ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-idiomas-extranjero-info ?>


<?php get_footer(); ?>