<?php

/*

Template Name: Información Cursos Online y Semipresenciales

*/

get_header(); 
the_post(); ?>

<div class="template-cursos-online-semi-info">
    <div class="container-fluid">
        <div class="container-cursos-online-semi-info">
            <div class="cta-back-title-general-cursos-online-semi-info">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pagina_cursos_online_semi'); ?>"><i class="fas fa-chevron-left"></i>Cursos online y semipresenciales</a>
                </div>
                <div class="title-cursos-online-semi-info">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_curso_online_semi_info'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-cursos-online-semi">

                <div class="item-info-curso-online-semi">
                    <?php
                        $lista_caracteristicas = get_field('lista_caracteristicas_curso_online_semi');
                        if ($lista_caracteristicas){
                            echo '<h4>Características</h4>';
                            echo '<ul>';
                            foreach ($lista_caracteristicas as $caracteristica) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $caracteristica['caracteristica_curso_online_semi'] . '</li>';
                            }
                            echo '</ul>';
                        }
                    ?>
                </div>
                
                <?php
                    $duracion_curso = get_field('duracion_curso_online_semi');
                    if ($duracion_curso){
                ?>
                <div class="item-info-curso-online-semi">
                    <h4><i class="fas fa-clock"></i>Duración del curso</h4>
                    <span><?php the_field('duracion_curso_online_semi'); ?></span>
                </div>
                <?php } ?>

                <?php
                    $horas_totales_curso = get_field('horas_totales_curso_online_semi');
                    if ($horas_totales_curso){
                ?>
                <div class="item-info-curso-online-semi">
                    <h4><i class="fas fa-stopwatch"></i>Horas totales</h4>
                    <?php the_field('horas_totales_curso_online_semi'); ?>
                </div>
                <?php } ?>

                <?php
                    $certificacion_acreditativa = get_field('certificacion_acreditativa_cursos_online_semi');
                    if ($certificacion_acreditativa){
                ?>
                <div class="item-info-curso-online-semi">
                    <h4><i class="fas fa-trophy"></i>Certificación acreditativa</h4>
                    <?php the_field('certificacion_acreditativa_cursos_online_semi'); ?>
                </div>
                <?php } ?>

                <?php
                    $numero_alumnos = get_field('numero_alumnos_cursos_online_semi');
                    if ($numero_alumnos){
                ?>
                <div class="item-info-curso-online-semi">
                    <h4><i class="fas fa-users"></i></i>Número de alumnos</h4>
                    <?php the_field('numero_alumnos_cursos_online_semi'); ?>
                </div>
                <?php } ?>

                <?php
                    $precio_curso = get_field('precio_cursos_online_semi');
                    if ($precio_curso){
                ?>
                <div class="item-info-curso-online-semi">
                    <h4><i class="fas fa-euro-sign"></i>Precio</h4>
                    <?php the_field('precio_cursos_online_semi'); ?>
                </div>
                <?php } ?>

                <?php
                    $forma_pago = get_field('forma_pago_cursos_online_semi');
                    if ($forma_pago){
                ?>
                <div class="item-info-curso-online-semi forma-pago">
                    <h4><i class="fas fa-chevron-right"></i>Forma de pago:</h4>  
                    <?php the_field('forma_pago_cursos_online_semi'); ?>
                </div>
                <?php } ?>

            </div> <?php // .container-general-info-cursos-online-semi ?>
            
         </div> <?php // .container-cursos-online-semi-info ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-cursos-online-semi-info ?>




<?php get_footer(); ?>