<?php

/*

Template Name: Incompany Inglés

*/

get_header(); 
the_post(); ?>

<div class="template-incompany-ingles-cat">
    <div class="container-fluid">
        <div class="container-incompany-ingles-cat">
            <div class="cta-back-title-general-incompany-ingles-cat">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pagina_incompany_ingles_cat'); ?>"><i class="fas fa-chevron-left"></i>Cursos In company</a>
                </div>
                <div class="title-incompany-ingles-cat">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_incompany_ingles_categ'); ?></h2>
                </div>
            </div>

            <div class="container-general-incompany-ingles-cat">

                <h3>Elige una categoría</h3>

                <div class="container-list-ingles-cat">

                    <div class="list-ingles-cat">

                        <div class="cont-item-cat">
                            <a href="<?php the_field('pagina_ingles_para_empresas'); ?>">
                                <div class="item-cat">
                                    <i class="fas fa-briefcase"></i>
                                    <p>Para empresas</p>
                                </div>
                            </a>
                        </div> <?php //.cont-item-cat ?>

                        <div class="cont-item-cat">
                            <a href="<?php the_field('pagina_ingles_para_turismo_hosteleria'); ?>">
                                <div class="item-cat">
                                    <i class="fas fa-utensils"></i>
                                    <p>Para turismo y hostelería</p>
                                </div>
                            </a>
                        </div> <?php //.cont-item-cat ?>

                        <div class="cont-item-cat">
                            <a href="<?php the_field('pagina_ingles_para_supermercados'); ?>">
                                <div class="item-cat">
                                    <i class="fas fa-lemon"></i>
                                    <p>Para supermercados</p>
                                </div>
                            </a>
                        </div> <?php //.cont-item-cat ?>

                        <div class="cont-item-cat">
                            <a href="<?php the_field('pagina_ingles_para_industria'); ?>">
                                <div class="item-cat">
                                    <i class="fas fa-industry"></i>
                                    <p>Para industria</p>
                                </div>
                            </a>
                        </div> <?php //.cont-item-cat ?>
    
                    </div> <?php //.list-ingles-cat ?>

                </div> <?php // .container-list-ingles-cat ?>

                <div class="lista-cursos-otros">
                    <h3>Elige un curso</h3>
                    <h4>Otros</h4>
                    <div class="cont-lista">
                        <?php
                            $lista_otros_cursos_incompany = get_field('lista_otros_cursos_ingles_incompany');
                            if ($lista_otros_cursos_incompany){
                                echo '<ul>';
                                foreach ($lista_otros_cursos_incompany as $curso_ingles_incompany) {
                                    echo '<li><i class="fas fa-chevron-right"></i><a href="' . $curso_ingles_incompany['pagina_otros_cursos_ingles_incompany'] .'">' . $curso_ingles_incompany['texto_otros_cursos_ingles_incompany'] . '</a></li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                    </div> <?php // .cont-lista ?>
                </div> <?php // .lista-cursos-otros ?>


            </div> <?php // . container-general-incompany-ingles-cat ?>
            
         </div> <?php // .container-incompany-ingles-cat ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-incompany-ingles-cat ?>




<?php get_footer(); ?>