<?php

/*

Template Name: Homepage

*/

get_header(); 
the_post(); ?>

<div class="template-homepage">
    <div class="container-fluid">
        <?php
            $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
            if ($tag_h1_meta){
        ?>
        <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
        <?php } else{ ?>
        <h1 class="d-none"><?php echo get_the_title(); ?></h1>
        <?php } ?>
        <h2 class="mobile" id="h2-homepage-mobile"><?php the_field('titulo_homepage'); ?></h2>
        <h3 class="mobile" id="h3-homepage-mobile"><?php the_field('subtitulo_homepage'); ?></h3>
        <div class="row">
            <div class="col-12 col-xl-8">
                <div class="section-levels" style="background-image: url('<?php the_field('imagen_fondo_niveles'); ?>');">
                    <h2 id="h2-homepage-tablet-desktop"><?php the_field('titulo_homepage'); ?></h2>
                    <h3 id="h3-homepage-tablet-desktop"><?php the_field('subtitulo_homepage'); ?></h3>
                    <div class="container-levels">
                        <div class="levels">
                            <div class="cont-cta-level">
                                <a href="<?php the_field('pagina_nivel_primaria'); ?>" class="cta-levels">
                                    <p>Primaria</p>
                                    <span class="age">3-11 años</span>
                                </a>
                            </div>
                            <div class="cont-cta-level">
                                <a href="<?php the_field('pagina_nivel_secund_bach'); ?>" class="cta-levels">
                                    <p>Secundaria y Bachillerato</p>
                                    <span class="age">12-16 años</span>
                                </a>
                            </div>
                            <div class="cont-cta-level">
                                <a href="<?php the_field('pagina_nivel_uni_adultos'); ?>" class="cta-levels">
                                    <p>Universitarios y Adultos</p>
                                    <span class="age">16-25 años</span>
                                </a>
                            </div>
                            <div class="cont-cta-level">
                                <a href="<?php the_field('pagina_nivel_prof_ejecut'); ?>" class="cta-levels">
                                    <p>Profesionales y Ejecutivos</p>
                                    <span class="age">25+</span>
                                </a>
                            </div>
                            <div class="cont-cta-level">
                                <a href="<?php the_field('pagina_nivel_seniors'); ?>" class="cta-levels">
                                    <p>Seniors</p>
                                    <span class="age">50+</span>
                                </a>
                            </div>
                        </div> <?php // .levels ?>
                    </div> <?php // .container-levels ?> 
                </div> <?php // .section-levels ?>

                <div class="ctas-ofrecimientos desktop d-none d-xl-flex">
                    <a href="<?php the_field('pagina_traducciones'); ?>">Traducciones</a>
                    <a href="<?php the_field('pagina_exam_oficiales'); ?>">Exámenes oficiales</a>
                    <a href="<?php the_field('pagina_formacion_profesorado'); ?>">Formación del profesorado</a>
                    <a href="<?php the_field('pagina_cursos_online_semi'); ?>">Cursos online y semipresenciales</a>
                </div> <?php // .ctas-ofrecimientos ?>

            </div> <?php // .col ?> 
            <div class="col-12 col-xl-4">

                <div class="section-idiomas-extraesco">

                    <a href="<?php the_field('pagina_promociones'); ?>">
                        <div class="formacion" style="background-image: url('<?php the_field('imagen_promociones'); ?>');">
                            <div class="title-promociones">
                                <?php the_field('titulo_promociones'); ?>
                            </div>
                            <div class="texto">
                                <?php the_field('texto_promociones'); ?>
                            </div>
                        </div>
                    </a>

                    <a href="<?php the_field('pagina_idiomas_empresas_instituciones'); ?>">
                        <div class="formacion incompany" style="background-image: url('<?php the_field('imagen_idiomas_empresas_instituciones'); ?>');">
                            <div class="texto">
                                <?php the_field('texto_idiomas_empresas_instituciones'); ?>
                            </div>
                        </div>
                    </a>

                    <a href="<?php the_field('pagina_extraesc_campamentos'); ?>" class="d-block d-xl-none">
                        <div class="formacion extra-campa-verano" style="background-image: url('<?php the_field('imagen_extraesc_campamentos'); ?>');">
                            <div class="texto">
                                <?php the_field('texto_extraesc_campamentos'); ?>
                            </div>
                        </div>
                    </a>

                    <a href="<?php the_field('pagina_idiomas_extranjero'); ?>" class="d-block d-xl-none">
                        <div class="formacion idiomas-extranjero" style="background-image: url('<?php the_field('imagen_idiomas_extranjero'); ?>');">
                            <div class="texto">
                                <?php the_field('texto_idiomas_extranjero'); ?>
                            </div>
                        </div>
                    </a>


                </div>

            </div> <?php // .col ?>   
        </div> <?php // .row ?>

        <div class="row">

            <div class="col-12 col-xl-8">

                <div class="ctas-ofrecimientos mobile-tablet d-block d-md-flex d-xl-none">
                    <a href="<?php the_field('pagina_traducciones'); ?>">Traducciones</a>
                    <a href="<?php the_field('pagina_exam_oficiales'); ?>">Exámenes oficiales</a>
                    <a href="<?php the_field('pagina_formacion_profesorado'); ?>">Formación del profesorado</a>
                    <a href="<?php the_field('pagina_cursos_online_semi'); ?>">Cursos online y semipresenciales</a>
                </div> <?php // .ctas-ofrecimientos ?>

                <div class="container-alumnos-blog-promociones-partners">
                    <div class="row">
                        <div class="col-12 col-lg-7 col-xl-7">
                            <div class="alumnos-blog-trabaja" style="background-image: url('<?php the_field('imagen_alumnos_blog_trabaja'); ?>');">
                                <a href="<?php the_field('pagina_clases_conversacion'); ?>">clases de conversación</a>
                                <a href="<?php the_field('pagina_area_alumnos'); ?>" target="_blank">área de alumnos</a>
                                <a href="<?php the_field('pagina_blog'); ?>">blog</a>
                                <a href="<?php the_field('pagina_videos'); ?>">vídeos</a>
                            </div> <?php // .alumnos-blog-trabaja ?>
                        </div>

                        <div class="col-12 col-lg-5 col-xl-5">

                            <div class="container-promociones-destacamos-partners">
                                <div class="promociones-destacamos">
                                    <a href="<?php the_field('pagina_destacamos_semana'); ?>">
                                        <div class="promo-dest" style="background-image: url('<?php the_field('imagen_destaca_semana'); ?>');">
                                            <div class="title-destaca-semana">
                                                <?php the_field ('titulo_texto_destaca_semana'); ?>
                                            </div>
                                            <div class="text">
                                                <?php the_field('texto_destaca_semana'); ?>
                                            </div>
                                        </div>
                                    </a>
                                </div> <?php // .promociones-destacamos ?>
                
                            </div> <?php // . container-promociones-destacamos-partners ?>

                        </div> <?php // .col ?>
                    </div> <?php // .row ?>

                    <div class="container-partners">
                        <div class="partners">

                            <?php
                                $images = get_field('galeria_partners');
                                if ($images){
                                    echo '<ul>';
                                    foreach ($images as $image) {
                                        echo '<li><a href="' . $image['url_partner'] . '" target="_blank"><img src="' . $image['imagen_partner'] . '" alt="' . $image['alt_imagen_partner'] . '" /></a></li>';
                                    }
                                    echo '</ul>';
                                }
                            ?>

                        </div> <?php // .partners ?>
                    </div>

                </div> <?php // .container-alumnos-blog-promociones-partners ?>

            </div> <?php // .col ?>

            <div class="col-12 col-lg-4 d-none d-xl-block">
                <div class="formacion-idiomas-empresas tablet-desktop">

                    <a href="<?php the_field('pagina_extraesc_campamentos'); ?>">
                        <div class="formacion extra-campa-verano" style="background-image: url('<?php the_field('imagen_extraesc_campamentos'); ?>');">
                            <div class="texto">
                                <?php the_field('texto_extraesc_campamentos'); ?>
                            </div>
                        </div>
                    </a>

                    <a href="<?php the_field('pagina_idiomas_extranjero'); ?>">
                        <div class="formacion idiomas-extranjero" style="background-image: url('<?php the_field('imagen_idiomas_extranjero'); ?>');">
                            <div class="texto">
                                <?php the_field('texto_idiomas_extranjero'); ?>
                            </div>
                        </div>
                    </a>

                </div>
            </div> <?php // .col ?>

        </div> <?php // .row ?>

        <?php
            $texto_homepage = get_field('texto_homepage');
            if ($texto_homepage){ 
        ?>
        <div class="container-texto-seo d-none">
            <?php the_field('texto_homepage') ?>
        </div>
        <?php } ?>

    </div> <?php // .container-fluid ?>
</div> <?php // .template-homepage ?>

<?php get_footer(); ?>