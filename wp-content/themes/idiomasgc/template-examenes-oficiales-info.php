<?php

/*

Template Name: Información Exámenes Oficiales

*/

get_header(); 
the_post(); ?>

<div class="template-exam-oficiales-info">
    <div class="container-fluid">
        <div class="container-exam-oficiales-info">
            <div class="cta-back-title-general-exam-info">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pagina_exam_oficiales'); ?>"><i class="fas fa-chevron-left"></i>Exámenes oficiales</a>
                </div>
                <div class="title-exam">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_exam_oficiales'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-exam-oficiales">

                <div class="imagen-text-cta-portada-exam">
                    <img src="<?php the_field('imagen_portada_exam_oficiales'); ?>" alt="imagen portada examenes oficiales">
                    <div class="text-info-exam">
                        <?php the_field('texto_informacion_exam_oficiales'); ?>
                    </div>

                    <div class="cta-reserva-ahora">
                        <a href="<?php the_field('pagina_reserva_ahora_exam_oficiales'); ?>">Reserva ahora</a>
                    </div>
                </div> <?php // .imagen-text-cta-portada-exam ?>

                <div class="item-info-exam">
                    <div class="cont-titulo">
                        <div class="titulo">
                            <?php the_field ('titulo_caracteristicas_exam_oficiales'); ?>
                        </div>
                    </div>
                    <?php
                        $lista_caracteristicas = get_field('lista_caracteristicas_exam_oficiales');
                        if ($lista_caracteristicas){
                            echo '<ul>';
                            foreach ($lista_caracteristicas as $caracteristica) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $caracteristica['caracteristica_exam'] . '</li>';
                            }
                            echo '</ul>';
                        }
                    ?>
                </div>

                <div class="item-info-exam">
                    <div class="cont-titulo">
                        <i class="fas fa-clock"></i>
                        <div class="titulo">
                            <?php the_field('titulo_duracion_curso_exam_oficiales'); ?>
                        </div>
                    </div>
                    <?php the_field('duracion_curso_exam_oficiales'); ?>
                </div>

                <div class="item-info-exam">
                    <div class="cont-titulo">
                        <i class="fas fa-stopwatch"></i>
                        <div class="titulo">
                            <?php the_field('titulo_horas_totales_exam_oficiales'); ?>
                        </div>
                    </div>
                    <?php the_field('horas_totales_exam_oficiales'); ?>
                </div>

                <div class="item-info-exam">
                    <div class="cont-titulo">
                        <i class="fas fa-users"></i>
                        <div class="titulo">
                            <?php the_field('titulo_numero_alumnos_exam_oficiales'); ?>
                        </div>
                    </div>
                    <?php the_field('numero_alumnos_exam_oficiales'); ?>
                </div>

                <div class="item-info-exam">
                    <div class="cont-titulo">
                        <i class="fas fa-euro-sign"></i>
                        <div class="titulo">
                            <?php the_field('titulo_precio_exam_oficiales'); ?>
                        </div>
                    </div>
                    <?php the_field('precio_exam_oficiales'); ?>
                </div>

                <div class="item-info-exam">
                    <div class="cont-titulo">
                        <i class="fas fa-check"></i>
                        <div class="titulo">
                            <?php the_field('titulo_garantias_exam_oficiales'); ?>
                        </div>
                    </div>
                    <?php
                        $lista_garantias = get_field('lista_garantias_exam_oficiales');
                        if ($lista_garantias){
                            echo '<ul>';
                            foreach ($lista_garantias as $garantia) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $garantia['garantia_exam'] . '</li>';
                            }
                            echo '</ul>';
                        }
                    ?>    
                </div>

                <div class="item-info-exam">
                    <?php
                        $fechas_inicio_exam = get_field('fechas_exam_oficiales');
                        if ($fechas_inicio_exam){
                    ?>
                    <div class="cont-titulo">
                        <i class="far fa-calendar-alt"></i>
                        <div class="titulo">
                            <?php the_field('titulo_fechas_exam_oficiales'); ?>
                        </div>
                    </div>
                    <div class="table-info-exam">
                        <table class="table table-bordered">
                            <?php
                            echo '<thead>';
                            echo '<tr>';
                                foreach ( $fechas_inicio_exam['header'] as $th ) {
            
                                    echo '<th>';
                                        echo $th['c'];
                                    echo '</th>';
                                }
                
                            echo '</tr>';
                            echo '</thead>';
                            ?>
                            <tbody>
                            <?php
                                foreach ( $fechas_inicio_exam['body'] as $tr ) {
                                    echo '<tr>';
                                        foreach ( $tr as $td ) {
                                            echo '<td>';
                                                echo $td['c'];
                                            echo '</td>';
                                        }
                                    echo '</tr>';
                                }
                            ?>
                            </tbody>
                        </table>

                    </div> <?php // .table ?>
                    <?php } ?>

                </div>

            </div> <?php // . container-general-info-exam-oficiales ?>
            
         </div> <?php // .container-exam-oficiales ?>
    </div> <?php // .container-fluid ?>
</div><?php // .exam-oficiales ?>




<?php get_footer(); ?>