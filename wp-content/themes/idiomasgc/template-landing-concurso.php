<?php

/*

Template Name: Landing Concurso

*/

get_header(); 
the_post(); ?>

<div class="template-landing-concurso">
    <div class="container-fluid">
        <div class="container-landing-concurso">
            <div class="cta-back-title-general-landing-concurso">
                <div class="title-landing-concurso">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                </div>
            </div>

            <div class="container-general-header">
                <div class="cont-header">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="#"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <?php
                                    $lista_items_menu_concurso = get_field('lista_items_menu_concurso');
                                    $i = 0;
                                    foreach ($lista_items_menu_concurso as $item_menu) {
                                        $i++;
                                        echo '<li class="nav-item">';
                                        echo '<a class="nav-link" href="#section-' . $i . '">' . $item_menu['texto_menu'] . '</a>';
                                        echo '</li>';
                                    }
                                ?>
                            </ul>
                        </div>
                    </nav>

                </div>          
            </div> <?php // . container-header ?>

            <div class="container-general-info-landing-concurso">

                <div class="container-portada" style="background-image: url('<?php the_field('imagen_portada_principal_concurso'); ?>');">
                    <div class="image-portada-bg"></div>
                </div> <?php // .container-portada ?>

                <div id="section-1" class="section one">
                    <div class="container">
                        <div class="row">

                            <div class="col-12 col-lg-6">
                                <div class="cont-info imagen">
                                    <div class="cont-imagen">
                                        <div class="imagen section-1">
                                            <img src="<?php the_field('imagen_left_section_1'); ?>" alt="imagen left sección 1">
                                        </div>
                                    </div>
                                </div> <?php // .cont-info ?>
                            </div> <?php // .col ?>

                            <div class="col-12 col-lg-6">
                                <div class="cont-info">
                                    <div class="title">
                                        <?php the_field('titulo_derecha_section_1'); ?>
                                    </div>
                                    <div class="texto">
                                        <?php the_field('texto_derecha_section_1'); ?>
                                    </div>
                                </div> <?php // .cont-info ?>
                            </div> <?php // .col ?>

                        </div> <?php // . row ?>
                    </div> <?php // .container ?>
                </div> <?php // .section ?>

                <div id="section-2" class="section two">
                    <div class="container">
                        <div class="row">
                        
                            <div class="col-12 col-lg-6">
                                <div class="cont-info imagen">
                                    <div class="cont-imagen">
                                        <div class="imagen">
                                            <img src="<?php the_field('imagen_left_section_2'); ?>" alt="imagen left sección 2">
                                        </div>
                                    </div>
                                </div> <?php // .cont-info ?>
                            </div> <?php // .col ?>

                            <div class="col-12 col-lg-6">
                                <div class="cont-info">
                                    <div class="item-info">
                                        <div class="title">
                                            <?php the_field('titulo1_derecha_section_2'); ?>
                                        </div>
                                        <div class="lista-info">
                                            <ul>
                                                <?php
                                                    $lista_premios_concurso = get_field('lista_premios_concurso');
                                                    foreach ($lista_premios_concurso as $premios) {
                                                        echo '<li><i class="fas fa-chevron-right"></i>' . $premios['texto_premio'] . '</li>';
                                                    }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="section-3" class="item-info">
                                        <div class="title">
                                            <?php the_field('titulo2_derecha_section_3'); ?>
                                        </div>
                                        <div class="lista-info">
                                            <ul>
                                                <?php
                                                    $lista_requisitos = get_field('lista_requisitos');
                                                    foreach ($lista_requisitos as $requisitos) {
                                                        echo '<li><i class="fas fa-chevron-right"></i>' . $requisitos['texto_requisito'] . '</li>';
                                                    }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div> <?php // .cont-info ?>
                            </div> <?php // .col ?>

                        </div> <?php // .row ?>
                    </div> <?php // .container ?>
                </div> <?php // .section ?>

                <div id="section-4" class="section four">
                    <div class="container">
                        <div class="row">

                            <div class="col-12 col-lg-6">
                                <div class="cont-info flex-center">
                                    <div class="title">
                                        <?php the_field('titulo_izquierda_section_4'); ?>
                                    </div>
                                    <div class="lista-info">
                                        <ul>
                                            <?php
                                                $lista_fechas = get_field('lista_fechas');
                                                foreach ($lista_fechas as $fechas) {
                                                    echo '<li><i class="fas fa-chevron-right"></i>' . $fechas['texto_fechas'] . '</li>';
                                                }
                                            ?>
                                        </ul>
                                    </div>
                                </div> <?php // .cont-info ?>
                            </div> <?php //. col ?>

                            <div class="col-12 col-lg-6">
                                <div class="cont-info">
                                    <div class="imagen">
                                        <img src="<?php the_field('imagen_right_section_4'); ?>" alt="imagen right sección 4">
                                    </div>
                                </div>
                            </div> <?php //. col ?>

                        </div> <?php //.row ?>
                    </div> <?php // .container ?> 
                </div> <?php // .section ?>

                <div id="section-5" class="section six">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="cont-info">
                                    <div class="title centrado">
                                        <?php the_field('titulo_section_5'); ?>
                                    </div>
                                    <div class="imagen form">
                                        <img src="<?php the_field('imagen_section_5'); ?>" alt="imagen sección 5">
                                    </div>
                                    <div class="formulario">
                                        <?php the_field('formulario_registro'); ?>
                                    </div>
                                </div> <?php // .cont-info ?>
                            </div> <?php //. col ?>
                        </div> <?php //.row ?>       
                    </div> <?php // .container ?> 
                </div> <?php // .section ?>

                <div id="section-6" class="section six">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="cont-info">
                                    <div class="title centrado">
                                        <?php the_field('titulo_section_6'); ?>
                                    </div>
                                    <div class="texto">
                                        <?php the_field('texto_section_6'); ?>
                                    </div>
                                </div> <?php // .cont-info ?>
                            </div> <?php //. col ?>
                        </div> <?php //.row ?>       
                    </div> <?php // .container ?> 
                </div> <?php // .section ?>

            </div> <?php // . container-general-info-landing-concurso ?>
            
         </div> <?php // .container-landing-concurso ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-landing-concurso ?>




<?php get_footer(); ?>