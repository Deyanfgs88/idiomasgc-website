<?php

/*

Template Name: Blog

*/

get_header(); 
the_post(); ?>

<div class="template-blog">
    <div class="container-fluid">
        <div class="container-blog">
            <div class="cta-back-title-general-blog">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-blog">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_blog'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-blog">
               <?php echo do_shortcode('[wcp-carousel id="1460" orderby="date"]'); ?>
            </div> <?php // . container-general-info-blog ?>
            
         </div> <?php // .container-blog ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-blog ?>




<?php get_footer(); ?>