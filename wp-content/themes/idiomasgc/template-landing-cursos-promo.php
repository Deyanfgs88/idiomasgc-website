<?php

/*

Template Name: Cursos Landing Promo

*/

get_header(); 
the_post(); ?>

<div class="template-cursos">
    <div class="container-fluid">
        <div class="container-curso">
            <div class="cta-back-title-general-curso">
                <div class="title-curso">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_pagina_cursos'); ?></h2>
                </div>          
            </div>

            <div class="container-general-class">

                <div class="container-clases">
                    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_1'); ?>" alt="imagen curso">
                            <a href="<?php the_field('pagina_reserva_curso_1'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_1'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_curso_1'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_reserva_curso_1'); ?>"><?php the_field('texto_reserva_boton_curso_1'); ?></a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_2'); ?>" alt="imagen curso">
                            <a href="<?php the_field('pagina_reserva_curso_2'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_2'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_curso_2'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_reserva_curso_2'); ?>"><?php the_field('texto_reserva_boton_curso_2'); ?></a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_3'); ?>" alt="imagen curso">
                            <a href="<?php the_field('pagina_reserva_curso_3'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_3'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_curso_3'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_reserva_curso_3'); ?>"><?php the_field('texto_reserva_boton_curso_3'); ?></a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_curso_4'); ?>" alt="imagen curso">
                            <a href="<?php the_field('pagina_reserva_curso_4'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_curso_4'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_curso_4'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_reserva_curso_4'); ?>"><?php the_field('texto_reserva_boton_curso_4'); ?></a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
        
                </div> <?php // .container-clases ?>
            </div> <?php // .container-general-class ?>
            
        </div> <?php // .container-curso ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-cursos ?>



<?php get_footer(); ?>