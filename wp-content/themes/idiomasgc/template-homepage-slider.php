<?php

/*

Template Name: Homepage Slider

*/

get_header(); 
the_post(); ?>

<div class="template-homepage-slider">
    <div class="container-fluid">
        <?php
            $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
            if ($tag_h1_meta){
        ?>
        <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
        <?php } else{ ?>
        <h1 class="d-none"><?php echo get_the_title(); ?></h1>
        <?php } ?>
        <h2 class="mobile d-md-none" id="h2-homepage-mobile"><?php the_field('titulo_homepage'); ?></h2>
        <h3 class="mobile d-md-none" id="h3-homepage-mobile"><?php the_field('subtitulo_homepage'); ?></h3>

        <div class="container-slider-levels">
            
            <div class="slider-homepage">

                <div class="container-slider">
                    <div class="mobile d-md-none">
                        <?php echo do_shortcode('[smartslider3 alias="homepage-mobile"]'); ?>
                    </div>
                    <div class="tablet d-none d-md-block d-lg-none">
                        <?php echo do_shortcode('[smartslider3 alias="homepage-tablet"]'); ?>
                    </div>
                    <div class="tablet-horizontal desktop d-none d-lg-block">
                        <?php echo do_shortcode('[smartslider3 alias="homepage-desktop"]'); ?>
                    </div> 
                </div> <?php // .container-slider ?>

                <div class="container-levels">
                    <div class="levels">
                        <div class="cont-cta-level">
                            <a href="<?php the_field('pagina_nivel_primaria'); ?>" class="cta-levels">
                                <p>Primaria</p>
                                <span class="age">3-11 años</span>
                            </a>
                        </div>
                        <div class="cont-cta-level">
                            <a href="<?php the_field('pagina_nivel_secund_bach'); ?>" class="cta-levels">
                                <p>Secundaria y Bachillerato</p>
                                <span class="age">12-16 años</span>
                            </a>
                        </div>
                        <div class="cont-cta-level">
                            <a href="<?php the_field('pagina_nivel_uni_adultos'); ?>" class="cta-levels">
                                <p>Universitarios y Adultos</p>
                                <span class="age">16-25 años</span>
                            </a>
                        </div>
                        <div class="cont-cta-level">
                            <a href="<?php the_field('pagina_nivel_prof_ejecut'); ?>" class="cta-levels">
                                <p>Profesionales y Ejecutivos</p>
                                <span class="age">25+</span>
                            </a>
                        </div>
                        <div class="cont-cta-level">
                            <a href="<?php the_field('pagina_nivel_seniors'); ?>" class="cta-levels">
                                <p>Seniors</p>
                                <span class="age">50+</span>
                            </a>
                        </div>
                    </div> <?php // .levels ?>
                </div> <?php // .container-levels ?>

            </div> <?php // .slider-homepage ?>

        </div> <?php // .container-slider-levels ?>
        
        <div class="ctas-ofrecimientos">
            <a href="<?php the_field('pagina_traducciones'); ?>">Traducciones</a>
            <a href="<?php the_field('pagina_exam_oficiales'); ?>">Exámenes oficiales</a>
            <a href="<?php the_field('pagina_formacion_profesorado'); ?>">Formación del profesorado</a>
            <a href="<?php the_field('pagina_cursos_online_semi'); ?>">Cursos online y semipresenciales</a>
        </div> <?php // .ctas-ofrecimientos ?>

        <div class="secciones-otros-servicios">

            <div class="item-servicio">
                <a href="<?php the_field('pagina_idiomas_empresas_instituciones'); ?>">
                    <div class="formacion incompany" style="background-image: url('<?php the_field('imagen_idiomas_empresas_instituciones'); ?>');">
                        <div class="texto">
                            <?php the_field('texto_idiomas_empresas_instituciones'); ?>
                        </div>
                    </div>
                </a>
            </div> <?php // .item-servicio ?>

            <div class="item-servicio">
                <a href="<?php the_field('pagina_extraesc_campamentos'); ?>">
                    <div class="formacion extra-campa-verano" style="background-image: url('<?php the_field('imagen_extraesc_campamentos'); ?>');">
                        <div class="texto">
                            <?php the_field('texto_extraesc_campamentos'); ?>
                        </div>
                    </div>
                </a>
            </div> <?php // .item-servicio ?>

            <div class="item-servicio">
                <a href="<?php the_field('pagina_idiomas_extranjero'); ?>">
                    <div class="formacion idiomas-extranjero" style="background-image: url('<?php the_field('imagen_idiomas_extranjero'); ?>');">
                        <div class="texto">
                            <?php the_field('texto_idiomas_extranjero'); ?>
                        </div>
                    </div>
                </a>
            </div> <?php // .item-servicio ?>

        </div> <?php // .secciones-otros-servicios ?>

        <div class="container-otras-secciones">
                    
            <div class="servicios-de-interes">

                <a href="<?php the_field('pagina_clases_conversacion'); ?>">
                    <div class="item-interes">
                        <div class="imagen-bg clases-conversacion" style="background-image: url('<?php the_field('imagen_clases_conversacion'); ?>');"></div>
                    </div>
                    <p>clases de conversación</p>
                </a>
                <a href="<?php the_field('pagina_area_alumnos'); ?>" target="_blank">
                    <div class="item-interes">
                        <div class="imagen-bg area-alumnos" style="background-image: url('<?php the_field('imagen_area_alumnos'); ?>');"></div>
                    </div>
                    <p>área de alumnos</p>
                </a>
                <a href="<?php the_field('pagina_blog'); ?>">
                    <div class="item-interes">
                        <div class="imagen-bg blog" style="background-image: url('<?php the_field('imagen_blog'); ?>');"></div>
                    </div>
                    <p>blog</p>
                </a>
                <a href="<?php the_field('pagina_videos'); ?>">
                    <div class="item-interes">
                        <div class="imagen-bg videos" style="background-image: url('<?php the_field('imagen_videos'); ?>');"></div>
                    </div>
                    <p>vídeos</p>
                </a>
            </div> <?php // .servicios-de-interes ?>

        </div> <?php // .container-otras-secciones ?>

        <div class="container-partners">
            <div class="partners">

                <?php
                    $images = get_field('galeria_partners');
                    if ($images){
                        echo '<ul>';
                        foreach ($images as $image) {
                            echo '<li><a href="' . $image['url_partner'] . '" target="_blank"><img src="' . $image['imagen_partner'] . '" alt="' . $image['alt_imagen_partner'] . '" /></a></li>';
                        }
                        echo '</ul>';
                    }
                ?>

            </div> <?php // .partners ?>
        </div> <?php // .container-partners ?>

        <?php
            $texto_homepage = get_field('texto_homepage');
            if ($texto_homepage){ 
        ?>
        <div class="container-texto-seo d-none">
            <?php the_field('texto_homepage') ?>
        </div> <?php // .container-texto-seo ?>
        <?php } ?>

    </div> <?php // .container-fluid ?>
</div> <?php // .template-homepage-slider ?>

<?php get_footer(); ?>