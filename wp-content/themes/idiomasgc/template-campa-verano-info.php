<?php

/*

Template Name: Información Campamentos de verano

*/

get_header(); 
the_post(); ?>

<div class="template-extra-campa-verano-info template-cursos-info">
    <div class="container-fluid">
        <div class="container-curso-info">
            <div class="cta-back-title-general-curso">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pag_todos_programas_campa_veran_info') ?>"><i class="fas fa-chevron-left"></i>Todos los programas</a>
                </div>
                <div class="title-curso-info">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_camp_veran_info'); ?></h2>
                </div>
            </div>

            <div class="container-general-class-info">

                <div class="container-clase-info info-campa-verano">
                    
                    <div class="cont-clase-info">
                        <div class="clase-info">
                            <div class="row">

                                <div class="col-12 col-lg-6 col-xl-4">
                                    <div class="info left">
                                        <img src="<?php the_field('imagen_principal_camp_veran_info'); ?>" alt="imagen campamentos verano información">
                                        <div class="titulo-curso">
                                            <?php the_field('titulo_camp_veran_info'); ?>
                                        </div>
                                        <div class="small-info">
                                            <?php the_field('texto_principal_camp_veran_info'); ?>
                                        </div>
                                        <div class="ctas-campa-verano-info">
                                            <a href="<?php the_field('pagina_reserva_ahora_camp_veran_info'); ?>">Reserva ahora</a>
                                            <a href="<?php the_field('pagina_ver_activ_camp_veran_info'); ?>">Ver actividades</a>
                                        </div>
                                    </div> <? // .info ?>
                                </div> <?php // .col ?>

                                <div class="col-12 col-lg-6 col-xl-8">
                                    <div class="info right">

                                        <div class="container-ubicacion">

                                            <h3><i class="fas fa-map-marker-alt"></i>Ubicación</h3>
                                            <?php
                                                $titulo_ubica_camp_verano = get_field('titulo_ubica_camp_veran_info');
                                                $texto_ubica_camp_verano = get_field('texto_ubica_camp_veran_info');
                                                if ($titulo_ubica_camp_verano && $texto_ubica_camp_verano){
                                            ?>
                                            <div class="item-info-ubica">
                                                <h4><?php the_field('titulo_ubica_camp_veran_info'); ?></h4>
                                                <?php the_field('texto_ubica_camp_veran_info'); ?>
                                            </div>
                                            <?php } ?>

                                            <?php
                                                $titulo_que_ofrece_camp_verano = get_field('titulo_que_ofrece_camp_veran_info');
                                                $lista_que_ofrece_camp_verano = get_field('lista_que_ofrece_camp_verano_info');
                                                if ($titulo_que_ofrece_camp_verano && $lista_que_ofrece_camp_verano){
                                            ?>
                                            <div class="item-info-ubica">
                                                <h4><?php the_field('titulo_que_ofrece_camp_veran_info'); ?></h4>
                                                <?php
                                                    echo '<ul>';
                                                    foreach ($lista_que_ofrece_camp_verano as $que_ofrece) {
                                                        echo '<li><i class="fas fa-chevron-right"></i>' . $que_ofrece['ofrece_camp_verano_info'] . '</li>';
                                                    }
                                                    echo '</ul>';
                                                ?>
                                            </div>
                                            <?php } ?>
                                        
                                        </div> <?php // .container-ubicacion ?>

                                        <div class="container-calendario-precios-metod">

                                            <h3><i class="far fa-calendar-alt"></i>Calendario y precios</h3>

                                            <?php
                                                $texto_calendario_precios = get_field('texto_calendario_precios_camp_verano_info');
                                                if ($texto_calendario_precios){
                                            ?>
                                            <div class="info-text">
                                                <?php the_field('texto_calendario_precios_camp_verano_info'); ?>
                                            </div>
                                            <?php } ?>

                                            <div class="cont-lista-semanas-precios">

                                                <div class="row">

                                                    <div class="col-12 col-xl-5">
                                                        <?php
                                                            $lista_calendario_semanas = get_field('lista_calendario_semanas_camp_verano_info');
                                                            if ($lista_calendario_semanas){
                                                        ?>
                                                        <div class="lista-semanas">
                                                            <?php
                                                                echo '<ul>';
                                                                foreach ($lista_calendario_semanas as $semanas) {
                                                                    echo '<li><i class="fas fa-chevron-right"></i>' . $semanas['calendario_camp_verano_info'] . '</li>';
                                                                }
                                                                echo '</ul>';
                                                            ?>
                                                        </div>
                                                        <?php } ?>
                                                    </div> <?php // .col ?>

                                                    <div class="col-12 col-xl-7">
                                                        <div class="container-semana-precios-detalles">
                                                            <div class="symbol-euro">
                                                                <i class="fas fa-euro-sign"></i>
                                                            </div>
                                                            <div class="cont-semana-precio-detalles">
                                                                <?php 
                                                                    $lista_semanas_precio = get_field('lista_semanas_precio_camp_verano_info');
                                                                    if ($lista_semanas_precio){
                                                                ?>
                                                                <div class="lista-semanas-precios">
                                                                    <?php
                                                                        echo '<ul>';
                                                                        foreach ($lista_semanas_precio as $semanas_precio) {
                                                                            echo '<li><i class="fas fa-chevron-right"></i>' . $semanas_precio['semana_precio_camp_veran_info'] . '</li>';
                                                                        }
                                                                        echo '</ul>';
                                                                    ?>
                                                                </div>
                                                                <?php } ?>

                                                                <?php 
                                                                    $detalles_precios = get_field('detalles_precio_camp_verano_info');
                                                                    if ($detalles_precios){
                                                                ?>
                                                                <div class="detalles-precios">
                                                                    <?php the_field('detalles_precio_camp_verano_info'); ?>
                                                                </div>
                                                                <?php } ?>

                                                                <?php
                                                                    $texto_igic = get_field('texto_igic');
                                                                    if ($texto_igic){
                                                                ?>
                                                                <div class="texto-igic">
                                                                    <?php the_field('texto_igic'); ?>
                                                                </div>
                                                                <?php } ?>

                                                            </div>
                                                        </div> <?php // .container-semana-precios-detalles ?>
                                                    </div> <?php // . col ?>

                                                </div> <? // .row ?>
                                            </div> <?php // .cont-lista-semanas-precios ?> 
                                            
                                            <?php
                                                $texto_metodologia = get_field('texto_metodologia_camp_verano_info');
                                                if ($texto_metodologia){
                                            ?>
                                            <div class="cont-metodologia">
                                                <h3>Metodología:</h3>
                                                <div class="texto-metodologia">
                                                    <?php the_field('texto_metodologia_camp_verano_info'); ?>
                                                </div>
                                            </div> <?php // .cont-metodologia ?>
                                            <?php } ?>
    
                                        </div> <?php // .container-calendiaro-precios ?>
                                        
                                    </div> <?php // .info ?>
                                </div> <?php // .col ?>
                            </div> <?php // .row ?>
                            
                        </div> <?php // .clase-info ?>
                    </div> <?php // .cont-clase-info ?>
    
                </div> <?php // .container-clase-info ?>
            </div> <?php // .container-general-class-info ?>


        </div> <?php // .container-curso-info ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-extra-campa-verano-info ?>



<?php get_footer(); ?>