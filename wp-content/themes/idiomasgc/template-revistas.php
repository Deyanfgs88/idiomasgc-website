<?php

/*

Template Name: Revistas

*/

get_header(); 
the_post(); ?>

<div class="template-revistas">
    <div class="container-fluid">
        <div class="container-revistas">
            <div class="cta-back-title-general-revistas">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-revistas">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_revistas'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-revistas">
                <div class="container-revistas">
                    <?php
                        $lista_revistas_web =  get_field('lista_revistas_web');
                        if ($lista_revistas_web){
                            foreach ($lista_revistas_web as $revista) {
                                echo '<div class="cont-revista">';
                                echo '<div class="revista">';
                                echo '<div class="imagen-bg" style="background-image: url(' . $revista['imagen_portada_revista'] . ')"></div>';
                                echo '<div class="titulo">' . $revista['titulo_revista'] . '</div>';
                                echo '<a href="' . $revista['archivo_revista'] . '" target="_blank"><i class="fas fa-download"></i>Descargar</a>';
                                echo '</div>';
                                echo '</div>';
                            }
                        }
                    ?>
                </div>
            </div> <?php // . container-general-info-revistas ?>
            
         </div> <?php // .container-revistas ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-revistas ?>




<?php get_footer(); ?>