<?php

/*

Template Name: Promociones

*/

get_header(); 
the_post(); ?>

<div class="template-promociones">
    <div class="container-fluid">
        <div class="container-promociones">
            <div class="cta-back-title-general-promociones">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-promociones">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_promociones'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-promociones">

                <?php
                    $subtitle_desc_promocion = get_field('titulo_desc_promocion');
                    if ($subtitle_desc_promocion){
                ?>
                <div class="subtitle-promociones">
                    <h2><i class="fas fa-chevron-right"></i><?php the_field('titulo_desc_promocion'); ?></h2>
                </div>
                <?php } ?>

                <div class="imagen-text-portada-promociones">
                    <img src="<?php the_field('imagen_portada_promociones'); ?>" alt="imagen portada promociones">
                    <div class="text-info-promociones">
                        <?php the_field('texto_informacion_promociones'); ?>
                    </div>

                    <div class="formulario">
                        <?php echo do_shortcode( '[bkFormFoot observaciones="" imagen="" origen="14" origenForm="contacto" formulario="100"][/bkFormFoot]' ); ?>
                    </div>

                </div> <?php // .imagen-text-cta-portada-promociones ?>

            </div> <?php // . container-general-info-promociones ?>
            
         </div> <?php // .container-promociones ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-promociones ?>




<?php get_footer(); ?>