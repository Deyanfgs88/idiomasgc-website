<?php

/*

Template Name: Clases de conversación

*/

get_header(); 
the_post(); ?>

<div class="template-clases-conver">
    <div class="container-fluid">
        <div class="container-clases-conver">
            <div class="cta-back-title-general-clases-conver">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-clases-conver">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_clases_conver'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-clases-conver">

                <div class="texto-info">
                    <?php the_field('texto_info_clases_conver'); ?>
                </div>

                <div class="ctas-clases-conver">
                    <a href="<?php the_field('boton_pagina_temas_clases_conver'); ?>">Temas</a>
                    <a href="<?php the_field('boton_pagina_horarios_clases_conver'); ?>">Horarios</a>
                </div> <?php // .ctas-clases-conver ?>

            </div> <?php // . container-general-info-clases-conver ?>
            
         </div> <?php // .container-clases-conver ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-clases-conver ?>




<?php get_footer(); ?>