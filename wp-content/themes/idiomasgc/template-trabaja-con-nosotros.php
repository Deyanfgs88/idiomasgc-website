<?php

/*

Template Name: Trabaja con nosotros

*/

get_header(); 
the_post(); ?>

<div class="template-trabaja-con-nosotros">
    <div class="container-fluid">
        <div class="container-trabaja-con-nosotros">
            <div class="cta-back-title-general-trabaja-con-nosotros">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-trabaja-con-nosotros">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_trabaja_nosotros'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-trabaja-con-nosotros">
                
                <?php
                    $texto_trabaja_nosotros = get_field('texto_principal_trabaja_nosotros');
                    if ($texto_trabaja_nosotros){
                ?>
                <div class="texto-trabaja-nosotros">
                    <h3><?php the_field('texto_principal_trabaja_nosotros'); ?></h3>
                </div>
                <?php } ?>

                <div class="imagen-mobile d-block d-xl-none">
                    <img src="<?php the_field('imagen_background_trabaja_nosotros'); ?>" alt="imagen trabaja con nosotros">
                </div> <?php // .imagen-mobile ?>

                <div class="container-bg-formulario" style="background-image: url('<?php the_field('imagen_background_trabaja_nosotros'); ?>');">
                    <div class="formulario">
                        <?php echo do_shortcode( '[contact-form-7 id="1727" title="Trabaja con nosotros"]' ); ?>
                    </div>
                </div>

            </div> <?php // . container-general-info-trabaja-con-nosotros ?>
            
         </div> <?php // .container-trabaja-con-nosotros ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-trabaja-con-nosotros ?>




<?php get_footer(); ?>