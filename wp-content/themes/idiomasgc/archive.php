<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package idiomasgc
 */

get_header();
?>

<div class="template-archive">
	<div class="container-fluid">
		<div class="container-archive">

			<div class="cta-back-archive">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
			</div>

			<div class="container-general-info-archive">
				<?php if ( have_posts() ) : ?>
					<div class="page-title">
						<?php the_archive_title( '<h1>', '</h1>' ); ?>
					</div>
					<div class="cont-result-posts">
						<?php
							while ( have_posts() ) :
								the_post();
								get_template_part( 'template-parts/content', get_post_type() );
							endwhile;

							the_posts_navigation();
						?>
					</div>
				<?php
					else :
						get_template_part( 'template-parts/content', 'none' );
					endif;
				?>

			</div> <?php // .container-general-info-archive ?>
			
		</div> <?php // .container-archive ?>
	</div> <?php // .container-fluid ?>
</div> <?php // .template-archive ?>


<?php get_footer(); ?>
