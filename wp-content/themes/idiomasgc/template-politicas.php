<?php

/*

Template Name: Politicas

*/

get_header(); 
the_post(); ?>

<div class="template-politicas">
    <div class="container-fluid">
        <div class="container-politicas">
            <div class="cta-back-title-general-politicas">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-politicas">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_politicas'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-politicas">

                <?php the_field('contenido_politicas') ?>

            </div> <?php // . container-general-info-politicas ?>
            
         </div> <?php // .container-politicas ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-politicas ?>




<?php get_footer(); ?>