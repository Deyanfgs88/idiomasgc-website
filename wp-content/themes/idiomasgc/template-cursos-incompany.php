<?php

/*

Template Name: Cursos Incompany

*/

get_header(); 
the_post(); ?>

<div class="template-cursos-incompany">
    <div class="container-fluid">
        <div class="container-cursos-incompany">
            <div class="cta-back-title-general-cursos-incompany">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pagina_incompany'); ?>"><i class="fas fa-chevron-left"></i>In company</a>
                </div>
                <div class="title-cursos-incompany">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_cursos_incompany'); ?></h2>
                </div>
            </div>

            <div class="container-general-cursos-incompany">

                <h3>Elige tu idioma</h3>

                <div class="container-list-idiomas">

                    <div class="list-idiomas">
    
                        <?php 
                            $lista_idiomas = get_field('lista_idiomas_cursos_incompany');
                            if ($lista_idiomas){
                                foreach ($lista_idiomas as $idioma) {
                                    echo '<div class="cont-item-language">';
                                    echo '<a href="' . $idioma['pagina_idioma_cursos_incompany'] . '">';
                                    echo '<div class="item-language">';
                                    echo '<img src="' . $idioma['imagen_idioma_cursos_incompany'] . '" alt="bandera">';
                                    echo '<p>' . $idioma['texto_idioma_cursos_incompany'] . '</p>';
                                    echo '</div>';
                                    echo '</a>';
                                    echo '</div>';
                                }
                            }
                        ?>
    
                    </div> <?php //.list-idiomas ?>

                </div> <?php // .container-list-idiomas ?>


            </div> <?php // . container-general-cursos-incompany ?>
            
         </div> <?php // .container-cursos-incompany ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-cursos-incompany ?>




<?php get_footer(); ?>