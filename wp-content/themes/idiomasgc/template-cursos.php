<?php

/*

Template Name: Cursos

*/

get_header(); 
the_post(); ?>

<div class="template-cursos">
    <div class="container-fluid">
        <div class="container-curso">
            <div class="cta-back-title-general-curso">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-curso">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_curso'); ?></h2>
                </div>          
            </div>

            <?php
                $texto_select_course = get_field('texto_seleccionar_curso');
                $list_select_courses = get_field('lista_seleccionar_cursos');
            ?>
            <div class="container-seleccionar-item-curso">
                <?php
                    if ($texto_select_course) {
                ?>
                <p><?php the_field('texto_seleccionar_curso'); ?></p>
                <?php } ?>
                <?php
                    if ($list_select_courses) {
                ?>
                <select class="seleccionar-curso">
                    <?php
                        $i = 0;
                        foreach ($list_select_courses as $course) {
                            $i++;
                            echo '<option value="curso-' . $i .'">' . $course['texto_curso'] . '</option>';
                        }
                    ?>
                </select>
                <?php } ?>
            </div> <?php // .container-seleccionar-item-curso ?>

            <div class="container-general-class">

                <div class="container-clases">
                    
                    <div id="curso-1" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_action_english'); ?>" alt="imagen action english">
                            <a href="<?php the_field('pagina_curso_informacion_action_english'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_action_english'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_action_english = get_field('duracion_action_english');
                                if ($duracion_action_english){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_action_english'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_action_english'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_curso_informacion_action_english'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_curso_reserva_ahora_action_english'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div id="curso-2" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_action_english_plus'); ?>" alt="imagen action english plus">
                            <a href="<?php the_field('pagina_curso_informacion_action_english_plus'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_action_english_plus'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_action_english_plus = get_field('duracion_action_english_plus');
                                if ($duracion_action_english_plus){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_action_english_plus'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_action_english_plus'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_curso_informacion_action_english_plus'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_curso_reserva_ahora_action_english_plus'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div id="curso-3" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_mini_grupos'); ?>" alt="imagen mini grupos">
                            <a href="<?php the_field('pagina_curso_informacion_mini_grupos'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_mini_grupos'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_mini_grupos = get_field('duracion_mini_grupos');
                                if ($duracion_mini_grupos){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_mini_grupos'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_mini_grupos'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_curso_informacion_mini_grupos'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_curso_reserva_ahora_mini_grupos'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
                    
                    <div id="curso-4" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_cursos_individuales'); ?>" alt="imagen cursos individuales">
                            <a href="<?php the_field('pagina_curso_informacion_cursos_individuales'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_cursos_individuales'); ?>
                                </div>
                            </a>
                            <?php
                                $duracion_individuales = get_field('duracion_individuales');
                                if ($duracion_individuales){
                            ?>
                            <div class="duracion-curso">
                                <div class="icon">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="text">
                                    <?php the_field('duracion_individuales'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="small-info">
                                <?php the_field('texto_curso_individuales'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_curso_informacion_cursos_individuales'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_curso_reserva_ahora_cursos_individuales'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
    
                </div> <?php // .container-clases ?>
            </div> <?php // .container-general-class ?>


        </div> <?php // .container-curso ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-cursos ?>



<?php get_footer(); ?>