<?php

/*

Template Name: Idiomas y formación en el extranjero SLIDER

*/

get_header(); 
the_post(); ?>

<div class="template-idiomas-extranjero-slider">
    <div class="container-fluid">
        <div class="container-idiomas-extranjero">
            <div class="cta-back-title-general-idiomas-extranjero">
                <div class="title-idiomas-extranjero">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                </div>          
            </div>

            <div class="container-slider">
                <div class="mobile d-md-none">
                    <?php echo do_shortcode('[smartslider3 alias="idiomas-extranjero-mobile"]'); ?>
                </div>
                <div class="tablet d-none d-md-block d-xl-none">
                    <?php echo do_shortcode('[smartslider3 alias="idiomas-extranjero-tablet"]'); ?>
                </div>
                <div class="desktop d-none d-xl-block">
                    <?php echo do_shortcode('[smartslider3 alias="idiomas-extranjero-desktop"]'); ?>
                </div> 
            </div> <?php // .container-slider ?>

            <div class="container-general-info-idiomas-extranjero">
                
                <div class="row">

                    <div class="col-xl-6">

                        <div class="container-general-destinos niños">

                            <div class="main-title">
                                <i class="fas fa-chevron-right"></i>
                                <div class="title">
                                    <span><?php the_field('titulo_destino_niños'); ?></span>
                                </div>          
                            </div> <?php // .main-title ?>

                            <?php
                                $lista_items_destinos_niños = get_field('lista_destinos_ninos');
                                if ($lista_items_destinos_niños) {
                                    foreach ($lista_items_destinos_niños as $destino) {
                            ?>
                            
                            <div class="item-destino">
                                <div class="imagen">
                                    <a href="<?php echo $destino['pagina_ver_detalles']; ?>">
                                        <img src="<?php echo $destino['imagen']; ?>" alt="<?php echo strip_tags($destino['titulo']); ?>">
                                    </a>
                                </div>
                                <div class="info">
                                    <a href="<?php echo $destino['pagina_ver_detalles']; ?>"><div class="titulo"><?php echo $destino['titulo']; ?></div></a>
                                    <div class="duration-availability-wrap">
                                        <div class="d-a-info"><i class="far fa-calendar-alt"></i><?php echo $destino['calendario']; ?></div>
                                    </div>
                                    <div class="content-text">
                                        <?php echo $destino['descripcion']; ?>
                                    </div>
                                    
                                    <div class="price">
                                        <span>Desde</span>
                                        <span class="money"><?php echo $destino['precio']; ?></span>
                                    </div>
                                    <div class="cta-details">
                                        <a href="<?php echo $destino['pagina_ver_detalles']; ?>">Ver detalles</a>
                                    </div>  
                                </div> <?php // .info ?>
                            </div> <?php // .item-destino ?>
                            <?php
                                }
                            }  
                            ?>

                        </div> <?php // .container-general-destinos ?>

                    </div> <?php // .col ?>
                    
                    <div class="col-xl-6">

                        <div class="container-general-destinos adultos">

                            <div class="main-title">
                                <i class="fas fa-chevron-right"></i>
                                <div class="title">
                                    <span><?php the_field('titulo_destino_adultos'); ?></span>
                                </div>          
                            </div> <?php // .main-title ?>

                            <?php
                                $lista_items_destinos_adultos = get_field('lista_destinos_adultos');
                                if ($lista_items_destinos_adultos) {
                                    foreach ($lista_items_destinos_adultos as $destino) {
                            ?>
                            
                            <div class="item-destino">
                                <div class="imagen">
                                    <a href="<?php echo $destino['pagina_ver_detalles']; ?>">
                                        <img src="<?php echo $destino['imagen']; ?>" alt="<?php echo strip_tags($destino['titulo']); ?>">
                                    </a>
                                </div>
                                <div class="info">    
                                    <a href="<?php echo $destino['pagina_ver_detalles']; ?>"><div class="titulo"><?php echo $destino['titulo']; ?></div></a>
                                    <div class="duration-availability-wrap">
                                        <div class="d-a-info"><i class="far fa-calendar-alt"></i><?php echo $destino['calendario']; ?></div>
                                    </div>
                                    <div class="content-text">
                                        <?php echo $destino['descripcion']; ?>
                                    </div>
                                    
                                    <div class="price">
                                        <span>Desde</span>
                                        <span class="money"><?php echo $destino['precio']; ?></span>
                                    </div>
                                    <div class="cta-details">
                                        <a href="<?php echo $destino['pagina_ver_detalles']; ?>">Ver detalles</a>
                                    </div>        
                                </div> <?php // .info ?>
                            </div> <?php // .item-destino ?>
                            <?php
                                }
                            }  
                            ?>

                        </div> <?php // .container-general-destinos ?>
                        
                    </div> <?php // .col ?>

                </div> <?php // .row ?>

                <?php
                    $texto_disclaimer_destinos = get_field('texto_disclaimer_destinos_completos');
                    if ($texto_disclaimer_destinos){
                ?>
                <div class="texto-disclaimer-destinos">
                    <?php the_field('texto_disclaimer_destinos_completos'); ?>
                </div>
                <?php } ?>

                <?php
                    $texto_igic = get_field('texto_igic_destinos_extranjero');
                    if ($texto_igic){
                ?>
                <div class="texto-igic">
                    <?php the_field('texto_igic_destinos_extranjero'); ?>
                </div>
                <?php } ?>
                
            </div> <?php // .container-general-info-idiomas-extranjero ?>

        </div> <?php // .container-idiomas-extranjero ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-idiomas-extranjero-slider ?>



<?php get_footer(); ?>