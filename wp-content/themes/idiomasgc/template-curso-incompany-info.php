<?php

/*

Template Name: Información Curso Incompany

*/

get_header(); 
the_post(); ?>

<div class="template-curso-incompany-info">
    <div class="container-fluid">
        <div class="container-curso-incompany-info">
            <div class="cta-back-title-general-curso-incompany-info">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_pagina_todos_programas_curso_incompany_info'); ?>"><i class="fas fa-chevron-left"></i>Todos los programas</a>
                </div>
                <div class="title-curso-incompany-info">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_curso_incompany_info'); ?></h2>
                </div>
            </div>

            <div class="container-general-curso-incompany-info" style="background-image: url('<?php the_field('imagen_background_curso_incompany_info'); ?>');">
                
                <div class="imagen-mobile d-block d-xl-none">
                    <img src="<?php the_field('imagen_background_curso_incompany_info'); ?>" alt="imagen curso profesionales y ejecutivos">
                </div> <?php // .imagen-mobile ?>

                <div class="container-info-curso-incompany">
                    <div class="info-curso-incompany">

                        <?php
                            $texto_info_curso = get_field('texto_info_curso_incompany_info');
                            if ($texto_info_curso){
                        ?>
                        <div class="item-info">
                            <?php the_field('texto_info_curso_incompany_info'); ?>
                        </div> <?php // .item-info ?>
                        <?php } ?>

                        <div class="item-info">

                            <?php
                                $objetivos_especificos = get_field('objetivos_especificos_ejemplos_curso_incompany_info');
                                if ($objetivos_especificos){
                            ?>
                            <div class="item">
                                <h3>Objetivos específicos (ejemplos):</h3>
                                <?php the_field('objetivos_especificos_ejemplos_curso_incompany_info'); ?>
                            </div>
                            <?php } ?>

                            <?php
                                $niveles = get_field('niveles_curso_incompany_info');
                                if ($niveles){
                            ?>
                            <div class="item">
                                <h3>Niveles</h3>
                                <?php the_field('niveles_curso_incompany_info'); ?>
                            </div>
                            <?php } ?>

                            <?php
                                $duracion = get_field('duracion_curso_incompany_info');
                                if ($duracion){
                            ?>
                            <div class="item">
                                <h3>Duración:</h3>
                                <?php the_field('duracion_curso_incompany_info'); ?>
                            </div>
                            <?php } ?>

                        </div> <?php // .item-info ?>

                        <div class="item-info">
                            <a href="<?php the_field('pagina_reserva_ahora_curso_incompany_info'); ?>" class="cta-reservar">Reserva ahora</a>
                        </div> <?php // .item-info ?>

                    </div> <?php // .info-curso-incompany ?>
                </div> <?php // .container-info-curso-incompany ?>

            </div> <?php // .container-general-curso-incompany-info ?>
            
         </div> <?php // .container-curso-incompany-info ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-curso-incompany-info ?>


<?php get_footer(); ?>