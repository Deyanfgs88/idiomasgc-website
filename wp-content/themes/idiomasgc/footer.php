<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package idiomasgc
 */

?>

	</div><!-- #content -->

	<div class="container-newsletter">
		<div class="container-fluid">
			<div class="cont-form-orion">
				<div class="texto">
					<h3><?php echo get_theme_mod('titulo_asesoramiento_form'); ?></h3>
					<p><?php echo get_theme_mod('texto_asesoramiento_form'); ?></p>
				</div>
				<div class="formulario">
					<?php echo do_shortcode( '[bkFormFoot observaciones="" imagen="" origen="14" origenForm="contacto" formulario="100"][/bkFormFoot]' ); ?>
				</div>
			</div>
			<div class="bg-opacity"></div>
		</div>
	</div>

	<footer id="colophon" class="footer">
		<div class="container-fluid">
			<div class="container-footer">

				<div class="container-social-sedes">

					<div class="cont-rss-sedes cont-footer">
						<div class="social-networks">
							<a href="<?php echo get_theme_mod('facebook'); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
							<a href="<?php echo get_theme_mod('twitter'); ?>" target="_blank"><i class="fab fa-twitter"></i></a>
							<a href="<?php echo get_theme_mod('instagram'); ?>" target="_blank"><i class="fab fa-instagram"></i></a>
							<a href="<?php echo get_theme_mod('youtube'); ?>" target="_blank"><i class="fab fa-youtube"></i></a>
							<a href="<?php echo get_theme_mod('linkedin'); ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
						</div> <?php // .social-networks ?>

						<div class="sedes">
							<div class="address">
								<i class="fas fa-map-marker-alt"></i>
								<p><?php echo get_theme_mod('address_tomasmorales'); ?></p>
								<a href="mailto:<?php echo get_theme_mod('email_tomasmorales'); ?>"><?php echo get_theme_mod('email_tomasmorales'); ?></a>
							</div>
							<div class="address">
								<i class="fas fa-map-marker-alt"></i>
								<p><?php echo get_theme_mod('address_mesaylopez'); ?></p>
								<a href="mailto:<?php echo get_theme_mod('email_mesaylopez'); ?>"><?php echo get_theme_mod('email_mesaylopez'); ?></a>
							</div>
							<div class="address">
								<i class="fas fa-map-marker-alt"></i>
								<p><?php echo get_theme_mod('address_sietepalmas'); ?></p>
								<a href="mailto:<?php echo get_theme_mod('email_sietepalmas'); ?>"><?php echo get_theme_mod('email_sietepalmas'); ?></a>
							</div>
						</div> <?php // .sedes ?>

					</div> <?php // .cont-rss-sedes ?>


					<div class="horario-general">
						<div class="texto">
							<i class="fas fa-clock"></i><?php echo get_theme_mod('horario_general'); ?>
						</div>
					</div>

				</div>


				<div class="container-politicas-copyright cont-footer">
					<div class="cont-politicas">
						<?php
							$link_trabaja_nosotros = get_theme_mod('trabaja_nosotros');
							$link_privacidad = get_theme_mod('politica_privacidad');
							$link_cookies = get_theme_mod('politica_cookies');
							$link_contratacion_cursos = get_theme_mod('politica_contratacion_cursos');
							$link_aviso_legal = get_theme_mod('aviso_legal');
						?>
						<div class="politicas">
							<a href="<?php echo get_permalink($link_trabaja_nosotros); ?>"><i class="fas fa-chevron-right"></i>Trabaja con nosotros</a>
							<a href="<?php echo get_permalink($link_privacidad); ?>"><i class="fas fa-chevron-right"></i>Política de privacidad</a>
							<a href="<?php echo get_permalink($link_cookies); ?>"><i class="fas fa-chevron-right"></i>Política de cookies</a>
						</div>
						<div class="politicas">
							<a href="<?php echo get_permalink($link_contratacion_cursos); ?>"><i class="fas fa-chevron-right"></i>Política de contratación de cursos</a>
							<a href="<?php echo get_permalink($link_aviso_legal); ?>"><i class="fas fa-chevron-right"></i>Aviso legal</a>
						</div>
					</div> <?php // .cont-politicas ?>

					<div class="copyright">
						<p><i class="far fa-copyright"></i><?php echo get_theme_mod('copyright'); ?></p>
					</div>

				</div> <?php // .cont-footer ?>

			</div> <?php //.container-footer ?>
		</div> <?php // .container-fluid ?>
	</footer> <?php // .footer ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
