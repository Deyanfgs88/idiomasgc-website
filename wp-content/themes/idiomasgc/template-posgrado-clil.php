<?php

/*

Template Name: Posgrado CLILL-ULL

*/

get_header(); 
the_post(); ?>

<div class="template-formac-profesorado-info">
    <div class="container-fluid">
        <div class="container-formac-prof-info">
            <div class="cta-back-title-general-formac-info">
                <div class="cta-back-pagina">
                    <a href="<?php the_field('boton_formac_profesorado'); ?>"><i class="fas fa-chevron-left"></i>Formación del profesorado</a>
                </div>
                <div class="title-formac-prof">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_posgrado_clill'); ?></h2>
                </div>
            </div>

            <div class="container-general-formac-prof-info">

                <div class="imagen-text-portada-formac-prof">
                    <img src="<?php the_field('imagen_portada_posgrado_clill'); ?>" alt="imagen portada posgrado clill">
                    <div class="text-info-formac-prof">
                        <?php the_field('texto_informacion_posgrado_clill'); ?>
                    </div>
                </div> <?php // .imagen-text-portada-formac-prof ?>

                <div class="item-info-formac-prof">
                    <h4>Destinatarios:</h4>
                    <?php
                        $lista_destinatarios_posgrado_clill = get_field('lista_destinatarios_posgrado_clill');
                        if ($lista_destinatarios_posgrado_clill){
                            echo '<ul>';
                            foreach ($lista_destinatarios_posgrado_clill as $destinario_posgrado_clill) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $destinario_posgrado_clill['destinatario_posgrado_clill'] . '</li>';
                            }
                            echo '</ul>';
                        }
                    ?>
                </div>

                <div class="item-info-formac-prof">
                    <h4>Competencias generales:</h4>
                    <?php the_field('competencias_generales_posgrado_clill'); ?>
                </div>

                <div class="item-info-formac-prof">
                    <h4>Detalles:</h4>
                    <?php
                        $lista_detalles_posgrado_clill = get_field('lista_detalles_posgrado_clill');
                        if ($lista_detalles_posgrado_clill){
                            echo '<ul>';
                            foreach ($lista_detalles_posgrado_clill as $detalle_posgrado_clill) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $detalle_posgrado_clill['detalle_posgrado_clill'] . '</li>';
                            }
                            echo '</ul>';
                        }
                    ?>
                </div>

                <div class="item-info-formac-prof">
                    <h4><i class="fas fa-euro-sign"></i>Precio:</h4>
                    <?php
                        $lista_precios_posgrado_clill = get_field('lista_precio_posgrado_clill');
                        if ($lista_precios_posgrado_clill){
                            echo '<ul>';
                            foreach ($lista_precios_posgrado_clill as $precio_posgrado_clill) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $precio_posgrado_clill['precio_posgrado_clill'] . '</li>';
                            }
                            echo '</ul>';
                        }
                    ?>
                </div>
                
                <div class="item-info-formac-prof">
                    <h4>Reuniones informativas:</h4>
                    <?php
                        $lista_reuniones_informativas_posgrado_clill = get_field('lista_reuniones_informativas_posgrado_clill');
                        if ($lista_reuniones_informativas_posgrado_clill){
                            echo '<ul>';
                            foreach ($lista_reuniones_informativas_posgrado_clill as $reunion_informativa_posgrado_clill) {
                                echo '<li><i class="fas fa-chevron-right"></i>' . $reunion_informativa_posgrado_clill['reunion_informativa_posgrado_clill'] . '</li>';
                            }
                            echo '</ul>';
                        }
                    ?>    
                </div>

                <div class="item-info-formac-prof ctas">
                    <a href="<?php the_field('boton_folleto_informativo_posgrado_clill'); ?>" target="_blank">Descarga el folleto informativo</a>
                    <a href="<?php the_field('boton_solicitud_inscripcion_sietepalmas_posgrado_clill'); ?>" target="_blank">Descarga la solicitud de inscripción - 7Palmas</a>
                    <a href="<?php the_field('boton_solicitud_inscripcion_mesaylopez_posgrado_clill'); ?>" target="_blank">Descarga la solicitud de inscripción - Mesa y López</a>
                    <a href="<?php the_field('boton_solicitud_inscripcion_tomasmorales_posgrado_clill'); ?>" target="_blank">Descarga la solicitud de inscripción - Tomás Morales</a>
                    <a href="<?php the_field('boton_reserva_ahora_posgrado_clill'); ?>">Reserva ahora</a>
                </div>

            </div> <?php // . container-general-formac-prof-info ?>
            
         </div> <?php // .container-formac-prof-info" ?>
    </div> <?php // .container-fluid ?>
</div><? // .template-formac-profesorado-info ?>




<?php get_footer(); ?>