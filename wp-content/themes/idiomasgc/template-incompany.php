<?php

/*

Template Name: Incompany

*/

get_header(); 
the_post(); ?>

<div class="template-incompany">
    <div class="container-fluid">
        <div class="container-incompany">
            <div class="cta-back-title-general-incompany">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-incompany">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_incompany'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-incompany">
                <div class="item-info imagen-portada">
                    <img src="<?php the_field('imagen_portada_incompany'); ?>" alt="imagen portada incompany">
                </div>
                
                <div class="item-info ctas-revista-cursos">
                    <a href="<?php the_field('boton_revista_incompany'); ?>" target="_blank">Revista en PDF</a>
                    <a href="<?php the_field('boton_nuestros_cursos_incompany'); ?>">Nuestros cursos</a>
                </div>
                
                <?php
                    $titulo_formacion_esencial_empresa = get_field('titulo_esencial_formacion_empresa_incompany');
                    $texto_formacion_esencial_empresa = get_field('texto_esencial_formacion_empresa_incompany');
                    if ($titulo_formacion_esencial_empresa && $texto_formacion_esencial_empresa){
                ?>
                <div class="item-info">
                    <h3><?php the_field('titulo_esencial_formacion_empresa_incompany'); ?></h3>
                    <?php the_field('texto_esencial_formacion_empresa_incompany'); ?>
                </div>
                <?php } ?>

                <?php
                    $titulo_razones_globales_empresa = get_field('titulo_razones_globales_incompany');
                    $lista_razones_globales_empresa = get_field('lista_razones_globales_incompany');
                    if ($titulo_razones_globales_empresa && $lista_razones_globales_empresa){
                ?>
                <div class="item-info lista-razones-empresa">
                    <h3><?php the_field('titulo_razones_globales_incompany'); ?></h3>
                    <?php
                        echo '<ul>';
                        foreach($lista_razones_globales_empresa as $razon_global){
                            echo '<li><i class="fas fa-chevron-right"></i>' . $razon_global['razon_global_incompany'] . '</li>';
                        }
                        echo '</ul>';
                    ?>
                </div>
                <?php } ?>

                <?php
                    $titulo_razones_globales_empleado = get_field('titulo_razones_globales_empleado_incompany');
                    $lista_razones_globales_empleado = get_field('lista_razones_globales_empleado_incompany');
                    if ($titulo_razones_globales_empleado && $lista_razones_globales_empleado){
                ?>
                <div class="item-info lista-razones-empleado">
                    <h3><?php the_field('titulo_razones_globales_empleado_incompany'); ?></h3>
                    <?php
                        echo '<ul>';
                        foreach($lista_razones_globales_empleado as $razon_global){
                            echo '<li><i class="fas fa-chevron-right"></i>' . $razon_global['razon_global_empleado_incompany'] . '</li>';
                        }
                        echo '</ul>';
                    ?>
                </div>
                <?php } ?>

                <?php
                    $titulo_porque_language_campus = get_field('titulo_porque_lc_incompany');
                    $lista_porque_language_campus = get_field('lista_porque_lc_incompany');
                    if ($titulo_porque_language_campus && $lista_porque_language_campus){
                ?>
                <div class="item-info porque-language-campus">
                    <h3><?php the_field('titulo_porque_lc_incompany'); ?></h3>
                    <?php
                        echo '<ul>';
                        foreach($lista_porque_language_campus as $motivos){
                            echo '<li><i class="fas fa-chevron-right"></i><span>' . $motivos['titulo_motivo_porque_lc_incompany'] . '</span>' . $motivos['texto_motivo_porque_lc_incompany'] . '</li>';
                        }
                        echo '</ul>';
                    ?>
                </div>
                <?php } ?>

                <?php
                    $titulo_fases_proceso_formativo = get_field('titulo_fases_proceso_formativo_incompany');
                    $imagen_fases_proceso_formativo = get_field('imagen_fases_proceso_formativo_incompany');
                    if ($titulo_fases_proceso_formativo && $imagen_fases_proceso_formativo){
                ?>
                <div class="item-info fases-proceso-formativo">
                    <h3><?php the_field('titulo_fases_proceso_formativo_incompany'); ?></h3>
                    <div class="imagen-fases">
                        <img src="<?php the_field('imagen_fases_proceso_formativo_incompany'); ?>" alt="imagen fases proceso formativo">
                    </div>
                </div>
                <?php } ?>

                <?php
                    $titulo_niveles_incompany = get_field('titulo_nuetros_niveles_incompany');
                    $imagen_niveles_incompany = get_field('imagen_nuestros_niveles_incompany');
                    if ($titulo_niveles_incompany && $imagen_niveles_incompany){
                ?>
                <div class="item-info nuestros-niveles">
                    <h3><?php the_field('titulo_nuetros_niveles_incompany'); ?></h3>
                    <div class="imagen-niveles">
                        <img src="<?php the_field('imagen_nuestros_niveles_incompany'); ?>" alt="imagen nuestros niveles">
                    </div>
                </div>
                <?php } ?>

                <?php
                    $imagen_tripartita = get_field('imagen_tripartita_incompany');
                    $titulo_formacion_bonificable = get_field('titulo_formacion_bonificable_incompany');
                    $texto_formacion_bonificable = get_field('texto_formacion_bonificable_incompany');
                    if ($imagen_tripartita && $titulo_formacion_bonificable && $texto_formacion_bonificable){
                ?>
                <div class="item-info formacion-bonificable">
                    <div class="imagen-tripartita">
                        <img src="<?php the_field('imagen_tripartita_incompany'); ?>" alt="imagen tripartita">
                    </div>
                    <h3><?php the_field('titulo_formacion_bonificable_incompany'); ?></h3>
                    <?php the_field('texto_formacion_bonificable_incompany'); ?>
                </div>
                <?php } ?>

            </div> <?php // . container-general-info-incompany ?>
            
         </div> <?php // .container-incompany ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-incompany ?>




<?php get_footer(); ?>