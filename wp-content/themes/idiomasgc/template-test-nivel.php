<?php

/*

Template Name: Test Nivel

*/

get_header(); 
the_post(); ?>

<div class="template-test-nivel template-cursos">
    <div class="container-fluid">
        <div class="container-test-nivel">
            <div class="cta-back-title-test-nivel">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-test">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_test_nivel'); ?></h2>
                </div>          
            </div>

            <?php
                $titulo_principal_adultos_test = get_field('titulo_principal_adultos_test_nivel');
                if ($titulo_principal_adultos_test){
            ?>
            <h2><?php the_field('titulo_principal_adultos_test_nivel'); ?></h2>
            <?php } ?>

            <div class="container-general-items-languages">

                <div class="container-items">
                    
                    <div class="cont-item">
                        <div class="item-language">
                            <img src="<?php the_field('imagen_test_ingles_adultos'); ?>" alt="imagen test inglés adultos">
                            <h3><?php the_field('titulo_test_ingles_adultos'); ?></h3>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_test_ingles_adultos'); ?>">Realizar Test</a>
                            </div>
                        </div> <?php // .item-language ?>
                    </div>
    
                    <div class="cont-item">
                        <div class="item-language">
                            <img src="<?php the_field('imagen_test_aleman_adultos'); ?>" alt="imagen test alemán adultos">
                            <h3><?php the_field('titulo_test_aleman_adultos'); ?></h3>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_test_aleman_adultos'); ?>">Realizar Test</a>
                            </div>
                        </div> <?php // .item-language ?>
                    </div>
    
                    <div class="cont-item">
                        <div class="item-language">
                            <img src="<?php the_field('imagen_test_frances_adultos'); ?>" alt="imagen test francés adultos">
                            <h3><?php the_field('titulo_test_frances_adultos'); ?></h3>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_test_frances_adultos'); ?>">Realizar Test</a>
                            </div>
                        </div> <?php // .item-language ?>
                    </div>
                    
                    <div class="cont-item">
                        <div class="item-language">
                            <img src="<?php the_field('imagen_test_italiano_adultos'); ?>" alt="imagen test italiano adultos">
                            <h3><?php the_field('titulo_test_italiano_adultos'); ?></h3>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_test_italiano_adultos'); ?>">Realizar Test</a>                            </div>
                        </div> <?php // .item-language ?>
                    </div>

                    <div class="cont-item">
                        <div class="item-language">
                            <img src="<?php the_field('imagen_test_portugues_adultos'); ?>" alt="imagen test portugués adultos">
                            <h3><?php the_field('titulo_test_portugues_adultos'); ?></h3>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_test_portugues_adultos'); ?>">Realizar Test</a>                            </div>
                        </div> <?php // .item-language ?>
                    </div>

                    <div class="cont-item">
                        <div class="item-language">
                            <img src="<?php the_field('imagen_test_espanol_adultos'); ?>" alt="imagen test español adultos">
                            <h3><?php the_field('titulo_test_espanol_adultos'); ?></h3>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_test_espanol_adultos'); ?>">Realizar Test</a>                            </div>
                        </div> <?php // .item-language ?>
                    </div>

                    <div class="cont-item">
                        <div class="item-language">
                            <img src="<?php the_field('imagen_test_ruso_adultos'); ?>" alt="imagen test ruso adultos">
                            <h3><?php the_field('titulo_test_ruso_adultos'); ?></h3>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_test_ruso_adultos'); ?>">Realizar Test</a>                            </div>
                        </div> <?php // .item-language ?>
                    </div>
    
    
                </div> <?php // .container-items ?>
            </div> <?php // .container-general-items-languages ?>

            <?php
                $titulo_principal_ninos_test = get_field('titulo_principal_ninos_test_nivel');
                if ($titulo_principal_ninos_test){
            ?>
            <h2><?php the_field('titulo_principal_ninos_test_nivel'); ?></h2>
            <?php } ?>

            <div class="container-general-items-languages niños">

                <div class="container-items">

                     <div class="cont-item">
                        <div class="item-language">
                            <img src="<?php the_field('imagen_test_ingles_primaria_ninos'); ?>" alt="imagen test inglés primaria niños">
                            <h3><?php the_field('titulo_test_ingles_primaria_ninos'); ?></h3>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_test_ingles_primaria_ninos'); ?>">Realizar Test</a>                            </div>
                        </div> <?php // .item-language ?>
                    </div>

                    <div class="cont-item">
                        <div class="item-language">
                            <img src="<?php the_field('imagen_test_ingles_eso_bach_ninos'); ?>" alt="imagen test inglés ESO y Bachillerato niños">
                            <h3><?php the_field('titulo_test_ingles_eso_bach_ninos'); ?></h3>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_test_ingles_eso_bach_ninos'); ?>">Realizar Test</a>                            </div>
                        </div> <?php // .item-language ?>
                    </div>

                </div>
            </div>

        </div> <?php // .container-curso ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-test-nivel ?>



<?php get_footer(); ?>