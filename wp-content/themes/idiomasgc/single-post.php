<?php

get_header(); 
the_post(); ?>

<div class="template-single-post">
    <div class="container-fluid">
        <div class="container-single-post">
            <div class="cta-back-title-general-single-post">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>/blog"><i class="fas fa-chevron-left"></i>Volver al blog</a>
                </div>
                <div class="title-single-post">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i>Blog</h2>
                </div>
            </div>

            <div class="container-general-info-single-post">
                <?php
                 if ( has_post_thumbnail() ) {
                ?>
                <div class="imagen">
                    <?php the_post_thumbnail('full'); ?>
                </div>
                 <?php } ?>

                 <div class="info-post">
                     <h2><?php echo get_the_title(); ?></h2>
                     <?php the_content(); ?>
                 </div>

                 <div class="autor-fecha">
                     <p>By <?php echo get_the_author(); ?></p>
                     <p><?php echo get_the_date('j \d\e F Y'); ?></p>
                 </div>

                 <div class="share-social-networks">
                    <p>Compartir</p>
                    <?php
                        $pin_imagen = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); 
                    ?>
                    <div class="social-networks">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>" target="_blank"><i class="fab fa-twitter"></i></a>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=&source=<?php bloginfo('name'); ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                        <a href="https://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo $pin_imagen[0]; ?>" target="_blank"><i class="fab fa-pinterest-p"></i></a>
                    </div>
                 </div>

                <?php
                    $custom_query_args = array( 
                        'post_type' => 'post',
                        'posts_per_page' => 3, 
                        'post__not_in' => array($post->ID),
                        'orderby' => 'rand', 
                    );
                    $custom_query = new WP_Query( $custom_query_args );
                    if ( $custom_query->have_posts() ) {
                ?>
                 <div class="contenido-relacionado">
                    <p>Contenido relacionado</p>
                    <div class="cont-post-related">
                        <?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
                            <?php if ( has_post_thumbnail() ) { ?>
                                <a href="<?php the_permalink(); ?>">
                                    <div class="image-post-related-bg" style="background-image: url('<?php the_post_thumbnail_url('full') ?>')">
                                        <div class="overlay"><p class="title-post-hover"><?php echo get_the_title(); ?></p><i class="fas fa-chevron-right"></i></div>
                                    </div>
                                    <h4 class="title-post-related d-xl-none"><?php echo get_the_title(); ?></h4>
                                </a>
                            <?php } ?>
                        <?php endwhile; ?>  
                        <?php wp_reset_postdata(); ?>  
                    </div>    
                </div>
                <?php } ?>

            </div> <?php // . container-general-info-single-post ?>
            
         </div> <?php // .container-single-post ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-single-post ?>




<?php get_footer(); ?>