<?php

/*

Template Name: Reviews

*/

get_header(); 
the_post(); ?>

<div class="template-reviews">
    <div class="container-fluid">
        <div class="container-reviews">
            <div class="cta-back-title-general-reviews">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-reviews">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_reviews'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-reviews">

                <?php
                    $titulo_reviews = get_field('titulo_reviews');
                    $texto_reviews = get_field('texto_reviews');
                    if ($titulo_reviews && $texto_reviews){
                ?>
                <div class="container-title-text-reviews">
                    <div class="titulo">
                        <?php the_field('titulo_reviews'); ?>
                    </div>
                    <?php the_field('texto_reviews'); ?>
                </div> <?php // .container-title-text-reviews ?>
                <?php } ?>

                <div class="container-sedes">
                    <div class="row">

                        <div class="col-12 col-lg-4">
                            <?php
                                $titulo_sede_tomas_morales = get_field('titulo_sede_tomas_morales_ubica_gc');
                                $imagen_sede_tomas_morales = get_field('imagen_sede_tomas_morales_ubica_gc');
                                if ($titulo_sede_tomas_morales && $imagen_sede_tomas_morales){
                            ?>
                            <div class="cont-sede">
                                <div class="sede">
                                    <div class="titulo-sede">
                                        <?php the_field('titulo_sede_tomas_morales_ubica_gc'); ?>
                                    </div>
                                    <a href="<?php the_field('pagina_sede_tomas_morales_ubica_gc'); ?>">
                                        <div class="imagen-bg-sede" style="background-image: url('<?php the_field('imagen_sede_tomas_morales_ubica_gc'); ?>')">
                                            <div class="overlay"><i class="fas fa-chevron-right"></i></div>
                                        </div>
                                    </a>
                                    <div class="ctas-google-fb">
                                        <a href="<?php the_field('url_reseña_google_tomas_morales'); ?>" target="_blank">Reseña en Google</a>
                                        <a href="<?php the_field('url_reseña_fb_tomas_morales'); ?>" target="_blank">Reseña en Facebook</a>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div> <?php // .col ?>

                        <div class="col-12 col-lg-4">
                            <?php
                                $titulo_sede_mesa_lopez = get_field('titulo_sede_mesa_lopez_ubica_gc');
                                $imagen_sede_mesa_lopez = get_field('imagen_sede_mesa_lopez_ubica_gc');
                                if ($titulo_sede_mesa_lopez && $imagen_sede_mesa_lopez){
                            ?>
                            <div class="cont-sede">
                                <div class="sede">
                                    <div class="titulo-sede">
                                        <?php the_field('titulo_sede_mesa_lopez_ubica_gc'); ?>
                                    </div>
                                    <a href="<?php the_field('pagina_sede_mesa_lopez_ubica_gc'); ?>">
                                        <div class="imagen-bg-sede" style="background-image: url('<?php the_field('imagen_sede_mesa_lopez_ubica_gc'); ?>')">
                                            <div class="overlay"><i class="fas fa-chevron-right"></i></div>
                                        </div>
                                    </a>
                                    <div class="ctas-google-fb">
                                        <a href="<?php the_field('url_reseña_google_mesa_lopez'); ?>" target="_blank">Reseña en Google</a>
                                        <a href="<?php the_field('url_reseña_fb_mesa_lopez'); ?>" target="_blank">Reseña en Facebook</a>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div> <?php // .col ?>

                        <div class="col-12 col-lg-4">
                            <?php
                                $titulo_sede_siete_palmas = get_field('titulo_sede_siete_palmas_ubica_gc');
                                $imagen_sede_siete_palmas = get_field('imagen_sede_siete_palmas_ubica_gc');
                                if ($titulo_sede_siete_palmas && $imagen_sede_siete_palmas){
                            ?>
                            <div class="cont-sede">
                                <div class="sede">
                                    <div class="titulo-sede">
                                        <?php the_field('titulo_sede_siete_palmas_ubica_gc'); ?>
                                    </div>
                                    <a href="<?php the_field('pagina_sede_siete_palmas_ubica_gc'); ?>">
                                        <div class="imagen-bg-sede" style="background-image: url('<?php the_field('imagen_sede_siete_palmas_ubica_gc'); ?>')">
                                            <div class="overlay"><i class="fas fa-chevron-right"></i></div>
                                        </div>
                                    </a>
                                    <div class="ctas-google-fb">
                                        <a href="<?php the_field('url_reseña_google_siete_palmas'); ?>" target="_blank">Reseña en Google</a>
                                        <a href="<?php the_field('url_reseña_fb_siete_palmas'); ?>" target="_blank">Reseña en Facebook</a>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div> <?php // .col ?>

                    </div> <?php // .row ?>
                </div> <?php // .container-sedes ?>

            </div> <?php // . container-general-info-reviews ?>
            
         </div> <?php // .container-reviews ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-reviews ?>


<?php get_footer(); ?>