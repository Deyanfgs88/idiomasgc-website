<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package idiomasgc
 */

get_header();
?>

<div class="template-search">
	<div class="container-fluid">
		<div class="container-search">
			<div class="cta-back-search">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
			</div>

			<div class="container-general-info-search">

				<?php if ( have_posts() ) : ?>
					<div class="page-title">
						<h1>
						<?php
						/* translators: %s: search query. */
						printf( esc_html__( 'Resultados de búsqueda para: %s', 'idiomasgc' ), '<span>' . get_search_query() . '</span>' );
						?>
						</h1>
					</div>
					<div class="cont-result-posts">
						<?php
							while ( have_posts() ) :
								the_post();
								get_template_part( 'template-parts/content', 'search' );

							endwhile;

							the_posts_navigation();
						?>
					</div>
				<?php
					else :
						get_template_part( 'template-parts/content', 'none' );
					endif;
				?>
			</div>

		</div> <?php // .container-search ?>
	</div> <?php // .container-fluid ?>
</div> <?php // .template-search ?>

<?php get_footer(); ?>
