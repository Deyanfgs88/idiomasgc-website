<?php

/*

Template Name: Formación Profesorado

*/

get_header(); 
the_post(); ?>

<div class="template-cursos">
    <div class="container-fluid">
        <div class="container-curso">
            <div class="cta-back-title-general-curso">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-curso">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_formac_profesorado'); ?></h2>
                </div>          
            </div>

            <div class="container-general-class">

                <div class="container-clases">
                    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_posgrado_clil'); ?>" alt="imagen posgrado CLIL-ULL">
                            <a href="<?php the_field('pagina_mas_informacion_posgrado_clil'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_posgrado_clil'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_posgrado_clil'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_posgrado_clil'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_posgrado_clil'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_certesol'); ?>" alt="imagen certesol">
                            <a href="<?php the_field('pagina_mas_informacion_certesol'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_certesol'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_certesol'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_certesol'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_certesol'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_formac_linguistica'); ?>" alt="imagen formación lingüistica">
                            <a href="<?php the_field('pagina_mas_informacion_formac_linguistica'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_formac_linguistica'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_formac_linguistica'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_formac_linguistica'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_formac_linguistica'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
                    
                    <div class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_tripartita'); ?>" alt="imagen tripartita">
                            <a href="<?php the_field('pagina_mas_informacion_tripartita'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_tripartita'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_tripartita'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_mas_informacion_tripartita'); ?>">Más Información</a>
                                <a href="<?php the_field('pagina_reserva_ahora_tripartita'); ?>">Reserva ahora</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
    
                </div> <?php // .container-clases ?>
            </div> <?php // .container-general-class ?>


        </div> <?php // .container-curso ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-cursos ?>



<?php get_footer(); ?>