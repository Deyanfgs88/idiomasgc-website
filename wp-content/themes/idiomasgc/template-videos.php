<?php

/*

Template Name: Videos

*/

get_header(); 
the_post(); ?>

<div class="template-videos">
    <div class="container-fluid">
        <div class="container-videos">
            <div class="cta-back-title-general-videos">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-videos">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_videos'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-videos">

                <div class="container-channel-youtube">
                    
                    <div class="cont-channel-youtube">
                        <div class="title">
                            <h2><i class="fas fa-chevron-right"></i><?php the_field('titulo_lista_videos_1'); ?></h2>
                        </div>
                        <?php echo do_shortcode( '[yotuwp type="playlist" id="PLhWDYe-hJq9ki3mH2jI_t-NmTEQakboQ2" ]' ); ?>
                    </div>

                    <div class="cont-channel-youtube">
                        <div class="title">
                            <h2><i class="fas fa-chevron-right"></i><?php the_field('titulo_lista_videos_2'); ?></h2>
                        </div>
                        <?php echo do_shortcode( '[yotuwp type="playlist" id="PLhWDYe-hJq9lhL3JXDzy3PPEvpZsCNBoW" ]' ); ?>
                    </div>

                </div> <?php // .imagen-text-cta-portada-videos ?>

            </div> <?php // . container-general-info-videos ?>
            
         </div> <?php // .container-videos ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-videos ?>




<?php get_footer(); ?>