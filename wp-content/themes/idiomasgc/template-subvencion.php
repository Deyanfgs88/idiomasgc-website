<?php

/*

Template Name: Subvención

*/

get_header(); 
the_post(); ?>

<div class="template-subvencion">
    <div class="container-fluid">
        <div class="container-subvencion">
            <div class="cta-back-title-general-subvencion">
                <div class="cta-back-pagina">
                    <a href="<?php echo get_home_url(); ?>"><i class="fas fa-chevron-left"></i>Página principal</a>
                </div>
                <div class="title-subvencion">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_subvencion'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-subvencion">

                <div class="imagen-text-portada-subvencion">
                    <img src="<?php the_field('imagen_portada_subvencion'); ?>" alt="imagen portada subvencion">
                    <div class="text-info-subvencion">
                        <?php the_field('texto_informacion_subvencion'); ?>
                    </div>
                </div> <?php // .imagen-text-cta-portada-subvencion ?>

            </div> <?php // . container-general-info-subvencion ?>
            
         </div> <?php // .container-subvencion ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-subvencion ?>




<?php get_footer(); ?>