<?php
/*
Plugin Name: OrionForm
Description: Formularios personalisados para el Orión Adimin System
Author: Bernardo Klein Salas
Version: 2.0.0
*/

// $dir = plugins_url();
// wp_enqueue_style( 'style',  $dir.'/Orionform/css/style.css' );


function lc_home_form( $atts , $content = null ) {
	// Attributes
	extract( shortcode_atts(
		array(
			'observaciones' => '',
			'imagen' => '',
			'origen' => '',
			'origenForm' => '',			
			'formulario' => '',			
			'color_boton' => '',			
			'texto_boton' => '',			
			'gracias' => '',
		), $atts )
	);

$laURLg = ($gracias == '') ? 'https://www.lcidiomasgc.com/gracias/':$gracias;
$textoB = ($texto_boton == '') ? 'ENVIAR':$texto_boton;
$colorB = ($color_boton == '') ? '':' style="background-color:'.$color_boton.' !important;" ';
	// Code

	$lcBox = '<a name="formularioLC'.$formulario.'"></a>
	<img src="'.$imagen.'" style="margin:-15px 0px 0px 0px; width:100%; border:1px solid #FFF" />

<form id="forma'.$formulario.'" name="forma'.$formulario.'" method="post">

<input type="hidden" name="forma" value="'.$origen.'"  /><input type="hidden" name="origenCRM" value="'.$origen.'" />
<input type="hidden" name="laURL" value="'.$laURLg.'" />

<input type="hidden" name="origenFORM" value="'.$origenForm.'" /><input type="hidden" name="origen" value="14" /><input id="nombre'.$formulario.'" type="text" name="nombre" class="bkform" placeholder="Nombre*" />

<div class="clearBKform"></div>
<input id="apellidos" type="text" name="apellidos"  class="bkform" placeholder="Apellidos*" />
<div class="clearBKform"></div>
<input id="emilio" type="text" name="emilio" class="bkform" placeholder="E-mail*" />
<div class="clearBKform"></div>
<input id="telefono" type="text" name="telefono" class="bkform" placeholder="Teléfono*" />
<div class="clearBKform"></div>
<textarea id="comentarios" name="comentarios" class="bkform" placeholder="Comentarios">'.$observaciones.'</textarea>
<h3 align="center" style="padding:8px; background-color:#1A80B6; color:#FFFFFF; width:91%; ">Si est&aacute;s interesado en contactar con Language Campus Tenerife entra <a href="http://www.lcidiomas.com/contacto/" style="color:#E7E7E7; font-weight:bold">aqu&iacute;</a></h3>
<select id="donde" name="donde" class="bkform">
<option value="0">¿Dónde quieres ser atendido?</option>
<option value="2">Las Palmas - Tomás Morales</option>
<option value="11">Las Palmas - Mesa y López </option>
<option value="12">Las Palmas - 7 Palmas </option>
<option value="4">Dpto. Extraescolares</option>
<option value="5">Dpto. Incompany</option>
<option value="6">Dpto. Campamentos</option>
<option value="7">Dpto. Idiomas en el extranjero</option>
</select>
<div class="clearBKform"></div>
<select id="idioma" name="idioma" class="bkform">
<option value="0">Elige un idioma</option>
<option value="1">Inglés</option>
<option value="2">Alemán</option>
<option value="3">Francés</option>
<option value="4">Portugues</option>
<option value="6">Italiano</option>
<option value="9">Ruso</option>
<option value="8">Español</option>
<option value="7">Chino</option>
<option value="5"> Catalán </option></select>
<div class="clearBKform"></div>
<input id="publicidad" type="checkbox" name="publicidad" value="s" checked="checked" class="bkformChk" />
<label for="publicidad" class="bkform">Deseo recibir noticias y publicaciones</label>
<div class="clearBKform"></div>
<input id="politica" type="checkbox" name="politica" value="1" class="bkformChk"/>
<label for="politica" class="bkform" >He leído y acepto los términos de la <a href="https://www.lcidiomasgc.com/politica-de-privacidad/" target="_blank">política de privacidad</a></label>
<div id="alert'.$formulario.'" style="display:none; background:#900; color:#FFF; font-size:10px; text-align:center; height:20px; line-height:20px; width:90%">Debe aceptar la política de privacidad</div>
<div class="clearBKform"></div>
<a href="#formularioLC'.$formulario.'" class="bkBoton" onclick="validar('.$formulario.')" '.$colorB.' >'.$textoB .'</a> 
</form>';
return $lcBox;	

}
add_shortcode( 'bkForm', 'lc_home_form' );



function lc_home_form2( $atts , $content = null ) {



	// Attributes

	extract( shortcode_atts(

		array(

			'observaciones' => '',
			'imagen' => '',
			'origen' => '',
			'origenForm' => '',
			'formulario' => '',	
			'color_boton' => '',		
			'texto_boton' => '',			
			'gracias' => '',
			'idiomaPre' => '',
		), $atts )

	);


$textoB = ($texto_boton == '') ? 'Enviar':$texto_boton;
	// Code
$idi1 = ($idiomaPre == '') ? '':' selected="selected"'; 
	$lcBox = '
<form id="forma'.$formulario.'" name="forma'.$formulario.'" method="post">
<input type="hidden" name="forma" value="'.$origen.'"  /><input type="hidden" name="origenCRM" value="'.$origen.'" /><input type="hidden" name="laURL" value="http2://www.lcidiomasgc.com/gracias/" />
<input type="hidden" name="origenFORM" value="'.$origenForm.'" /><input type="hidden" name="origen" value="14" />
<div class="inputs-row nombre-apellidos">
<input id="nombre'.$formulario.'" type="text" name="nombre" class="bkform50" placeholder="Nombre*"/>
<input id="apellidos" type="text" name="apellidos"  class="bkform50" placeholder="Apellidos*"  />
</div>
<div class="inputs-row email-phone">
<input id="emilio" type="text" name="emilio" class="bkform50" placeholder="E-mail*" />
<input id="telefono" type="text" name="telefono" class="bkform50" placeholder="Teléfono*" />
</div>
<textarea id="comentarios" name="comentarios" class="bkformL" placeholder="¿Cómo nos conociste?*">'.$observaciones.'</textarea>
<select id="donde" name="donde" class="bkform50">
<option value="0">¿Dónde quieres ser atendido?</option>
<option value="2">Las Palmas - Tomás Morales</option>
<option value="11">Las Palmas - Mesa y López </option>
<option value="12">Las Palmas - 7 Palmas </option>
<option value="4">Dpto. Extraescolares</option>
<option value="5">Dpto. Incompany</option>
<option value="6">Dpto. Campamentos</option>
<option value="7">Dpto. Idiomas en el extranjero</option>
</select>
<select id="idioma" name="idioma" class="bkform50">
<option value="0">-- Elige un idioma</option>
<option value="1" '.$idi1.'>Inglés</option>
<option value="2">Alemán</option>
<option value="3">Francés</option>
<option value="4">Portugues</option>
<option value="6">Italiano</option>
<option value="8">Español</option>
<option value="9">Ruso</option>
<option value="7">Chino</option>
<option value="5"> Catalán </option>
</select>
<div class="check_cform_text publicidad">
<input id="publicidad" type="checkbox" name="publicidad" value="s" checked="checked" class="bkformChk" />
<label for="publicidad" class="bkform">Deseo recibir noticias y publicaciones</label>
</div>
<div class="check_cform_text politicas">
<input id="politica" type="checkbox" name="politica" value="1" class="bkformChk"/>
<label for="politica" class="bkform" >He leído y acepto los términos de la <a href="/politica-de-privacidad" target="_blank">política de privacidad</a></label>
</div>
<div id="alert'.$formulario.'" style="display:none; background: #D1495B; color:#FFF; font-size:13px; text-align:center; height:30px; line-height:30px; width:100%; margin-top: 10px;">Debe aceptar la política de privacidad</div>
<div class="button-submit">
<a href="#formularioLC'.$formulario.'" onclick="validar('.$formulario.')">'.$textoB.'</a> 
</div>
</form>';

return $lcBox;	


}

add_shortcode( 'bkFormFoot', 'lc_home_form2' );










?>